﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.Threading;
using DragonTigerToFight.Screen;
using DragonTigerToFight.FightAction.FightActionKyo;
using DragonTigerToFight.FightAction;
using DragonTigerToFight.Item;

namespace DragonTigerToFight
{
    class NhanVatKyoBenPhai : NhanVatKyo
    {
        bool flag = false;
       
        public NhanVatKyoBenPhai(Game1 game, Nhanvat _newnhanvatdoithu)
            : base(game, _newnhanvatdoithu)
        {
            viTri.Y = toadoYbandau;
            viTri.X = toadoXbandau;
            blood = 100;
            energy = 100;
        }

        public override void capNhat_X_Tick(object sender, EventArgs e)
        {
            if (die2)
            {
                vitrihinhbinga = 6;
            }
            else
            {
                if (die || dangbinga)
                {
                    dangdo = false;
                    vitrihinhdungdo = 0;
                    if (vitrihinhbinga < soluonghinhbinga - 1)
                    {
                        if (die)
                        {
                            instancedie.Play();
                        }
                        vitrihinhbinga++;
                        viTri.X += (float)(dochenhlechX * 0.5);
                        if (viTri.X + list_hinhbinga[vitrihinhbinga].Width > Game.Window.ClientBounds.Width)
                        {
                            viTri.X = Game.Window.ClientBounds.Width - list_hinhbinga[vitrihinhbinga].Width;
                        }
                    }
                    else
                    {
                        vitrihinhbinga = 0;
                        if (die)
                        {
                            die2 = true;
                            instancedie.Stop();
                        }
                        else
                        {
                            dangbinga = false;
                            dangvucday = true;
                        }
                    }
                }
                else
                {
                    if (dangvucday)
                    {
                        if (vitrihinhvucday < soluonghinhvucday - 1)
                        {
                            vitrihinhvucday++;
                            if (vitrihinhvucday == 3)
                            {
                                viTri.Y = toadoYbandau + list_hinhdungyen[0].Height - Nhanvat.dochenhlechX - list_hinhvucday[vitrihinhvucday].Height;
                            }
                            else
                            {
                                viTri.Y = toadoYbandau + list_hinhdungyen[0].Height - list_hinhvucday[vitrihinhvucday].Height;
                            }
                            if (vitrihinhvucday >= 2 && vitrihinhvucday <= 5)
                            {
                                viTri.X -= (float)((soluonghinhbinga - 1) * (float)(dochenhlechX * 0.2) / 4.0);
                            }
                        }
                        else
                        {
                            vitrihinhvucday = 0;
                            viTri.Y = toadoYbandau;
                            dangvucday = false;
                        }
                    }
                    else
                    {
                        if (dangdo)
                        {
                            //instancedungdo.Play();
                            if (vitrihinhdungdo < soluonghinhdungdo - 1)
                            {
                                for (int i = 0; i < list_danhsachhanhdong.Count; i++)
                                {
                                    list_danhsachhanhdong[i].dangthuchien = false;
                                    list_danhsachhanhdong[i].vitrihinh = 0;
                                }
                                dangbinga = false;
                                vitrihinhbinga = 0;
                                dangchuyendong = false;
                                vitrihinhdungyen = 0;
                                dangngoi = false;
                                dangnhay = false;
                                vitrihinhnhaytaicho = 0;
                                dangnhaylui = false;
                                dangnhaytoi = false;
                                vitridangnhaytoi = 0;
                                dangvucday = false;
                                dangdo = false;
                                vitrihinhdungdo = 0;
                                vitrihinhvucday = 0;
                                vitrihinhdungdo++;
                                viTri.X += (dolui) / soluonghinhdungdo;
                                if (viTri.X > Game.Window.ClientBounds.Width - list_hinhdungyen[vitrihinhdungyen].Width)
                                {
                                    viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[vitrihinhdungyen].Width;
                                }
                            }
                            else
                            {
                                vitrihinhdungdo = 0;
                                dangdo = false;
                                instancedungdo.Stop();
                            }
                        }
                        else
                        {
                            if (biquynh1)
                            {
                                if (vitrihinhbiquynh1 < soluonghinhbiquynh1 - 1)
                                {
                                    for (int i = 0; i < list_danhsachhanhdong.Count; i++)
                                    {
                                        list_danhsachhanhdong[i].dangthuchien = false;
                                        list_danhsachhanhdong[i].vitrihinh = 0;
                                    }
                                    dangbinga = false;
                                    vitrihinhbinga = 0;
                                    dangchuyendong = false;
                                    vitrihinhdungyen = 0;
                                    dangngoi = false;
                                    dangnhay = false;
                                    vitrihinhnhaytaicho = 0;
                                    dangnhaylui = false;
                                    dangnhaytoi = false;
                                    vitridangnhaytoi = 0;
                                    dangvucday = false;
                                    vitrihinhvucday = 0;
                                    dangdo = false;
                                    vitrihinhdungdo = 0;
                                    vitrihinhbiquynh1++;
                                    viTri.X += (dolui) / soluonghinhbiquynh1;
                                    if (viTri.X > Game.Window.ClientBounds.Width - list_hinhdungyen[vitrihinhdungyen].Width)
                                    {
                                        viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[vitrihinhdungyen].Width;
                                    }
                                }
                                else
                                {
                                    vitrihinhbiquynh1 = 0;
                                    biquynh1 = false;
                                }
                            }
                            else
                            {
                                if (biquynh2)
                                {
                                    if (vitrihinhbiquynh2 < soluonghinhbiquynh2 - 1)
                                    {
                                        for (int i = 0; i < list_danhsachhanhdong.Count; i++)
                                        {
                                            list_danhsachhanhdong[i].dangthuchien = false;
                                            list_danhsachhanhdong[i].vitrihinh = 0;
                                        }
                                        dangbinga = false;
                                        vitrihinhbinga = 0;
                                        dangchuyendong = false;
                                        vitrihinhdungyen = 0;
                                        dangngoi = false;
                                        dangnhay = false;
                                        vitrihinhnhaytaicho = 0;
                                        dangnhaylui = false;
                                        dangnhaytoi = false;
                                        vitridangnhaytoi = 0;
                                        dangvucday = false;
                                        vitrihinhvucday = 0;
                                        vitrihinhbiquynh2++;
                                        viTri.X += (dolui) / soluonghinhbiquynh2;
                                        if (viTri.X > Game.Window.ClientBounds.Width - list_hinhdungyen[vitrihinhdungyen].Width)
                                        {
                                            viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[vitrihinhdungyen].Width;
                                        }
                                    }
                                    else
                                    {
                                        vitrihinhbiquynh2 = 0;
                                        biquynh2 = false;
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < list_danhsachhanhdong.Count; i++)
                                    {
                                        if (list_danhsachhanhdong[i].dangthuchien)
                                        {
                                            list_danhsachhanhdong[i].capNhat_X_Tick(sender, e);
                                            return;
                                        }
                                    }

                                    for (int i = 0; i < list_danhsachitem.Count; i++)
                                    {
                                        if (list_danhsachitem[i].dangsudung)
                                        {
                                            list_danhsachitem[i].capNhat_X_Tick(this);
                                            return;
                                        }
                                    }

                                    if (Dangnhay)
                                    {
                                        biquynh1 = false;
                                        if (vitrihinhnhaytaicho < soluonghinhnhaytaicho - 1)
                                        {
                                            if (vitrihinhnhaytaicho == 0 || vitrihinhnhaytaicho == 5)
                                                viTri.Y = toadoYbandau;
                                            else
                                            {
                                                if (vitrihinhnhaytaicho > 0 && vitrihinhnhaytaicho < 4)
                                                    viTri.Y -= buocnhay;
                                                else
                                                    viTri.Y += (float)(buocnhay * 3.0) / 1;
                                            }
                                            vitrihinhnhaytaicho++;
                                        }
                                        else
                                        {
                                            vitrihinhnhaytaicho = 0;
                                            Dangnhay = false;
                                            kiemtranhaytoi = false;
                                            ((KickbyfootwhenjumpKyoright)(list_danhsachhanhdong[4])).solan_vacham = 0;
                                            ((BeatbyhandwhenjumpKyoright)(list_danhsachhanhdong[1])).solan_vacham = 0;
                                            solan_vacham = 0;
                                        }
                                    }
                                    else
                                    {
                                        if (Dangnhaytoi)
                                        {
                                            kiemtracodangnhaytoi = true;
                                            if (vitridangnhaytoi < soluonghinhnhaytoi - 2)
                                            {
                                                if (collision == 2)
                                                {
                                                    collision = -1;
                                                }
                                                else
                                                {
                                                    if (collision != 6)
                                                    {
                                                        viTri.X -= buocChay * 8;
                                                    }
                                                }
                                                if (vitridangnhaytoi == 0)
                                                {
                                                    viTri.Y = toadoYbandau;
                                                }
                                                else
                                                {
                                                    if (vitridangnhaytoi > 0 && vitridangnhaytoi < 5)
                                                        viTri.Y -= buocnhay;
                                                    else
                                                        viTri.Y += (float)(buocnhay * 3.0) / 2;
                                                }
                                                raNgoaiBanDo();
                                                vitridangnhaytoi++;
                                            }
                                            else
                                            {
                                                vitridangnhaytoi = 0;
                                                viTri.Y = toadoYbandau;
                                                solankiemtradungdokhinhaytoi = 0;
                                                kiemtranhaytoi = false;
                                                Dangnhaytoi = false;
                                                ((KickbyfootwhenjumpKyoright)(list_danhsachhanhdong[4])).solan_vacham = 0;
                                                ((BeatbyhandwhenjumpKyoright)(list_danhsachhanhdong[1])).solan_vacham = 0;
                                                collision = -1;
                                            }

                                        }

                                        else
                                        {
                                            if (dangnhaylui)
                                            {
                                                if (vitridangnhaytoi > 0)
                                                {
                                                    if (collision == 2)
                                                    {
                                                        collision = -1;
                                                    }
                                                    else
                                                    {
                                                        if (collision != 6)
                                                        {
                                                            viTri.X += buocChay * 8;
                                                        }
                                                    }
                                                    if (vitridangnhaytoi == 0)
                                                    {
                                                        viTri.Y = toadoYbandau;
                                                    }
                                                    else
                                                    {
                                                        if (vitridangnhaytoi > 0 && vitridangnhaytoi < 5)
                                                            viTri.Y += (buocnhay + 10);
                                                        else
                                                            viTri.Y -= (float)(buocnhay * 3.0) / 2;
                                                    }
                                                    raNgoaiBanDo();
                                                    vitridangnhaytoi--;
                                                }
                                                else
                                                {
                                                    vitridangnhaytoi = 0;
                                                    viTri.Y = toadoYbandau;
                                                    solankiemtradungdokhinhaytoi = 0;
                                                    kiemtranhaytoi = false;
                                                    solan_vacham = 0;
                                                    ((KickbyfootwhenjumpKyoright)(list_danhsachhanhdong[4])).solan_vacham = 0;
                                                    ((BeatbyhandwhenjumpKyoright)(list_danhsachhanhdong[1])).solan_vacham = 0;
                                                    dangnhaylui = false;
                                                }
                                            }

                                            else
                                            {
                                                if (Dangchuyendong)
                                                {
                                                    if (huongChuyenDong == HanhDong.DiChuyenPhaiQuaTrai)
                                                    {
                                                        if (vitrihinhdichuyen < soluonghinhdichuyen - 1)
                                                            vitrihinhdichuyen++;
                                                        else
                                                            vitrihinhdichuyen = 0;

                                                    }
                                                    else
                                                    {
                                                        if (vitrihinhdichuyen > 0)
                                                            vitrihinhdichuyen--;
                                                        else
                                                            vitrihinhdichuyen = soluonghinhdichuyen - 1;

                                                    }
                                                }
                                                else
                                                {
                                                    if (vitrihinhdungyen < soluonghinhdungyen - 1)
                                                        vitrihinhdungyen++;
                                                    else
                                                        vitrihinhdungyen = 0;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        public override void FightAction(Nhanvat nhanvatdoithu)
        {
            for (int i = 0; i < list_danhsachhanhdong.Count; i++)
            {
                if (list_danhsachhanhdong[i].dangthuchien)
                {
                    if (list_danhsachhanhdong[i].IsFight(this, nhanvatdoithu, viTri))
                    {
                        list_danhsachhanhdong[i].SolveFight(this,nhanvatdoithu);
                    }
                }
            }
        }
            /*if (die2)
            {
                vitrihinhbinga = 6;
            }
            else
            {
                if (die)
                {
                    if (vitrihinhbinga < soluonghinhbinga - 1)
                    {
                        vitrihinhbinga++;
                    }
                    else
                    {
                        vitrihinhbinga = 0;
                        die2 = true;
                        die = false;
                    }
                }
                else
                {
                    if (dangquynhtay)
                    {

                        if (vitrihinhquynhtay == 2)
                        {
                            if (nhanvatdoithu.Dangnhaytoi)
                            {
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width >= viTri.X + list_hinhngoiquynhtay[list_hinhngoiquynhtay.Count - 1].Width - list_hinhquynhtay[vitrihinhquynhtay].Width && (nhanvatdoithu.viTri.Y + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height >= viTri.Y + dochenhlechY))
                                {
                                    fight_type = 1;
                                    solan_vacham++;
                                    return true;
                                }
                            }
                            else
                            {
                                if (nhanvatdoithu.Dangnhay)
                                {
                                    if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width >= viTri.X + list_hinhngoiquynhtay[list_hinhngoiquynhtay.Count - 1].Width - list_hinhquynhtay[vitrihinhquynhtay].Width && (nhanvatdoithu.viTri.Y + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height >= viTri.Y + dochenhlechY))
                                    {
                                        fight_type = 1;
                                        solan_vacham++;
                                        return true;
                                    }
                                }
                                else
                                {
                                    if (nhanvatdoithu.Dangchuyendong)
                                    {
                                        if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width >= viTri.X + list_hinhngoiquynhtay[list_hinhngoiquynhtay.Count - 1].Width - list_hinhquynhtay[vitrihinhquynhtay].Width)
                                        {
                                            fight_type = 0;
                                            solan_vacham++;
                                            return true;
                                        }
                                    }
                                    else
                                    {
                                        if (!nhanvatdoithu.Dangchuyendong)
                                        {
                                            if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width >= viTri.X + list_hinhngoiquynhtay[list_hinhngoiquynhtay.Count - 1].Width - list_hinhquynhtay[vitrihinhquynhtay].Width)
                                            {
                                                fight_type = 0;
                                                solan_vacham++;
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        
                        else
                        {
                            
                            else
                            {
                                
                                else
                                {
                                    if (nhayquynhtay)
                                    {
                                        if ((viTri.X + list_hinhdungyen[0].Width - list_hinhnhaytaicho[list_hinhnhaytaicho.Count - 2].Width) <= (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhdungyen[0].Width) && nhanvatdoithu.viTri.Y <= viTri.Y + 104)
                                        {
                                            fight_type = 8;
                                            solan_vacham++;
                                            return true;
                                        }
                                    }
                                    else
                                    {
                                        if (nhayquynhchan)
                                        {
                                            if (nhanvatdoithu.Dangnhaytoi)
                                            {
                                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width >= viTri.X + list_hinhngoiquynhtay[list_hinhngoiquynhtay.Count - 1].Width - list_hinhngoiquynhchan[vitrihinhngoiquynhchan].Width && (nhanvatdoithu.viTri.Y + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height >= viTri.Y + dochenhlechY))
                                                {
                                                    fight_type = 1;
                                                    solan_vacham++;
                                                    return true;
                                                }
                                            }

                                            else
                                            {
                                                if (nhanvatdoithu.Dangnhay)
                                                {
                                                    if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width >= viTri.X + list_hinhngoiquynhtay[list_hinhngoiquynhtay.Count - 1].Width - list_hinhngoiquynhchan[vitrihinhngoiquynhchan].Width && (nhanvatdoithu.viTri.Y + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height >= viTri.Y + dochenhlechY))
                                                    {
                                                        fight_type = 1;
                                                        solan_vacham++;
                                                        return true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (nhanvatdoithu.Dangchuyendong)
                                                    {
                                                        if (nhanvatdoithu.viTri.X <= viTri.X + list_hinhnhayquynhchan[0].Width && viTri.Y + 2 * dochenhlechY >= nhanvatdoithu.viTri.Y)
                                                        {
                                                            fight_type = 0;
                                                            solan_vacham++;
                                                            return true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (!nhanvatdoithu.Dangchuyendong)
                                                        {
                                                            if (nhanvatdoithu.viTri.X <= viTri.X + list_hinhnhayquynhchan[0].Width && viTri.Y + 2 * dochenhlechY >= nhanvatdoithu.viTri.Y)
                                                            {
                                                                fight_type = 0;
                                                                solan_vacham++;
                                                                return true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        public override void SolveFight(Nhanvat nhanvatdoithu)
        {
            switch (fight_type)
            {
                case 0:
                    {
                        if (!nhanvatdoithu.die && solan_vacham == 1)
                        {
                            Nhanvat.tiengvacham.Play();
                            nhanvatdoithu.blood -= 5;
                            energy += 10;
                            if (dangquynhtay || dangngoiquynhtay)
                            {
                                nhanvatdoithu.biquynh1 = true;
                                nhanvatdoithu.blood -= 5;
                                energy += 10;
                            }
                            else
                            {
                                nhanvatdoithu.biquynh2 = true;
                                nhanvatdoithu.blood -= 10;
                                energy += 15;
                            }
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X - list_hinhbinga[list_hinhbinga.Count - 1].Width < 0)
                                {
                                    nhanvatdoithu.viTri.X = 0;
                                    viTri.X = nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[list_hinhbinga.Count - 1].Width -dochenhlechX;
                                }
                            }
                        }
                        fight_type = -1;
                    }
                    break;
                case 1:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false)
                        {
                            tiengvacham.Play();
                            if (dangquynhtay || dangngoiquynhtay)
                            {
                                nhanvatdoithu.blood -= 5;
                                energy += 10;
                            }
                            else
                            {
                                nhanvatdoithu.blood -= 10;
                                energy += 15;
                            }
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X - list_hinhbinga[list_hinhbinga.Count - 1].Width < 0)
                                {
                                    nhanvatdoithu.viTri.X = 0;
                                    viTri.X = nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[list_hinhbinga.Count - 1].Width -dochenhlechX;
                                }
                            }
                            nhanvatdoithu.dangnhaylui = true;
                            nhanvatdoithu.Dangnhaytoi = false;
                            nhanvatdoithu.Dangnhay = false;
                            nhanvatdoithu.vitridangnhaytoi = 8;
                            vitridangnhaytoi = 0;
                        }
                        fight_type = -1;
                    }
                    break;
                /*case 2:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false)
                        {
                            tiengvacham.Play();
                            nhanvatdoithu.blood -= 10;
                            energy += 15;
                            if (nhanvatdoithu.Dangchuyendong || !nhanvatdoithu.Dangchuyendong)
                            {
                                nhanvatdoithu.biquynh2 = true;
                            }
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                nhanvatdoithu.viTri.X = 0;
                                viTri.X = nhanvatdoithu.list_hinhbinga[6].Width;
                            }
                            fight_type = -1;
                        }
                    }
                    break;
                case 3:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false)
                        {
                            tiengvacham.Play();
                            nhanvatdoithu.blood -= 10;
                            energy += 15;
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                nhanvatdoithu.viTri.X = 0;
                                viTri.X = nhanvatdoithu.list_hinhbinga[6].Width;
                            }
                            fight_type = -1;
                        }
                    }
                    break;
                case 4:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false)
                        {
                            tiengvacham.Play();
                            nhanvatdoithu.blood -= 4;
                            energy += 10;
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                nhanvatdoithu.viTri.X = 0;
                                viTri.X = nhanvatdoithu.list_hinhbinga[6].Width;
                            }
                            fight_type = -1;
                        }
                    }
                    break;
                case 5:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false )
                        {
                            tiengvacham.Play();
                            nhanvatdoithu.blood -= 4;
                            energy += 10;
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                nhanvatdoithu.viTri.X = 0;
                                viTri.X = nhanvatdoithu.list_hinhbinga[6].Width;
                            }
                            fight_type = -1;
                        }
                    }
                    break;
                case 6:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false )
                        {
                            tiengvacham.Play();
                            nhanvatdoithu.blood -= 5;
                            energy += 15;
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                nhanvatdoithu.viTri.X = 0;
                                viTri.X = nhanvatdoithu.list_hinhbinga[6].Width;
                            }
                            fight_type = -1;
                        }
                    }
                    break;
                case 7:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false )
                        {
                            tiengvacham.Play();
                            nhanvatdoithu.blood -= 5;
                            energy += 15;
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                nhanvatdoithu.viTri.X = 0;
                                viTri.X = nhanvatdoithu.list_hinhbinga[6].Width;
                            }
                            fight_type = -1;
                        }
                    }
                    break;
                case 8:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false )
                        {
                            tiengvacham.Play();
                            nhanvatdoithu.blood -= 4;
                            energy += 10;
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                nhanvatdoithu.viTri.X = 0;
                                viTri.X = nhanvatdoithu.list_hinhbinga[6].Width;
                            }
                            fight_type = -1;
                        }
                    }
                    break;
                case 9:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false)
                        {
                            tiengvacham.Play();
                            nhanvatdoithu.blood -= 5;
                            energy += 10;
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                nhanvatdoithu.viTri.X = 0;
                                viTri.X = nhanvatdoithu.list_hinhbinga[6].Width;
                            }
                            fight_type = -1;
                        }
                    }
                    break;
            }
        }*/

        public override void LoadContent(ContentManager contend)
        {
            sounddie = contend.Load<SoundEffect>("AMTHANH/AMTHANHKYO/bichetcuakyo");
            instancedie = sounddie.CreateInstance();
            sounddungdo = contend.Load<SoundEffect>("amthanhdo");
            instancedungdo = sounddungdo.CreateInstance();
            list_hinhbinga.Clear();
            list_hinhbiquynh1.Clear();
            list_hinhbiquynh2.Clear();
            list_hinhdichuyen.Clear();
            list_hinhdungyen.Clear();
            list_hinhnhaytaicho.Clear();
            list_hinhnhaytoi.Clear();
            list_danhsachhanhdong.Clear();
            list_hinhvucday.Clear();
            list_hinhdungdo.Clear();
            list_danhsachitem.Clear();
            if (chisonhanvat == 2)
            {
                list_danhsachhanhdong.Add(new BeatbyhandKyoleft());
                list_danhsachhanhdong.Add(new BeatbyhandwhenjumpKyoleft());
                list_danhsachhanhdong.Add(new BeatbyhandwhensitKyoleft());
                list_danhsachhanhdong.Add(new KickbyfootKyoleft());
                list_danhsachhanhdong.Add(new KickbyfootwhenjumpKyoleft());
                list_danhsachhanhdong.Add(new KickbyfootwhensitKyoleft());
                list_danhsachhanhdong.Add(new SpecialactionKyoleft());
                list_danhsachitem.Add(new Itemincreasedblood());
                list_danhsachitem.Add(new Itemincreasedpower());
                hinhngoinhanvat = contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhngoi");
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-1"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-2"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-3"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-4"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-5"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-6"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-7"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-8"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-9"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-10"));

                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-1"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-2"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-3"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-4"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-5"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-6"));

                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-1"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-3"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-4"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-5"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-6"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-7"));

                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-1"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-3"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-4"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-5"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-6"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-7"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-8"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-9"));

                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh1"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh2"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh3"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh4"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh5"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh6"));

                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh2/hinhbiquynh2-1"));
                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh2/hinhbiquynh2-3"));
                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh2/hinhbiquynh2-4"));

                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-1"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-2"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-3"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-4"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-5"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-6"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-7"));

                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-1"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-2"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-3"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-4"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-5"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-6"));

                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-1"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-2"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-3"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-4"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-5"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-6"));

                for (int i = 0; i < list_danhsachhanhdong.Count; i++)
                {
                    list_danhsachhanhdong[i].Load(contend, chisonhanvat);
                }

                list_hinhbinga.Clear();
                list_hinhbiquynh1.Clear();
                list_hinhbiquynh2.Clear();
                list_hinhdichuyen.Clear();
                list_hinhdungyen.Clear();
                list_hinhnhaytaicho.Clear();
                list_hinhnhaytoi.Clear();
                list_danhsachhanhdong.Clear();
                list_hinhvucday.Clear();
                list_hinhdungdo.Clear();
                list_danhsachitem.Clear();

                list_danhsachhanhdong.Add(new BeatbyhandKyoright());
                list_danhsachhanhdong.Add(new BeatbyhandwhenjumpKyoright());
                list_danhsachhanhdong.Add(new BeatbyhandwhensitKyoright());
                list_danhsachhanhdong.Add(new KickbyfootKyoright());
                list_danhsachhanhdong.Add(new KickbyfootwhenjumpKyoright());
                list_danhsachhanhdong.Add(new KickbyfootwhensitKyoright());
                list_danhsachhanhdong.Add(new SpecialactionKyoright());
                list_danhsachitem.Add(new Itemincreasedblood());
                list_danhsachitem.Add(new Itemincreasedpower());
                hinhngoinhanvat = contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhngoi");
                loai = 'p';
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-1"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-2"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-3"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-4"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-5"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-6"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-7"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-8"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-9"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungyen/dungyen-10"));

                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdichuyenqualai/dichuyen-1"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdichuyenqualai/dichuyen-2"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdichuyenqualai/dichuyen-3"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdichuyenqualai/dichuyen-4"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdichuyenqualai/dichuyen-5"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdichuyenqualai/dichuyen-6"));

                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytaicho/nhaytaicho-1"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytaicho/nhaytaicho-3"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytaicho/nhaytaicho-4"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytaicho/nhaytaicho-5"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytaicho/nhaytaicho-6"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytaicho/nhaytaicho-7"));

                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytoi/hinhnhaytoi-1"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytoi/hinhnhaytoi-3"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytoi/hinhnhaytoi-4"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytoi/hinhnhaytoi-5"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytoi/hinhnhaytoi-6"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytoi/hinhnhaytoi-7"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytoi/hinhnhaytoi-8"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhaytoi/hinhnhaytoi-9"));

                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbiquynh1/biquynh1"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbiquynh1/biquynh2"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbiquynh1/biquynh3"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbiquynh1/biquynh4"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbiquynh1/biquynh5"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbiquynh1/biquynh6"));

                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbiquynh2/hinhbiquynh2-1"));
                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbiquynh2/hinhbiquynh2-3"));
                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbiquynh2/hinhbiquynh2-4"));

                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbinga/hinhbinga-1"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbinga/hinhbinga-2"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbinga/hinhbinga-3"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbinga/hinhbinga-4"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbinga/hinhbinga-5"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbinga/hinhbinga-6"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhbinga/hinhbinga-7"));

                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhvucday/hinhvucday-1"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhvucday/hinhvucday-2"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhvucday/hinhvucday-3"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhvucday/hinhvucday-4"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhvucday/hinhvucday-5"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhvucday/hinhvucday-6"));
                this.viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width - 70;
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungdo/hinhdungdo-1"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungdo/hinhdungdo-2"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungdo/hinhdungdo-3"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungdo/hinhdungdo-4"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungdo/hinhdungdo-5"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdungdo/hinhdungdo-6"));
                
                for (int i = 0; i < list_danhsachhanhdong.Count; i++)
                {
                    list_danhsachhanhdong[i].Load(contend, chisonhanvat);
                }
                for (int i = 0; i < list_danhsachitem.Count; i++)
                {
                    list_danhsachitem[i].Load(contend);
                }
            }
            else
            {
                list_danhsachhanhdong.Add(new BeatbyhandKyoleft());
                list_danhsachhanhdong.Add(new BeatbyhandwhenjumpKyoleft());
                list_danhsachhanhdong.Add(new BeatbyhandwhensitKyoleft());
                list_danhsachhanhdong.Add(new KickbyfootKyoleft());
                list_danhsachhanhdong.Add(new KickbyfootwhenjumpKyoleft());
                list_danhsachhanhdong.Add(new KickbyfootwhensitKyoleft());
                list_danhsachhanhdong.Add(new SpecialactionKyoleft());
                list_danhsachitem.Add(new Itemincreasedblood());
                list_danhsachitem.Add(new Itemincreasedpower());
                hinhngoinhanvat = contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhngoi");
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-1"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-2"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-3"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-4"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-5"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-6"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-7"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-8"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-9"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungyen/dungyen-10"));

                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdichuyenqualai/dichuyen-1"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdichuyenqualai/dichuyen-2"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdichuyenqualai/dichuyen-3"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdichuyenqualai/dichuyen-4"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdichuyenqualai/dichuyen-5"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdichuyenqualai/dichuyen-6"));

                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytaicho/nhaytaicho-1"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytaicho/nhaytaicho-3"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytaicho/nhaytaicho-4"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytaicho/nhaytaicho-5"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytaicho/nhaytaicho-6"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytaicho/nhaytaicho-7"));

                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytoi/hinhnhaytoi-1"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytoi/hinhnhaytoi-3"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytoi/hinhnhaytoi-4"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytoi/hinhnhaytoi-5"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytoi/hinhnhaytoi-6"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytoi/hinhnhaytoi-7"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytoi/hinhnhaytoi-8"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhaytoi/hinhnhaytoi-9"));

                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbiquynh1/biquynh1"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbiquynh1/biquynh2"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbiquynh1/biquynh3"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbiquynh1/biquynh4"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbiquynh1/biquynh5"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbiquynh1/biquynh6"));

                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbiquynh2/hinhbiquynh2-1"));
                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbiquynh2/hinhbiquynh2-3"));
                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbiquynh2/hinhbiquynh2-4"));

                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbinga/hinhbinga-1"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbinga/hinhbinga-2"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbinga/hinhbinga-3"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbinga/hinhbinga-4"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbinga/hinhbinga-5"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbinga/hinhbinga-6"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhbinga/hinhbinga-7"));

                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhvucday/hinhvucday-1"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhvucday/hinhvucday-2"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhvucday/hinhvucday-3"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhvucday/hinhvucday-4"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhvucday/hinhvucday-5"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhvucday/hinhvucday-6"));

                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungdo/hinhdungdo-1"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungdo/hinhdungdo-2"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungdo/hinhdungdo-3"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungdo/hinhdungdo-4"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungdo/hinhdungdo-5"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhdungdo/hinhdungdo-6"));

                for (int i = 0; i < list_danhsachhanhdong.Count; i++)
                {
                    list_danhsachhanhdong[i].Load(contend, chisonhanvat);
                }
                list_hinhbinga.Clear();
                list_hinhbiquynh1.Clear();
                list_hinhbiquynh2.Clear();
                list_hinhdichuyen.Clear();
                list_hinhdungyen.Clear();
                list_hinhnhaytaicho.Clear();
                list_hinhnhaytoi.Clear();
                list_danhsachhanhdong.Clear();
                list_hinhvucday.Clear();
                list_hinhdungdo.Clear();
                list_danhsachitem.Clear();

                hinhngoinhanvat = contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhngoi");
                loai = 'p';
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-1"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-2"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-3"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-4"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-5"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-6"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-7"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-8"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-9"));
                list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungyen/dungyen-10"));

                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdichuyenqualai/dichuyen-1"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdichuyenqualai/dichuyen-2"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdichuyenqualai/dichuyen-3"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdichuyenqualai/dichuyen-4"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdichuyenqualai/dichuyen-5"));
                list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdichuyenqualai/dichuyen-6"));

                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytaicho/nhaytaicho-1"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytaicho/nhaytaicho-3"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytaicho/nhaytaicho-4"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytaicho/nhaytaicho-5"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytaicho/nhaytaicho-6"));
                list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytaicho/nhaytaicho-7"));

                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytoi/hinhnhaytoi-1"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytoi/hinhnhaytoi-3"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytoi/hinhnhaytoi-4"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytoi/hinhnhaytoi-5"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytoi/hinhnhaytoi-6"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytoi/hinhnhaytoi-7"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytoi/hinhnhaytoi-8"));
                list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhaytoi/hinhnhaytoi-9"));
                
                this.viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width;

                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbiquynh1/biquynh1"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbiquynh1/biquynh2"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbiquynh1/biquynh3"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbiquynh1/biquynh4"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbiquynh1/biquynh5"));
                list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbiquynh1/biquynh6"));

                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbiquynh2/hinhbiquynh2-1"));
                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbiquynh2/hinhbiquynh2-3"));
                list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbiquynh2/hinhbiquynh2-4"));
            
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbinga/hinhbinga-1"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbinga/hinhbinga-2"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbinga/hinhbinga-3"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbinga/hinhbinga-4"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbinga/hinhbinga-5"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbinga/hinhbinga-6"));
                list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhbinga/hinhbinga-7"));

                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhvucday/hinhvucday-1"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhvucday/hinhvucday-2"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhvucday/hinhvucday-3"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhvucday/hinhvucday-4"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhvucday/hinhvucday-5"));
                list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhvucday/hinhvucday-6"));

                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungdo/hinhdungdo-1"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungdo/hinhdungdo-2"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungdo/hinhdungdo-3"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungdo/hinhdungdo-4"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungdo/hinhdungdo-5"));
                list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdungdo/hinhdungdo-6"));

                for (int i = 0; i < list_danhsachhanhdong.Count; i++)
                {
                    list_danhsachhanhdong[i].Load(contend, chisonhanvat);
                }

                for (int i = 0; i < list_danhsachitem.Count; i++)
                {
                    list_danhsachitem[i].Load(contend);
                }
            }
        }

        public override void Update1(KeyboardState newcurrentkey)
        {
            keys = newcurrentkey;
            Keys[] currentPressedKeys = keys.GetPressedKeys();
            kiemtranhaytoi = false;
            kiemtranhaylui = false;
            if (chisonhanvat == 1)
            {
                for (int i = 0; i < list_danhsachitem.Count; i++)
                {
                    if (list_danhsachitem[i].IsUse(keys, chisonhanvat) && !list_danhsachitem[i].dangsudung)
                    {
                        list_danhsachitem[i].dangsudung = true;
                        list_danhsachitem[i].Affect(this, nhanvatdoithu);
                    }
                }
                if (keys.IsKeyDown(ScreenFight.keyrightnguoichoi1))
                {
                    if (currentPressedKeys.Contains(ScreenFight.keyattackbyhandnguoichoi1))
                    {
                        if (!Dangnhay && !dangnhaytoi && !dangnhaylui)
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[0].dangthuchien = true;
                                DiChuyen();
                                return;
                            }
                            return;
                        }
                    }
                    else
                    {
                        if (currentPressedKeys.Contains(ScreenFight.keyattackbyfootnguoichoi1))
                        {
                            if (!Dangnhay && !dangnhaytoi && !dangnhaylui)
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[3].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                        else
                        {
                            huongChuyenDong = HanhDong.DiChuyenTraiQuaPhai;
                            kiemtranhaylui = true;
                            DiChuyen();
                            return;
                        }
                    }
                }
                if (keys.IsKeyDown(ScreenFight.keyleftnguoichoi1))
                {
                    if (currentPressedKeys.Contains(ScreenFight.keyattackbyhandnguoichoi1))
                    {
                        if (!Dangnhay && !Dangnhaytoi && !dangnhaylui)
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[0].dangthuchien = true;
                                DiChuyen();
                                return;
                            }
                            return;
                        }
                    }
                    else
                    {
                        if (currentPressedKeys.Contains(ScreenFight.keyattackbyfootnguoichoi1))
                        {
                            if (!Dangnhay && !Dangnhaytoi && !dangnhaylui)
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[3].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                        else
                        {
                            huongChuyenDong = HanhDong.DiChuyenPhaiQuaTrai;
                            DiChuyen();
                            kiemtranhaytoi = true;
                            return;
                        }
                    }
                }

                if (keys.IsKeyDown(ScreenFight.keydownnguoichoi1))
                {
                    if (currentPressedKeys.Contains(ScreenFight.keyattackbyhandnguoichoi1) && !previouskeyboardstate.IsKeyDown(ScreenFight.keyattackbyhandnguoichoi1))
                    {
                        if (!IsAction(list_danhsachhanhdong))
                        {
                            list_danhsachhanhdong[2].dangthuchien = true;
                            DiChuyen();
                            return;
                        }
                        return;
                    }
                    else
                    {
                        if (currentPressedKeys.Contains(ScreenFight.keyattackbyfootnguoichoi1) && !previouskeyboardstate.IsKeyDown(ScreenFight.keyattackbyfootnguoichoi1))
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[5].dangthuchien = true;
                                DiChuyen();
                                return;
                            }
                            return;
                        }
                        else
                        {
                            huongChuyenDong = HanhDong.DangNgoi;
                            DiChuyen();
                            return;
                        }
                    }
                }
                if (keys.IsKeyDown(ScreenFight.keyattackbyhandnguoichoi1))
                {
                    if (Dangnhay)
                    {
                        if (!IsAction(list_danhsachhanhdong))
                        {
                            list_danhsachhanhdong[1].dangthuchien = true;

                            DiChuyen();
                            return;
                        }
                        return;
                    }
                    else
                    {
                        if (Dangnhaytoi)
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[1].dangthuchien = true;
                                DiChuyen();
                                return;
                            }
                            return;
                        }
                        else
                        {
                            if (dangnhaylui)
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[1].dangthuchien = true;

                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                            else
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[0].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                    }
                }
                if (keys.IsKeyDown(ScreenFight.keyattackbyfootnguoichoi1))
                {
                    if (Dangnhay)
                    {
                        if (!IsAction(list_danhsachhanhdong))
                        {
                            list_danhsachhanhdong[4].dangthuchien = true;
                            DiChuyen();
                            return;
                        }
                        return;
                    }
                    else
                    {
                        if (Dangnhaytoi)
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[4].dangthuchien = true;
                                DiChuyen();
                                return;
                            }
                            return;
                        }
                        else
                        {
                            if (dangnhaylui)
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[4].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                            else
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[3].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                    }
                   
                }
                if (keys.IsKeyDown(ScreenFight.keyspecialactionnguoichoi1))
                {
                    if (!dangchuyendong || dangchuyendong || dangngoi)
                    {
                        if (!IsAction(list_danhsachhanhdong) && (((SpecialactionKyoright)(list_danhsachhanhdong[6])).IsSatisfied(this)))
                        {
                            list_danhsachhanhdong[6].dangthuchien = true;
                            DiChuyen();
                            return;
                        }
                        return;
                    }
                }

                

                Dangchuyendong = false;
                kiemtranhaytoi = false;
                kiemtranhaylui = false;
            }
            else
            {
                for (int i = 0; i < list_danhsachitem.Count; i++)
                {
                    if (list_danhsachitem[i].IsUse(keys, chisonhanvat) && !list_danhsachitem[i].dangsudung)
                    {
                        list_danhsachitem[i].dangsudung = true;
                        list_danhsachitem[i].Affect(this, nhanvatdoithu);
                    }
                }
                if (keys.IsKeyDown(ScreenFight.keyrightnguoichoi2))
                {
                    if (currentPressedKeys.Contains(ScreenFight.keyattackbyhandnguoichoi2))
                    {
                        if (!Dangnhay && !dangnhaytoi && !dangnhaylui)
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[0].dangthuchien = true;
                                DiChuyen();
                                return;
                            }
                            return;
                        }
                    }
                    else
                    {
                        if (currentPressedKeys.Contains(ScreenFight.keyattackbyfootnguoichoi2))
                        {
                            if (!Dangnhay && !dangnhaytoi && !dangnhaylui)
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[3].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                        else
                        {
                            huongChuyenDong = HanhDong.DiChuyenTraiQuaPhai;
                            kiemtranhaylui = true;
                            DiChuyen();
                            return;
                        }
                    }
                }
                if (keys.IsKeyDown(ScreenFight.keyleftnguoichoi2))
                {
                    if (currentPressedKeys.Contains(ScreenFight.keyattackbyhandnguoichoi2))
                    {
                        if (!Dangnhay && !Dangnhaytoi && !dangnhaylui)
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[0].dangthuchien = true;
                                DiChuyen();
                                return;
                            }
                            return;
                        }
                    }
                    else
                    {
                        if (currentPressedKeys.Contains(ScreenFight.keyattackbyfootnguoichoi2))
                        {
                            if (!Dangnhay && !Dangnhaytoi && !dangnhaylui)
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[3].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                        else
                        {
                            huongChuyenDong = HanhDong.DiChuyenPhaiQuaTrai;
                            DiChuyen();
                            kiemtranhaytoi = true;
                            return;
                        }
                    }
                }

                if (keys.IsKeyDown(ScreenFight.keydownnguoichoi2))
                {
                    if (currentPressedKeys.Contains(ScreenFight.keyattackbyhandnguoichoi2) && !previouskeyboardstate.IsKeyDown(ScreenFight.keyattackbyhandnguoichoi2))
                    {
                        if (!IsAction(list_danhsachhanhdong))
                        {
                            list_danhsachhanhdong[2].dangthuchien = true;
                            DiChuyen();
                            return;
                        }
                        return;
                    }
                    else
                    {
                        if (currentPressedKeys.Contains(ScreenFight.keyattackbyfootnguoichoi2) && !previouskeyboardstate.IsKeyDown(ScreenFight.keyattackbyfootnguoichoi2))
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[5].dangthuchien = true;
                                DiChuyen();
                                return;
                            }
                            return;
                        }
                        else
                        {
                            huongChuyenDong = HanhDong.DangNgoi;
                            DiChuyen();
                            return;
                        }
                    }
                }
                if (keys.IsKeyDown(ScreenFight.keyattackbyhandnguoichoi2))
                {
                    if (Dangnhay)
                    {
                        if (!IsAction(list_danhsachhanhdong))
                        {
                            list_danhsachhanhdong[1].dangthuchien = true;

                            DiChuyen();
                            return;
                        }
                        return;
                    }
                    else
                    {
                        if (Dangnhaytoi)
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[1].dangthuchien = true;

                                DiChuyen();
                                return;
                            }
                            return;
                        }
                        else
                        {
                            if (dangnhaylui)
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[1].dangthuchien = true;

                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                            else
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[0].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                    }
                }
                if (keys.IsKeyDown(ScreenFight.keyattackbyfootnguoichoi2))
                {
                    if (Dangnhay)
                    {
                        if (!IsAction(list_danhsachhanhdong))
                        {
                            list_danhsachhanhdong[4].dangthuchien = true;
                            DiChuyen();
                            return;
                        }
                        return;
                    }
                    else
                    {
                        if (Dangnhaytoi)
                        {
                            if (!IsAction(list_danhsachhanhdong))
                            {
                                list_danhsachhanhdong[4].dangthuchien = true;
                                DiChuyen();
                                return;
                            }
                            return;
                        }
                        else
                        {
                            if (dangnhaylui)
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[4].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                            else
                            {
                                if (!IsAction(list_danhsachhanhdong))
                                {
                                    list_danhsachhanhdong[3].dangthuchien = true;
                                    DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                    }
                }

                if (keys.IsKeyDown(ScreenFight.keyspecialactionnguoichoi2))
                {
                    if (!dangchuyendong || dangchuyendong || dangngoi)
                    {
                        if (!IsAction(list_danhsachhanhdong) && (((SpecialactionKyoright)(list_danhsachhanhdong[6])).IsSatisfied(this)))
                        {
                            list_danhsachhanhdong[6].dangthuchien = true;
                            DiChuyen();
                            return;
                        }
                        return;
                    }
                }

                Dangchuyendong = false;
                kiemtranhaytoi = false;
                kiemtranhaylui = false;
            }
        }

       

        public override void Update2()
        {
            if (chisonhanvat == 1)
            {
                if (!Dangnhay && !dangnhaytoi && !dangnhaylui)
                {
                    if (kiemtranhaytoi)
                    {
                        if (keys.IsKeyDown(ScreenFight.keyupnguoichoi1) == true)
                        {
                            huongChuyenDong = HanhDong.NhayToi;
                            DiChuyen();
                            kiemtranhaytoi = false;
                        }
                    }
                    else
                    {
                        if (kiemtranhaylui)
                        {
                            if (keys.IsKeyDown(ScreenFight.keyupnguoichoi1) == true)
                            {
                                huongChuyenDong = HanhDong.NhayLui;
                                DiChuyen();
                                kiemtranhaylui = false;
                            }
                        }
                        else
                        {
                            if (keys.IsKeyDown(ScreenFight.keyupnguoichoi1) == true)
                            {
                                huongChuyenDong = HanhDong.NhayLenTaiCho;
                                DiChuyen();
                                kiemtranhaytoi = false;
                                kiemtranhaylui = false;
                            }
                        }
                    }
                    previouskeyboardstate = keys;
                    return;
                }
                return;
            }
            else
            {
                if (!Dangnhay && !dangnhaytoi && !dangnhaylui)
                {
                    if (kiemtranhaytoi)
                    {
                        if (keys.IsKeyDown(ScreenFight.keyupnguoichoi2) == true)
                        {
                            huongChuyenDong = HanhDong.NhayToi;
                            DiChuyen();
                            kiemtranhaytoi = false;
                        }
                    }
                    else
                    {
                        if (kiemtranhaylui)
                        {
                            if (keys.IsKeyDown(ScreenFight.keyupnguoichoi2) == true)
                            {
                                huongChuyenDong = HanhDong.NhayLui;
                                DiChuyen();
                                kiemtranhaylui = false;
                            }
                        }
                        else
                        {
                            if (keys.IsKeyDown(ScreenFight.keyupnguoichoi2) == true)
                            {
                                huongChuyenDong = HanhDong.NhayLenTaiCho;
                                DiChuyen();
                                kiemtranhaytoi = false;
                                kiemtranhaylui = false;
                            }
                        }
                    }
                    previouskeyboardstate = keys;
                    return;
                }
                return;
            }
        }
    }
}

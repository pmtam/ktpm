﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace DragonBall_ZZZ
{
    public class My2DSprite
    {
        private Texture2D[] _Texture;

        public Texture2D[] Texture
        {
            get { return _Texture; }
            set
            {
                _Texture = value;
                _nTextures = _Texture.Length;
                _iTexture = 0;
                _Width = _Texture[0].Width;
                _Height = _Texture[0].Height;
            }
        }
        private int _nTextures;

        public int nTextures
        {
            get { return _nTextures; }
            set { _nTextures = value; }
        }
        private int _iTexture;

        public int iTexture
        {
            get { return _iTexture; }
            set { _iTexture = value; }
        }
        private double _Left;

        private int _State = 0;

        public int State
        {
            get { return _State; }
            set { _State = value; }
        }
        public double Left
        {
            get { return _Left; }
            set { _Left = value; }
        }
        private double _Top;

        public double Top
        {
            get { return _Top; }
            set { _Top = value; }
        }
        private double _Width;

        public double Width
        {
            get { return _Width; }
            set { _Width = value; }
        }
        private double _Height;

        public double Height
        {
            get { return _Height; }
            set { _Height = value; }
        }

        public My2DSprite(Texture2D[] texture, double left, double top)
        {
            Texture = texture;
            Left = left;
            Top = top;
        }

        double delta1 = 1;
        double delta2 = 1;
        double _delay = 16;

        public double Delay
        {
            get { return _delay; }
            set { _delay = value; }
        }

        private AbstractPathUpdater _PathUpdate = null;

        public AbstractPathUpdater PathUpdate
        {
            get { return _PathUpdate; }
            set { _PathUpdate = value; }
        }

        public void Update(GameTime gametime)
        {
            int nFrame = (int)(gametime.TotalGameTime.TotalMilliseconds / _delay);
            _iTexture = nFrame % _nTextures;

            if (_PathUpdate != null)
            {
                Vector2 NewPos = _PathUpdate.Update();
                _Left = NewPos.X;
                _Top = NewPos.Y;
            }
        }

        public void Draw(GameTime gametime, SpriteBatch spriteBatch, Color color)
        {
            if (State == 0)
                spriteBatch.Draw(_Texture[_iTexture], new Rectangle((int)_Left, (int)_Top, (int)_Width,
                (int)_Height), color);
            else
            {
                //ve hinh kich thuoc binh thuong
                Rectangle sourceRect = new Rectangle(
                    (int)_Left,
                    (int)_Top,
                    (int)Width,
                    (int)Height);

                //ve hinh kich thuoc thay doi
                Rectangle destRect = new Rectangle(
                    (int)(Left - delta1),
                    (int)(Top - delta1),
                    (int)(Width + 2 * delta1),
                    (int)(Height + 2 * delta1));

                spriteBatch.Draw(_Texture[_iTexture], sourceRect, Color.YellowGreen);
            }
        }

        public bool IsSelected(double x, double y)
        {
            if (x < _Left || x >= _Left + _Width)
                return false;
            if (y < _Top || y >= _Top + _Height)
                return false;
            return true;
        }
    }
}

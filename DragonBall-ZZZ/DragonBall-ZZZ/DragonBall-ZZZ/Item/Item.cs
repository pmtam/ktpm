﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace DragonBall_ZZZ.Item
{
    public class Item
    {
        public int solandangsudung;
        public int solansudungtoida;
        public Keys keyuseitemactor1;
        public Keys keyuseitemactor2;
        public Vector2 vitri = new Vector2(0,0);

        public Texture2D sprite_item;
        public int solanhienlenhientai=0;
        public int solanhientoida = 6;
        public int dodichchuyen = 75;
        public bool dangsudung = false;
        public bool IsUse(KeyboardState keys, int chisonhanvat)
        {
            if (chisonhanvat == 1)
            {
                if (keys.IsKeyDown(keyuseitemactor1) && solandangsudung<solansudungtoida)
                {
                    return true;
                }
            }
            else
            {
                if (keys.IsKeyDown(keyuseitemactor2) && solandangsudung < solansudungtoida)
                {
                    return true;
                }
            }
            return false;
        }
        virtual public void Affect(Nhanvat nhanvat)
        {
            ;
        }

        virtual public void Load(ContentManager content)
        {
            ;
        }

        virtual public void Draw(Nhanvat nhanvat, SpriteBatch spriteBatch, int chiso)
        {
            ;
        }

        virtual public void capNhat_X_Tick(Nhanvat nhanvat)
        {
            ;
        }

    }
}

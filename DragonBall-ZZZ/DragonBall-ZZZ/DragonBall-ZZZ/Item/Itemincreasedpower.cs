﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace DragonBall_ZZZ.Item
{
     public class Itemincreasedpower : Item
    {
       public int luongnangluongtang;
        public Itemincreasedpower()
        {
            solandangsudung = 0;
            solansudungtoida = 2;
            luongnangluongtang = 50;
            keyuseitemactor1 = Keys.E;
            keyuseitemactor2 = Keys.I;
        }
        public override void Affect(Nhanvat nhanvat)
        {
                solandangsudung++;
                nhanvat.energy += luongnangluongtang;
                if (luongnangluongtang > Nhanvat.max_energy)
                {
                    nhanvat.energy = Nhanvat.max_energy;
                }
        }

        public override void Load(ContentManager content)
        {
           sprite_item = content.Load<Texture2D>("HINHITEM/item/Tangnangluong");
        }

        public override void Draw(Nhanvat nhanvat, SpriteBatch spriteBatch, int chiso)
        {
            spriteBatch.Begin();

            vitri.X = nhanvat.viTri.X;
            vitri.Y = nhanvat.viTri.Y - solanhienlenhientai * (dodichchuyen) / solanhientoida;
            spriteBatch.Draw(sprite_item, vitri, Microsoft.Xna.Framework.Color.White);
            
            spriteBatch.End();
        }

        public override void capNhat_X_Tick(Nhanvat nhanvat)
        {
            if (solanhienlenhientai < solanhientoida)
            {
                solanhienlenhientai++;
               
            }
            else
            {
                solanhienlenhientai = 0;
                dangsudung = false;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace DragonBall_ZZZ.Item
{
    public class Itemincreasedblood : Item
    {
       public int luongmautang;
       
        public Itemincreasedblood()
        {
            solandangsudung = 0;
            solansudungtoida = 2;
            luongmautang = 20;
            keyuseitemactor1 = Keys.E;
            keyuseitemactor2 = Keys.I;
        }
        public override void Affect(Nhanvat nhanvat)
        {
            solandangsudung++;
            nhanvat.blood += luongmautang;
            if (nhanvat.blood > Nhanvat.max_blood)
            {
                nhanvat.blood = Nhanvat.max_blood;
            }
        }

        public override void Load(ContentManager contend)
        {
            sprite_item = contend.Load<Texture2D>("HINHITEM/item/Tangmau");
        }

        public override void Draw(Nhanvat nhanvat, SpriteBatch spriteBatch, int chiso)
        {
            spriteBatch.Begin();
            if (chiso == 0)
            {
                vitri.X = nhanvat.viTri.X;
                vitri.Y = nhanvat.viTri.Y - solanhienlenhientai * (dodichchuyen) / solanhientoida;
                spriteBatch.Draw(sprite_item, vitri, Microsoft.Xna.Framework.Color.White);
            }
            else
            {
                if (chiso == 1)
                {
                    spriteBatch.Draw(sprite_item, vitri, Microsoft.Xna.Framework.Color.White);
                }
            }
            
            spriteBatch.End();
        }

        public override void capNhat_X_Tick(Nhanvat nhanvat)
        {
            if (solanhienlenhientai < solanhientoida)
            {
                solanhienlenhientai++;
               
            }
            else
            {
                solanhienlenhientai = 0;
                dangsudung = false;
            }
        }

    }
}

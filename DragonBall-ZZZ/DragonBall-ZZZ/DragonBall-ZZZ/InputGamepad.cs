﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace DragonBall_ZZZ
{
    class InputGamepad
    {
        GamepadKey Up, Left, Right, Down, RT, LT, A, B, X, Y, Start, Select;
        GamepadKey[] gamepadKeys = new GamepadKey[12];
        List<Keys> keys = new List<Keys>();

        static List<InputGamepad> playerGamepads = null;

        internal static List<InputGamepad> PlayerGamepads
        {
            get 
            {
                LoadComponet();
                return InputGamepad.playerGamepads; 
            }
        }

        public static void LoadComponet()
        {
            if (playerGamepads != null)
            {
                return;
            }
            playerGamepads = new List<InputGamepad>(4);
            List<string> Lines = Service.ReadTextFile("defaultKeyboardInput.txt");
            defultInit_1();
            defultInit_2();

            for (int i = 0; i < Lines.Count; i++)
            {
                string line = Lines[i];
                Change(i, line);
            }
        }

        private static void Change(int index, string line)
        {
            InputGamepad rs = playerGamepads[index];
            if (null == rs)
                rs = new InputGamepad();
            string[] args = line.Split(' ');
            for (int i = 1; i < args.Length; i++)
            {
                Keys tmpKey = rs.keys[i - 1];
                tmpKey = (Keys)Int32.Parse(args[i]);
            }
        }


        private InputGamepad ()
        {
            gamepadKeys[0] = Up = new GamepadKey();
            gamepadKeys[1] = Left = new GamepadKey();
            gamepadKeys[2] = Right = new GamepadKey();
            gamepadKeys[3] = Down = new GamepadKey();
            gamepadKeys[4] = LT = new GamepadKey();
            gamepadKeys[5] = RT = new GamepadKey();
            gamepadKeys[6] = A =new GamepadKey();
            gamepadKeys[7] = B =new GamepadKey();
            gamepadKeys[8] = X =new GamepadKey();
            gamepadKeys[9] = Y = new GamepadKey();
            gamepadKeys[10] = Start = new GamepadKey();
            gamepadKeys[11] = Select= new GamepadKey();
                
        }
        private static InputGamepad defultInit_1()
        {
            InputGamepad rs = playerGamepads[0];
            if (null == rs)
                rs = new InputGamepad();
            rs.keys.Add(Keys.Up);
            rs.keys.Add(Keys.Left );
            rs.keys.Add(Keys.Right);
            rs.keys.Add(Keys.Down );
            rs.keys.Add(Keys.Q); 
            rs.keys.Add(Keys.R); 
            rs.keys.Add(Keys.A);
            rs.keys.Add(Keys.S);
            rs.keys.Add(Keys.D); 
            rs.keys.Add(Keys.F); 
            rs.keys.Add(Keys.Enter);
            rs.keys.Add(Keys.RightShift); 
            return rs;
        }

        private static InputGamepad defultInit_2()
        {
            InputGamepad rs = playerGamepads[1];
            if (null == rs)
                rs = new InputGamepad();
            rs.keys.Add(Keys.U);
            rs.keys.Add(Keys.K);  
            rs.keys.Add(Keys.J);    
            rs.keys.Add(Keys.LeftAlt );   
            rs.keys.Add(Keys.RightAlt );   
            rs.keys.Add(Keys.V );  
            rs.keys.Add(Keys.B );   
            rs.keys.Add(Keys.N );  
            rs.keys.Add(Keys.M );  
            rs.keys.Add(Keys.Enter );   
            rs.keys.Add(Keys.RightControl );  

            return rs;
        }
        public void Update()
        {
            for (int i = 0; i<keys.Count; i++)
            {
                KeyboardState state = Keyboard.GetState();
                gamepadKeys[i].Update(state.IsKeyDown(keys[i]));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DragonBall_ZZZ
{
    public class Global
    {
        public static int CELL_WIDTH = 150;
        public static int CELL_HEIGHT = 150;

        public static int GameWidth = 1000;
        public static int GameHeight = 600;

        public static int viTriAnhDongMenu_X = 360;
        public static int viTriAnhDongMenu_Y = 120;

        public static int viTriButton_X = 40;
        public static int viTriButton_y = 350;
        public static int khoangCachButton = 35;

        public static int kichThuocAnhMenuDong = 1870;
    }
}

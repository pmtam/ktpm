﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

// -----------------------------------------------------------------------
// <copyright file="$safeitemrootname$.cs" company="$registeredorganization$">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
namespace DragonBall_ZZZ
{
    public abstract class AbstractPathUpdater
    {
        public virtual Vector2 Update()
        {
            return Vector2.Zero;
        }
    }
}

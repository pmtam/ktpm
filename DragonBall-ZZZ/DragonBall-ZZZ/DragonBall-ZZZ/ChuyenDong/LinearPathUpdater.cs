﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DragonBall_ZZZ.ChuyenDong
{
    public class LinearPathUpdater : AbstractPathUpdater
    {
        private Vector2 _P;

        public Vector2 P
        {
            get { return _P; }
            set { _P = value; }
        }
        private Vector2 _V;

        public Vector2 V
        {
            get { return _V; }
            set { _V = value; }
        }

        private Vector2 _CurrentPos;
        public LinearPathUpdater(Vector2 p, Vector2 v)
        {
            P = p;
            V = v;
            _CurrentPos = p;
        }

        public override Microsoft.Xna.Framework.Vector2 Update()
        {
            _CurrentPos.X -= V.X;
            _CurrentPos.Y -= V.Y;
            return _CurrentPos;
            //return base.Update();
        }
    }
}

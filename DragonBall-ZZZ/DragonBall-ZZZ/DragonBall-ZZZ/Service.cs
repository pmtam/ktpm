﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using System.IO;

namespace DragonBall_ZZZ
{
    class Service
    {
        public static List<string> ReadTextFile(string filePath)
        {
            List<string> rs = new List<string>();
            FileInfo f1 = new FileInfo(filePath);
            if (f1.Exists == true)
            {
                StreamReader sr = File.OpenText(filePath);
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    rs.Add(line);
                }
                sr.Close();
            }

            return rs;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using DragonBall_ZZZ.ChuyenDong;

namespace DragonBall_ZZZ
{
    class Menu: VisibleGameEntity
    {
        My2DSprite AnhChinh;
        List<My2DSprite> HinhDongMenu;
        int nHinhDongMenu = 2;
        List<My2DSprite> Button;
        int nButton;
        Song NhacNen;
        Texture2D BackGround;
        bool play = false;

        public void Load(ContentManager content)
        {
            BackGround = content.Load<Texture2D>(@"HinhAnh\Menu\Main");

            /////////////////////////////////////////////////////

            NhacNen = content.Load<Song>(@"AmThanh\NhacNenMenu\NhacNen");
            
            //MediaPlayer.Play(NhacNen);

            /////////////////////////////////////


            Texture2D[] menuTemp = new Texture2D[1];
            menuTemp[0] = content.Load<Texture2D>(@"HinhAnh\Menu\Main0");

            HinhDongMenu = new List<My2DSprite>();
            for (int i = 0; i < nHinhDongMenu; i++)
            {
                HinhDongMenu.Add(new My2DSprite(menuTemp, 0+i*1503, 0));
                AbstractPathUpdater pathUpdate = new ChayToiChayLui(new Vector2((float)0 + i * Global.kichThuocAnhMenuDong, (float)0),
                    new Vector2((float)0.5, (float)0));
                HinhDongMenu[i].PathUpdate = pathUpdate;
            }

            ////////////////////////////


            Texture2D[] temp = new Texture2D[3];

            temp[0] = content.Load<Texture2D>(@"HinhAnh\Menu\Main1");
            temp[1] = content.Load<Texture2D>(@"HinhAnh\Menu\Main2");
            temp[2] = content.Load<Texture2D>(@"HinhAnh\Menu\Main3");

            AnhChinh = new My2DSprite(temp, Global.viTriAnhDongMenu_X, Global.viTriAnhDongMenu_Y);
            AnhChinh.Delay = 80 * 16;

            //////////////////////////////


            My2DSprite Play;
            My2DSprite Load;
            My2DSprite Settings;
            My2DSprite Quit;
            Texture2D[] temp00 = new Texture2D[1];
            Texture2D[] temp01 = new Texture2D[1];
            Texture2D[] temp02 = new Texture2D[1];
            Texture2D[] temp03 = new Texture2D[1];

            temp00[0] = content.Load<Texture2D>(@"HinhAnh\Button\Play");
            temp01[0] = content.Load<Texture2D>(@"HinhAnh\Button\Load");
            temp02[0] = content.Load<Texture2D>(@"HinhAnh\Button\Settings");
            temp03[0] = content.Load<Texture2D>(@"HinhAnh\Button\Quit");

            Play = new My2DSprite(temp00, Global.viTriButton_X, Global.viTriButton_y);
            Load = new My2DSprite(temp01, Global.viTriButton_X, Global.viTriButton_y + Global.khoangCachButton);
            Settings = new My2DSprite(temp02, Global.viTriButton_X, Global.viTriButton_y + Global.khoangCachButton*2);
            Quit = new My2DSprite(temp03, Global.viTriButton_X, Global.viTriButton_y + Global.khoangCachButton*3);

            Button = new List<My2DSprite>();
            nButton = 4;
            Button.Add(Play);
            Button.Add(Load);
            Button.Add(Settings);
            Button.Add(Quit);
        }
        public void Update(GameTime gameTime, MouseState ms)
        {
            if (play == false)
            {
                MediaPlayer.Play(NhacNen);
                play = true;
            }

            if (ms.LeftButton == ButtonState.Pressed)
            {
                //bPressed = true;
                int idx = -1;
                double X = ms.X;
                double Y = ms.Y;

                idx = GetMenu(idx, X, Y);

                if (idx != -1)
                {
                    for (int i = 0; i < nButton; i++)
                    {
                        Button[i].State = (i == idx) ? 1 : 0;
                    }
                }
            }

            for (int i = 0; i < nHinhDongMenu; i++)
            {
                HinhDongMenu[i].Update(gameTime);
            }

            AnhChinh.Update(gameTime);

            for (int i = 0; i < nButton; i++)
            {
                Button[i].Update(gameTime);
            }

        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch, GraphicsDeviceManager graphics)
        {
            // ve backGround
            for (int i = 0; i < nHinhDongMenu; i++)
            {
                HinhDongMenu[i].Draw(gameTime, spriteBatch, Color.White);
            }
            spriteBatch.Draw(BackGround, new Rectangle(0, 0, graphics.PreferredBackBufferWidth,
                graphics.PreferredBackBufferHeight), Color.White);
            
            //ve button
            for (int i = 0; i < nButton; i++)
            {
                Button[i].Draw(gameTime, spriteBatch, Color.White);
            }

            AnhChinh.Draw(gameTime, spriteBatch, Color.White);
        }

        private int GetMenu(int idx, double X, double Y)
        {
            for (int i = 0; i < nButton; i++)
            {
                if (Button[i].IsSelected(X, Y))
                    idx = i;
            }
            return idx;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DragonBall_ZZZ
{
    public abstract class VisibleGameEntity : GameEntity
    {
        public List<My2DSprite> _Sprites;

        protected List<My2DSprite> Sprites
        {
            get { return _Sprites; }
            set { _Sprites = value; }
        }


        /*public VisibleGameEntity(float _left, float _top)
        {
            Texture2D[] texture;
            texture = new Texture2D[1];
            
        }*/
        public virtual void Update(GameTime gameTime)
        {
            foreach (My2DSprite sprite in _Sprites)
                sprite.Update(gameTime);
            base.Update(gameTime);
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (My2DSprite sprite in _Sprites)
                sprite.Draw(gameTime, spriteBatch, Color.White);
        }
    }
}

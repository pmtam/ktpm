﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace DragonBall_ZZZ
{
    public class GamepadKey
    {

        private bool isDown = false;

        public bool IsDown
        {
            get { return isDown; }
        }

        private bool isDownBefore = false;
        
        public bool IsUp
        {
            get { return !isDown; }
        }

        private bool IsUpBefore
        {
            get { return !isDownBefore; }
        }

        public bool IsPress
        {
            get { return (IsUpBefore && isDown); }
        }

        public bool IsRelease
        {
            get { return (isDownBefore && IsUp); }
        }

        public void Update(bool newKeyStatus)
        {
            isDownBefore = isDown;
            isDown = newKeyStatus;
        }
    }
}

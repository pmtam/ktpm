﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using DragonBall_ZZZ.Screen;

namespace DragonBall_ZZZ.FightAction.FightActionGoku
{
    class KickbyfootwhenjumpGokuleft : FightActionGokuLeft
    {
        public List<Texture2D> list_hinhnhayquynhchan = new List<Texture2D>();

        public bool flag = true;
        protected int soluonghinhnhaydachan = 1;

        public override void Load(ContentManager contend, int chisonhanvat)
        {
            if (chisonhanvat == 1)
            {
                list_hinhnhayquynhchan.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhayquynhchan/hinhnhayquynhchan"));
            }
            else
            {
                list_hinhnhayquynhchan.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhayquynhchan/hinhnhayquynhchan"));
            }
        }

        public override void DiChuyen()
        {
            dangthuchien = true;
            
        }

        public override void Draw(SpriteBatch spriteBatch, ref Vector2 viTri, Nhanvat nhanvat)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(list_hinhnhayquynhchan[0], viTri, Microsoft.Xna.Framework.Color.White);
            spriteBatch.End();
            //solan_vacham = 0;
            dangthuchien = false;
            //instanceaction.Stop();
        }



        public override void capNhat_X_Tick(object sender, EventArgs e)
        {
            ;
        }



        public override bool IsFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu, Vector2 vitri)
        {
            if (dangthuchien)
            {
                if (nhanvatdoithu.Dangnhaytoi)
                {
                    Microsoft.Xna.Framework.Color[] personTexture1Data;
                    Microsoft.Xna.Framework.Color[] personTexture2Data;


                    personTexture1Data =
                        new Microsoft.Xna.Framework.Color[list_hinhnhayquynhchan[vitrihinh].Width * list_hinhnhayquynhchan[vitrihinh].Height];
                    list_hinhnhayquynhchan[vitrihinh].GetData(personTexture1Data);

                    personTexture2Data = new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width * nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height];
                    nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].GetData(personTexture2Data);


                    System.Drawing.Rectangle personRectangle =
new System.Drawing.Rectangle((int)vitri.X, (int)vitri.Y,
list_hinhnhayquynhchan[vitrihinh].Width, list_hinhnhayquynhchan[vitrihinh].Height);

                    System.Drawing.Rectangle blockRectangle =
new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width, nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height);

                    if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                blockRectangle, personTexture2Data))
                    {
                        fight_type = 1;
                        solan_vacham++;
                        return true;
                    }
                }
                else
                {
                    if (nhanvatdoithu.Dangnhay)
                    {

                        Microsoft.Xna.Framework.Color[] personTexture1Data;
                        Microsoft.Xna.Framework.Color[] personTexture2Data;


                        personTexture1Data =
                            new Microsoft.Xna.Framework.Color[list_hinhnhayquynhchan[vitrihinh].Width * list_hinhnhayquynhchan[vitrihinh].Height];
                        list_hinhnhayquynhchan[vitrihinh].GetData(personTexture1Data);

                        personTexture2Data = new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width * nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height];
                        nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].GetData(personTexture2Data);


                        System.Drawing.Rectangle personRectangle =
new System.Drawing.Rectangle((int)vitri.X, (int)vitri.Y,
list_hinhnhayquynhchan[vitrihinh].Width, list_hinhnhayquynhchan[vitrihinh].Height);

                        System.Drawing.Rectangle blockRectangle =
    new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
    nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width, nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height);

                        if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                    blockRectangle, personTexture2Data))
                        {
                            fight_type = 1;
                            solan_vacham++;

                            return true;
                        }
                    }
                    else
                    {
                        if (nhanvatdoithu.Dangchuyendong)
                        {
                            if (nhanvatdoithu.huongChuyenDong == HanhDong.DiChuyenTraiQuaPhai)
                            {
                                fight_type = 2;
                                return true;
                            }
                            else
                            {
                                Microsoft.Xna.Framework.Color[] personTexture1Data;
                                Microsoft.Xna.Framework.Color[] personTexture2Data;

                                personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhnhayquynhchan[vitrihinh].Width * list_hinhnhayquynhchan[vitrihinh].Height];
                                list_hinhnhayquynhchan[vitrihinh].GetData(personTexture1Data);

                                personTexture2Data =
                                    new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width * nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height];
                                nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].GetData(personTexture2Data);

                                System.Drawing.Rectangle personRectangle =
        new System.Drawing.Rectangle((int)vitri.X - Nhanvat.dochenhlechX, (int)vitri.Y,
        list_hinhnhayquynhchan[vitrihinh].Width, list_hinhnhayquynhchan[vitrihinh].Height);

                                System.Drawing.Rectangle blockRectangle =
            new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
            nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width, nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height);

                                if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                            blockRectangle, personTexture2Data))
                                {
                                    fight_type = 0;
                                    solan_vacham++;

                                    return true;
                                }
                            }
                        }
                        else
                        {
                            if (!nhanvatdoithu.Dangchuyendong)
                            {

                                Microsoft.Xna.Framework.Color[] personTexture1Data;
                                Microsoft.Xna.Framework.Color[] personTextture2Data;

                                personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhnhayquynhchan[vitrihinh].Width * list_hinhnhayquynhchan[vitrihinh].Height];
                                list_hinhnhayquynhchan[vitrihinh].GetData(personTexture1Data);

                                personTextture2Data =
                                    new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width * nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height];
                                nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].GetData(personTextture2Data);

                                System.Drawing.Rectangle personRectangle =
        new System.Drawing.Rectangle((int)vitri.X, (int)vitri.Y,
        list_hinhnhayquynhchan[vitrihinh].Width, list_hinhnhayquynhchan[vitrihinh].Height);

                                System.Drawing.Rectangle blockRectangle =
            new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
            nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width, nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height);

                                if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                            blockRectangle, personTextture2Data))
                                {
                                    fight_type = 0;
                                    solan_vacham++;

                                    return true;
                                }
                            }
                        }
                    }

                }
            }
            return false;
        }

        public override void SolveFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu)
        {
            switch (fight_type)
            {
                case 0:
                    {
                        if (!nhanvatdoithu.die && solan_vacham == 1)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                Tiengvacham.Play();
                            }
                            nhanvatdoithu.biquynh1 = true;
                            nhanvatdoithu.blood -= decreaseblood;
                            nhanvat.energy += increaseenergy * 2;
                            if (nhanvat.energy >= Nhanvat.max_energy)
                            {
                                nhanvat.energy = Nhanvat.max_energy;
                            }

                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width >= Nhanvat.Game.Window.ClientBounds.Width)
                                {
                                    nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X - nhanvat.list_hinhdungyen[0].Width + Nhanvat.dochenhlechX;
                                }
                            }
                        }
                        
                        fight_type = -1;
                    }
                    break;
                case 1:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                Tiengvacham.Play();
                            }
                            nhanvatdoithu.blood -= decreaseblood;
                            nhanvat.energy += increaseenergy * 2;
                            if (nhanvat.energy >= Nhanvat.max_energy)
                            {
                                nhanvat.energy = Nhanvat.max_energy;
                            }
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width >= Nhanvat.Game.Window.ClientBounds.Width)
                                {
                                    nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X - nhanvat.list_hinhdungyen[0].Width + Nhanvat.dochenhlechX;
                                }
                            }
                            nhanvatdoithu.dangnhaylui = true;
                            nhanvatdoithu.Dangnhaytoi = false;
                            nhanvatdoithu.Dangnhay = false;
                            nhanvatdoithu.vitridangnhaytoi = nhanvatdoithu.soluonghinhnhaytoi - 1;
                            nhanvat.vitridangnhaytoi = 0;
                        }
                       
                        fight_type = -1;
                    }
                    break;
                case 2:
                    {
                        nhanvatdoithu.Dangdo = true;
                        nhanvat.energy += increaseenergy;
                        if (nhanvat.energy >= Nhanvat.max_energy)
                        {
                            nhanvat.energy = Nhanvat.max_energy;
                        }
                       
                        fight_type = -1;
                    }
                    break;
            }
        }
    }
}

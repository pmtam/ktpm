﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using DragonBall_ZZZ.Screen;

namespace DragonBall_ZZZ.FightAction.FightActionGoku
{
    class KickbyfootwhenjumpGokuright: FightActionGokuRight
    {
        public List<Texture2D> list_hinhnhayquynhchan = new List<Texture2D>();
        //public bool flag = true;
        protected int soluonghinhnhaydachan = 1;
        public override void Load(ContentManager contend, int chisonhanvat)
        {
            if (chisonhanvat == 2)
            {
                list_hinhnhayquynhchan.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhnhayquynhchan/hinhnhayquynhchan"));
            }
            else
            {
                list_hinhnhayquynhchan.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhnhayquynhchan/hinhnhayquynhchan"));
            }
        }

        public override void DiChuyen()
        {
            dangthuchien = true;
        }

        public override void Draw(SpriteBatch spriteBatch, ref Vector2 viTri, Nhanvat nhanvat)
        {
                viTri.X = viTri.X + nhanvat.list_hinhnhaytaicho[nhanvat.vitrihinhnhaytaicho].Width - list_hinhnhayquynhchan[0].Width;
                spriteBatch.Begin();
                spriteBatch.Draw(list_hinhnhayquynhchan[0], viTri, Microsoft.Xna.Framework.Color.White);
                spriteBatch.End();
                viTri.X -= nhanvat.list_hinhnhaytaicho[nhanvat.vitrihinhnhaytaicho].Width - list_hinhnhayquynhchan[0].Width;
                dangthuchien = false;       
        }
        public override void capNhat_X_Tick(object sender, EventArgs e)
        {
            ;
        }

        public override bool IsFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu, Vector2 vitri)
        {
            if (dangthuchien)
            {
                if (nhanvatdoithu.Dangnhaytoi)
                {
                    if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width >= nhanvat.viTri.X + nhanvat.list_hinhdungyen[0].Width - list_hinhnhayquynhchan[vitrihinh].Width && (nhanvatdoithu.viTri.Y + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height >= nhanvat.viTri.Y + Nhanvat.dochenhlechY))
                    {
                        fight_type = 1;
                        solan_vacham++;
                        return true;
                    }
                }

                    else
                    {
                        if (nhanvatdoithu.Dangnhay)
                        {
                            if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width >= nhanvat.viTri.X + nhanvat.list_hinhdungyen[0].Width - list_hinhnhayquynhchan[vitrihinh].Width && (nhanvatdoithu.viTri.Y + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height >= nhanvat.viTri.Y + Nhanvat.dochenhlechY))
                            {
                                fight_type = 1;
                                solan_vacham++;
                                return true;
                            }
                        }
                        else
                        {
                            if (nhanvatdoithu.Dangchuyendong)
                            {
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width >= nhanvat.viTri.X + nhanvat.list_hinhdungyen[0].Width - list_hinhnhayquynhchan[vitrihinh].Width)
                                {
                                    if (nhanvatdoithu.flag || nhanvatdoithu.huongChuyenDong == HanhDong.DiChuyenPhaiQuaTrai)
                                    {
                                        fight_type = 2;
                                        nhanvatdoithu.flag = false;
                                        return true;
                                    }
                                    else
                                    {
                                        fight_type = 0;
                                        solan_vacham++;
                                        return true;
                                    }
                                }
                            }
                            else
                            {
                                if (!nhanvatdoithu.Dangchuyendong)
                                {
                                    if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width >= nhanvat.viTri.X + nhanvat.list_hinhdungyen[0].Width - list_hinhnhayquynhchan[vitrihinh].Width)
                                    {
                                        fight_type = 0;
                                        solan_vacham++;
                                        return true;
                                    }
                                }
                            }
                        }
                    }
        
            }
                nhanvat.energy += increaseenergy;
                if (nhanvat.energy >= Nhanvat.max_energy)
                {
                    nhanvat.energy = Nhanvat.max_energy;
                }
            return false;
        }
       

        public override void SolveFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu)
        {
            switch (fight_type)
            {
                case 0:
                    {
                        if (!nhanvatdoithu.die && solan_vacham == 1)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                tiengvacham.Play();
                            }
                            nhanvat.energy += increaseenergy * 2;
                            if (nhanvat.energy >= Nhanvat.max_energy)
                            {
                                nhanvat.energy = Nhanvat.max_energy;
                            }
                            nhanvatdoithu.biquynh1 = true;
                            nhanvatdoithu.blood -= decreaseblood;

                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X - nhanvat.list_hinhbinga[nhanvat.list_hinhbinga.Count - 1].Width < 0)
                                {
                                    nhanvatdoithu.viTri.X = 0;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width - Nhanvat.dochenhlechX;
                                }
                            }
                        }
                        fight_type = -1;
                    }
                    break;
                case 1:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                tiengvacham.Play();
                            }
                            nhanvatdoithu.blood -= decreaseblood;
                            nhanvat.energy += increaseenergy * 2;
                            if (nhanvat.energy >= Nhanvat.max_energy)
                            {
                                nhanvat.energy = Nhanvat.max_energy;
                            }
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X - nhanvat.list_hinhbinga[nhanvat.list_hinhbinga.Count - 1].Width < 0)
                                {
                                    nhanvatdoithu.viTri.X = 0;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width - Nhanvat.dochenhlechX;
                                }
                            }
                            nhanvatdoithu.dangnhaylui = true;
                            nhanvatdoithu.Dangnhaytoi = false;
                            nhanvatdoithu.Dangnhay = false;
                            nhanvatdoithu.vitridangnhaytoi = 8;
                            nhanvat.vitridangnhaytoi = 0;
                        }
                        fight_type = -1;
                    }
                    break;
                case 2:
                    {
                        nhanvatdoithu.Dangdo = true;
                        nhanvat.energy += increaseenergy;
                        if (nhanvat.energy >= Nhanvat.max_energy)
                        {
                            nhanvat.energy = Nhanvat.max_energy;
                        }
                        fight_type = -1;
                    }
                    break;
            }
        }

    }
}

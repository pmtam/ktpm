﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using DragonBall_ZZZ.Screen;

namespace DragonBall_ZZZ.FightAction.FightActionGoku
{
    class KickbyfootGokuright: FightActionGokuRight
    {
        protected List<Texture2D> list_hinhdachan = new List<Texture2D>();
        private bool flag = true;
        protected int soluonghinhdachan = 7;
        public override void Load(ContentManager contend, int chisonhanvat)
        {
            if (OptionsMenuScreen.currentSound == 0)
            {
                soundaction = contend.Load<SoundEffect>("amthanhdachan");
                instanceaction = soundaction.CreateInstance();
            }
            if (chisonhanvat == 2)
            {
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdachan/hinhdachan-1"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdachan/hinhdachan-2"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdachan/hinhdachan-3"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdachan/hinhdachan-4"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdachan/hinhdachan-5"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdachan/hinhdachan-6"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT2BENPHAI/hinhdachan/hinhdachan-7"));
            }
            else
            {
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdachan/hinhdachan-1"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdachan/hinhdachan-2"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdachan/hinhdachan-3"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdachan/hinhdachan-4"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdachan/hinhdachan-5"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdachan/hinhdachan-6"));
                list_hinhdachan.Add(contend.Load<Texture2D>("HINHNHANVAT1BENPHAI/hinhdachan/hinhdachan-7"));
            }
        }

        public override void DiChuyen()
        {
            dangthuchien = true;
        }

        public override void Draw(SpriteBatch spriteBatch, ref Vector2 viTri, Nhanvat nhanvat)
        {
            if (vitrihinh >= 3 && vitrihinh <= 5)
            {
                viTri.Y = Nhanvat.toadoYbandau;
            }
            else
            {
                viTri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - list_hinhdachan[vitrihinh].Height;
            }
            viTri.X = viTri.X + nhanvat.list_hinhdungyen[0].Width - list_hinhdachan[vitrihinh].Width;
            spriteBatch.Begin();
            spriteBatch.Draw(list_hinhdachan[vitrihinh], viTri, Microsoft.Xna.Framework.Color.White);
            spriteBatch.End();
            viTri.X -= nhanvat.list_hinhdungyen[0].Width - list_hinhdachan[vitrihinh].Width;
            if (vitrihinh >= 3 && vitrihinh <= 5)
            {
                viTri.Y = Nhanvat.toadoYbandau;
            }
            else
            {
                viTri.Y = Nhanvat.toadoYbandau;
            }
        }
        public override void capNhat_X_Tick(object sender, EventArgs e)
        {
            if (vitrihinh < soluonghinhdachan - 1)
            {
                vitrihinh++;
            }
            else
            {
                vitrihinh = 0;
                dangthuchien = false;
                solan_vacham = 0;
                if (OptionsMenuScreen.currentSound == 0)
                {
                    instanceaction.Stop();
                }
                flag = true;
            }
        }

        public override bool IsFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu, Vector2 vitri)
        {
            if (dangthuchien)
            {
                if (vitrihinh == 4)
                {
                    if (OptionsMenuScreen.currentSound == 0)
                    {
                        instanceaction.Play();
                    }
                    if (nhanvatdoithu.Dangnhaytoi)
                    {
                        if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width >= nhanvat.viTri.X + nhanvat.list_hinhdungyen[0].Width - list_hinhdachan[vitrihinh].Width && (nhanvatdoithu.viTri.Y + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height >= nhanvat.viTri.Y + Nhanvat.dochenhlechY))
                        {
                            fight_type = 1;
                            solan_vacham++;
                            return true;
                        }
                    }
                    else
                    {
                        if (nhanvatdoithu.Dangnhay)
                        {
                            if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width >= nhanvat.viTri.X + nhanvat.list_hinhdungyen[0].Width - list_hinhdachan[vitrihinh].Width && (nhanvatdoithu.viTri.Y + nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height >= nhanvat.viTri.Y + Nhanvat.dochenhlechY))
                            {
                                fight_type = 1;
                                solan_vacham++;
                                return true;
                            }
                        }
                        else
                        {
                            if (nhanvatdoithu.Dangchuyendong)
                            {
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width >= nhanvat.viTri.X + nhanvat.list_hinhdungyen[0].Width - list_hinhdachan[vitrihinh].Width)
                                {

                                    if (nhanvatdoithu.flag || nhanvatdoithu.huongChuyenDong == HanhDong.DiChuyenPhaiQuaTrai)
                                    {
                                        fight_type = 2;
                                        nhanvatdoithu.flag = false;
                                        return true;
                                    }
                                    else
                                    {
                                        fight_type = 0;
                                        solan_vacham++;
                                        return true;
                                    }
                                }
                            }
                            else
                            {
                                if (!nhanvatdoithu.Dangchuyendong)
                                {
                                    if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width >= nhanvat.viTri.X + nhanvat.list_hinhdungyen[0].Width - list_hinhdachan[vitrihinh].Width)
                                    {
                                        fight_type = 0;
                                        solan_vacham++;
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (flag && vitrihinh == 4)
            {
                nhanvat.energy += increaseenergy;
                if (nhanvat.energy >= Nhanvat.max_energy)
                {
                    nhanvat.energy = Nhanvat.max_energy;
                }
                flag = false;
            }
            return false;
        }

        public override void SolveFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu)
        {
            switch (fight_type)
            {
                case 0:
                    {
                        if (!nhanvatdoithu.die && solan_vacham == 1)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                tiengvacham.Play();
                            }
                            nhanvatdoithu.biquynh2 = true;
                            nhanvatdoithu.blood -= decreaseblood;
                            nhanvat.energy += increaseenergy*2;
                            if (nhanvat.energy >= Nhanvat.max_energy)
                            {
                                nhanvat.energy = Nhanvat.max_energy;
                            }
                            
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width >= Nhanvat.Game.Window.ClientBounds.Width)
                                {
                                    nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X - nhanvat.list_hinhdungyen[0].Width + Nhanvat.dochenhlechX;
                                }
                            }
                        }
                        fight_type = -1;
                    }
                    break;
                case 1:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                tiengvacham.Play();
                            }
                            nhanvatdoithu.blood -= decreaseblood;
                            nhanvat.energy += increaseenergy*2;
                            if (nhanvat.energy >= Nhanvat.max_energy)
                            {
                                nhanvat.energy = Nhanvat.max_energy;
                            }

                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width >= Nhanvat.Game.Window.ClientBounds.Width)
                                {
                                    nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X - nhanvat.list_hinhdungyen[0].Width + Nhanvat.dochenhlechX;
                                }
                            }
                            nhanvatdoithu.dangnhaylui = true;
                            nhanvatdoithu.Dangnhaytoi = false;
                            nhanvatdoithu.Dangnhay = false;
                            nhanvatdoithu.vitridangnhaytoi = nhanvatdoithu.soluonghinhnhaytoi - 1;
                            nhanvat.vitridangnhaytoi = 0;
                        }
                        fight_type = -1;
                    }
                    break;
                case 2:
                    {
                        nhanvatdoithu.Dangdo = true;
                        nhanvat.energy += increaseenergy;
                        if (nhanvat.energy >= Nhanvat.max_energy)
                        {
                            nhanvat.energy = Nhanvat.max_energy;
                        }
                        fight_type = -1;
                    }
                    break;
            }
        }

    }
}

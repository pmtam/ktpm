﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace DragonBall_ZZZ.FightAction.FightActionGoku
{
    class BeatbyhandwhenjumpGokuleft: FightActionGokuLeft
    {
        public List<Texture2D> list_hinhnhayquynhtay = new List<Texture2D>();
        protected int soluonghinhnhayquynhtay = 1;
        
        public override void Load(ContentManager contend, int chisonhanvat)
        {
            if (chisonhanvat == 1)
            {
                list_hinhnhayquynhtay.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhnhayquynhtay/hinhnhayquynhtay-1"));
            }
            else
            {   
                list_hinhnhayquynhtay.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhayquynhtay/hinhnhayquynhtay-1"));
            }
        }

        public override void DiChuyen()
        {
            dangthuchien = true;
        }

        public override void Draw(SpriteBatch spriteBatch, ref Vector2 viTri, Nhanvat nhanvat)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(list_hinhnhayquynhtay[vitrihinh], viTri, Microsoft.Xna.Framework.Color.White);
            spriteBatch.End();
            dangthuchien = false;

        }

        public override void capNhat_X_Tick(object sender, EventArgs e)
        {
            ;
        }

        public override bool IsFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu, Vector2 viTri)
        {
            if (dangthuchien)
            {
                if (nhanvatdoithu.Dangnhaytoi)
                {
                    
                    Microsoft.Xna.Framework.Color[] personTexture1Data;
                    Microsoft.Xna.Framework.Color[] personTexture2Data;


                    personTexture1Data =
                        new Microsoft.Xna.Framework.Color[list_hinhnhayquynhtay[vitrihinh].Width * list_hinhnhayquynhtay[vitrihinh].Height];
                    list_hinhnhayquynhtay[vitrihinh].GetData(personTexture1Data);

                    personTexture2Data = new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width * nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height];
                    nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].GetData(personTexture2Data);


                    System.Drawing.Rectangle personRectangle =
new System.Drawing.Rectangle((int)nhanvat.viTri.X, (int)nhanvat.viTri.Y,
list_hinhnhayquynhtay[vitrihinh].Width, list_hinhnhayquynhtay[vitrihinh].Height);

                    System.Drawing.Rectangle blockRectangle =
new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width, nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height);

                    if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                blockRectangle, personTexture2Data))
                    {
                        fight_type = 1;
                        solan_vacham++;
                        return true;
                    }
                }
                else
                {
                    if (nhanvatdoithu.Dangnhay)
                    {
                       
                        Microsoft.Xna.Framework.Color[] personTexture1Data;
                        Microsoft.Xna.Framework.Color[] personTexture2Data;


                        personTexture1Data =
                            new Microsoft.Xna.Framework.Color[list_hinhnhayquynhtay[vitrihinh].Width * list_hinhnhayquynhtay[vitrihinh].Height];
                        list_hinhnhayquynhtay[vitrihinh].GetData(personTexture1Data);

                        personTexture2Data = new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width * nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height];
                        nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].GetData(personTexture2Data);


                        System.Drawing.Rectangle personRectangle =
new System.Drawing.Rectangle((int)nhanvat.viTri.X - Nhanvat.dochenhlechX, (int)nhanvat.viTri.Y,
list_hinhnhayquynhtay[vitrihinh].Width, list_hinhnhayquynhtay[vitrihinh].Height);

                        System.Drawing.Rectangle blockRectangle =
    new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
    nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width, nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height);

                        if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                    blockRectangle, personTexture2Data))
                        {
                            fight_type = 1;
                            solan_vacham++;
                            
                            return true;
                        }
                    }
                    else
                    {
                        if (nhanvatdoithu.Dangchuyendong)
                        {
                            
                            Microsoft.Xna.Framework.Color[] personTexture1Data;
                            Microsoft.Xna.Framework.Color[] personTexture2Data;

                            personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhnhayquynhtay[vitrihinh].Width * list_hinhnhayquynhtay[vitrihinh].Height];
                            list_hinhnhayquynhtay[vitrihinh].GetData(personTexture1Data);

                            personTexture2Data =
                                new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width * nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height];
                            nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].GetData(personTexture2Data);

                            System.Drawing.Rectangle personRectangle =
    new System.Drawing.Rectangle((int)nhanvat.viTri.X - Nhanvat.dochenhlechX, (int)nhanvat.viTri.Y,
    list_hinhnhayquynhtay[vitrihinh].Width, list_hinhnhayquynhtay[vitrihinh].Height);

                            System.Drawing.Rectangle blockRectangle =
        new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
        nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width, nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height);

                            if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                        blockRectangle, personTexture2Data))
                            {
                                fight_type = 0;
                                solan_vacham++;
                                
                                return true;
                            }
                        }
                        else
                        {
                            if (!nhanvatdoithu.Dangchuyendong)
                            {
                                
                                Microsoft.Xna.Framework.Color[] personTexture1Data;
                                Microsoft.Xna.Framework.Color[] personTextture2Data;

                                personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhnhayquynhtay[vitrihinh].Width * list_hinhnhayquynhtay[vitrihinh].Height];
                                list_hinhnhayquynhtay[vitrihinh].GetData(personTexture1Data);

                                personTextture2Data =
                                    new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width * nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height];
                                nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].GetData(personTextture2Data);

                                System.Drawing.Rectangle personRectangle =
        new System.Drawing.Rectangle((int)nhanvat.viTri.X, (int)nhanvat.viTri.Y,
        list_hinhnhayquynhtay[vitrihinh].Width, list_hinhnhayquynhtay[vitrihinh].Height);

                                System.Drawing.Rectangle blockRectangle =
            new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
            nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width, nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height);

                                if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                            blockRectangle, personTextture2Data))
                                {
                                    fight_type = 0;
                                    solan_vacham++;
                                    
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }


        public override void SolveFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu)
        {
            switch (fight_type)
            {
                case 0:
                    {
                        if (!nhanvatdoithu.die && solan_vacham == 1)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                Tiengvacham.Play();
                            }
                            nhanvatdoithu.biquynh1 = true;
                            nhanvatdoithu.blood -= decreaseblood;
                            nhanvat.energy += increaseenergy * 2;
                            if (nhanvat.energy >= Nhanvat.max_energy)
                            {
                                nhanvat.energy = Nhanvat.max_energy;
                            }

                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width >= Nhanvat.Game.Window.ClientBounds.Width)
                                {
                                    nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X - nhanvat.list_hinhdungyen[0].Width + Nhanvat.dochenhlechX;
                                }
                            }
                        }
                        fight_type = -1;
                    }
                    break;
                case 1:
                    {
                        if (solan_vacham == 1 && nhanvatdoithu.die == false)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                Tiengvacham.Play();
                            }
                            nhanvatdoithu.blood -= decreaseblood;
                            nhanvat.energy += increaseenergy * 2;
                            if (nhanvat.energy >= Nhanvat.max_energy)
                            {
                                nhanvat.energy = Nhanvat.max_energy;
                            }
                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width >= Nhanvat.Game.Window.ClientBounds.Width)
                                {
                                    nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X - nhanvat.list_hinhdungyen[0].Width + Nhanvat.dochenhlechX;
                                }
                            }
                            nhanvatdoithu.dangnhaylui = true;
                            nhanvatdoithu.Dangnhaytoi = false;
                            nhanvatdoithu.Dangnhay = false;
                            nhanvatdoithu.vitridangnhaytoi = nhanvatdoithu.soluonghinhnhaytoi - 1;
                            nhanvat.vitridangnhaytoi = 0;
                        }
                        fight_type = -1;
                    }
                    break;
                case 2:
                    {
                        nhanvatdoithu.Dangdo = true;
                        nhanvat.energy += increaseenergy;
                        if (nhanvat.energy >= Nhanvat.max_energy)
                        {
                            nhanvat.energy = Nhanvat.max_energy;
                        }
                        fight_type = -1;
                    }
                    break;
            }
        }

    }
}

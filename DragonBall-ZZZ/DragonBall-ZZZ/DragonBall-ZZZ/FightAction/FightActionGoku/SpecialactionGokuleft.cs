﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using DragonBall_ZZZ.Screen;

namespace DragonBall_ZZZ.FightAction.FightActionGoku
{
    class SpecialactionGokuleft : FightActionGokuLeft
    {
        public List<Texture2D> list_hinhtuyetchieu = new List<Texture2D>();
        
        protected int soluonghinhtuyetchieu = 18;
        private bool flag = true;
        private int minenergy = 40;
        public override void Load(ContentManager contend, int chisonhanvat)
        {
            if (OptionsMenuScreen.currentSound == 0)
            {
                soundaction = contend.Load<SoundEffect>("AMTHANH/AMTHANHGOKU/specialGoku");
                instanceaction = soundaction.CreateInstance();
            }
            if (chisonhanvat == 1)
            {
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-1"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-2"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-3"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-4"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-5"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-6"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-7"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-8"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-9"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-10"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-11"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-12"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-13"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-14"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-15"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-16"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-17"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT1BENTRAI/hinhtuyetchieu/hinhtuyetchieu-18"));
            }
            else
            {
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-1"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-2"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-3"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-4"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-5"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-6"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-7"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-8"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-9"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-10"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-11"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-12"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-13"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-14"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-15"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-16"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-17"));
                list_hinhtuyetchieu.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhtuyetchieu/hinhtuyetchieu-18"));
            }
        }

        public override void DiChuyen()
        {
            dangthuchien = true;
        }

        public bool IsSatisfied(Nhanvat nhanvat)
        {
            if (nhanvat.energy >= minenergy)
                return true;
            return false;
        }

        public override void Draw(SpriteBatch spriteBatch, ref Vector2 viTri, Nhanvat nhanvat)
        {
                if (vitrihinh >= 9)
                {
                    viTri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - list_hinhtuyetchieu[8].Height;
                    spriteBatch.Begin();
                    spriteBatch.Draw(list_hinhtuyetchieu[8], viTri, Microsoft.Xna.Framework.Color.White);
                    viTri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - list_hinhtuyetchieu[vitrihinh].Height;
                    viTri.X += Nhanvat.dochenhlechX * 3;
                    spriteBatch.Draw(list_hinhtuyetchieu[vitrihinh], viTri, Microsoft.Xna.Framework.Color.White);
                    spriteBatch.End();
                    viTri.X -= Nhanvat.dochenhlechX * 3;
                    viTri.Y = Nhanvat.toadoYbandau;

                }
                else
                {
                    float vitriybandau = 0;
                    vitriybandau = viTri.Y;
                    viTri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - list_hinhtuyetchieu[vitrihinh].Height;
                    spriteBatch.Begin();
                    spriteBatch.Draw(list_hinhtuyetchieu[vitrihinh], viTri, Microsoft.Xna.Framework.Color.White);
                    spriteBatch.End();
                    viTri.Y = vitriybandau;
                }
            
        }

        public override void capNhat_X_Tick(object sender, EventArgs e)
        {
                if (vitrihinh < soluonghinhtuyetchieu - 1)
                {
                    vitrihinh++;
                    solan_vacham = 0;
                }
                else
                {
                    vitrihinh = 0;
                    dangthuchien = false;
                    if (OptionsMenuScreen.currentSound == 0)
                    {
                        instanceaction.Stop();
                    }
                    flag = true;
                }
        }

        public override bool IsFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu, Vector2 vitri)
        {
            if (dangthuchien)
            {
                if (flag)
                {
                    nhanvat.energy -= minenergy;
                    if (nhanvat.energy < 0)
                    {
                        nhanvat.energy = 0;
                    }
                    flag = false;
                }
                if (vitrihinh >= 9)
                {
                    if (OptionsMenuScreen.currentSound == 0)
                    {
                        instanceaction.Play();
                    }

                    if (nhanvatdoithu.Dangnhaytoi)
                    {
                        if (vitrihinh >= 9)
                        {
                            vitri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - list_hinhtuyetchieu[vitrihinh].Height;
                            vitri.X += Nhanvat.dochenhlechX * 3;
                        }
                        Microsoft.Xna.Framework.Color[] personTexture1Data;
                        Microsoft.Xna.Framework.Color[] personTexture2Data;


                        personTexture1Data =
                            new Microsoft.Xna.Framework.Color[list_hinhtuyetchieu[vitrihinh].Width * list_hinhtuyetchieu[vitrihinh].Height];
                        list_hinhtuyetchieu[vitrihinh].GetData(personTexture1Data);

                        personTexture2Data = new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width * nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height];
                        nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].GetData(personTexture2Data);


                        System.Drawing.Rectangle personRectangle =
    new System.Drawing.Rectangle((int)vitri.X, (int)vitri.Y,
    list_hinhtuyetchieu[vitrihinh].Width, list_hinhtuyetchieu[vitrihinh].Height);

                        System.Drawing.Rectangle blockRectangle =
    new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
    nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width, nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height);

                        if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                    blockRectangle, personTexture2Data))
                        {
                            fight_type = 1;
                            solan_vacham++;
                            if (vitrihinh >= 9)
                            {
                                vitri.X -= Nhanvat.dochenhlechX * 3;
                                vitri.Y = Nhanvat.toadoYbandau;
                            }
                            return true;
                        }
                    }
                    else
                    {
                        if (nhanvatdoithu.Dangnhay && vitrihinh >= 9)
                        {
                            if (vitrihinh >= 9)
                            {
                                vitri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - list_hinhtuyetchieu[vitrihinh].Height;
                                vitri.X += Nhanvat.dochenhlechX * 3;
                            }
                            Microsoft.Xna.Framework.Color[] personTexture1Data;
                            Microsoft.Xna.Framework.Color[] personTexture2Data;


                            personTexture1Data =
                                new Microsoft.Xna.Framework.Color[list_hinhtuyetchieu[vitrihinh].Width * list_hinhtuyetchieu[vitrihinh].Height];
                            list_hinhtuyetchieu[vitrihinh].GetData(personTexture1Data);

                            personTexture2Data = new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width * nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height];
                            nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].GetData(personTexture2Data);


                            System.Drawing.Rectangle personRectangle =
    new System.Drawing.Rectangle((int)vitri.X - Nhanvat.dochenhlechX, (int)vitri.Y,
    list_hinhtuyetchieu[vitrihinh].Width, list_hinhtuyetchieu[vitrihinh].Height);

                            System.Drawing.Rectangle blockRectangle =
        new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
        nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width, nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height);

                            if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                        blockRectangle, personTexture2Data))
                            {
                                fight_type = 1;
                                solan_vacham++;
                                if (vitrihinh >= 9)
                                {
                                    vitri.X -= Nhanvat.dochenhlechX * 3;
                                    vitri.Y = Nhanvat.toadoYbandau;
                                }
                                return true;
                            }
                        }
                        else
                        {
                            if (nhanvatdoithu.Dangchuyendong && vitrihinh >= 9)
                            {
                                if (nhanvatdoithu.huongChuyenDong == HanhDong.DiChuyenTraiQuaPhai)
                                {
                                    fight_type = 2;
                                    return true;
                                }
                                else
                                {
                                    if (vitrihinh >= 9)
                                    {
                                        vitri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - list_hinhtuyetchieu[vitrihinh].Height;
                                        vitri.X += Nhanvat.dochenhlechX * 3;
                                    }
                                    Microsoft.Xna.Framework.Color[] personTexture1Data;
                                    Microsoft.Xna.Framework.Color[] personTexture2Data;

                                    personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhtuyetchieu[vitrihinh].Width * list_hinhtuyetchieu[vitrihinh].Height];
                                    list_hinhtuyetchieu[vitrihinh].GetData(personTexture1Data);

                                    personTexture2Data =
                                        new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width * nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height];
                                    nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].GetData(personTexture2Data);

                                    System.Drawing.Rectangle personRectangle =
            new System.Drawing.Rectangle((int)vitri.X - Nhanvat.dochenhlechX, (int)vitri.Y,
            list_hinhtuyetchieu[vitrihinh].Width, list_hinhtuyetchieu[vitrihinh].Height);

                                    System.Drawing.Rectangle blockRectangle =
                new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width, nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height);

                                    if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                                blockRectangle, personTexture2Data))
                                    {
                                        fight_type = 0;
                                        solan_vacham++;
                                        if (vitrihinh >= 9)
                                        {
                                            vitri.X -= Nhanvat.dochenhlechX * 3;
                                            vitri.Y = Nhanvat.toadoYbandau;
                                        }
                                        return true;
                                    }
                                }
                            }
                            else
                            {
                                if (!nhanvatdoithu.Dangchuyendong && vitrihinh >= 9)
                                {
                                    if (vitrihinh >= 9)
                                    {
                                        vitri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - list_hinhtuyetchieu[vitrihinh].Height;
                                        vitri.X += Nhanvat.dochenhlechX * 3;
                                    }
                                    Microsoft.Xna.Framework.Color[] personTexture1Data;
                                    Microsoft.Xna.Framework.Color[] personTextture2Data;

                                    personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhtuyetchieu[vitrihinh].Width * list_hinhtuyetchieu[vitrihinh].Height];
                                    list_hinhtuyetchieu[vitrihinh].GetData(personTexture1Data);

                                    personTextture2Data =
                                        new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width * nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height];
                                    nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].GetData(personTextture2Data);

                                    System.Drawing.Rectangle personRectangle =
            new System.Drawing.Rectangle((int)vitri.X, (int)vitri.Y,
            list_hinhtuyetchieu[vitrihinh].Width, list_hinhtuyetchieu[vitrihinh].Height);

                                    System.Drawing.Rectangle blockRectangle =
                new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width, nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height);

                                    if (nhanvat.IntersectPixels(personRectangle, personTexture1Data,
                                blockRectangle, personTextture2Data))
                                    {
                                        fight_type = 0;
                                        solan_vacham++;
                                        if (vitrihinh >= 9)
                                        {
                                            vitri.X -= Nhanvat.dochenhlechX * 3;
                                            vitri.Y = Nhanvat.toadoYbandau;
                                        }
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
                return false;
        }

        public override void SolveFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu)
        {
            switch (fight_type)
            {
                case 0:
                    {
                        if (!nhanvatdoithu.die && solan_vacham==1)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                tiengvacham.Play();
                            }
                            nhanvatdoithu.blood -= decreaseblood*1/2;
                           
                            nhanvatdoithu.viTri.X -= (nhanvatdoithu.dolui) / nhanvatdoithu.soluonghinhbiquynh1;
                            
                            nhanvatdoithu.biquynh1 = true;

                            nhanvatdoithu.viTri.X += (float)(Nhanvat.dochenhlechX*0.2);
                           
                            if (nhanvatdoithu.viTri.X > Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width)
                            {
                                nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width;
                            }

                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width >= Nhanvat.Game.Window.ClientBounds.Width)
                                {
                                    nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X - nhanvat.list_hinhdungyen[0].Width + Nhanvat.dochenhlechX;
                                }
                            }

                            if (vitrihinh == soluonghinhtuyetchieu - 1)
                            {
                                nhanvatdoithu.Dangbinga = true;
                                nhanvatdoithu.biquynh1 = false;
                            }
                        }
                        fight_type = -1;
                    }
                    break;
                case 1:
                    {
                        if (nhanvatdoithu.die == false && solan_vacham == 1)
                        {
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                tiengvacham.Play();
                            }
                            nhanvatdoithu.blood -= decreaseblood * 1 / 2;
                            

                            nhanvatdoithu.viTri.X -= (nhanvatdoithu.dolui) / nhanvatdoithu.soluonghinhbiquynh1;

                            nhanvatdoithu.biquynh1 = true;

                            nhanvatdoithu.viTri.X += (float)(Nhanvat.dochenhlechX * 0.2);
                            
                            if (nhanvatdoithu.viTri.X > Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width)
                            {
                                nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width;
                            }

                            if (nhanvatdoithu.blood <= 0)
                            {
                                nhanvatdoithu.die = true;
                                if (nhanvatdoithu.viTri.X + nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width >= Nhanvat.Game.Window.ClientBounds.Width)
                                {
                                    nhanvatdoithu.viTri.X = Nhanvat.Game.Window.ClientBounds.Width - nhanvatdoithu.list_hinhbinga[nhanvatdoithu.list_hinhbinga.Count - 1].Width;
                                    nhanvat.viTri.X = nhanvatdoithu.viTri.X - nhanvat.list_hinhdungyen[0].Width + Nhanvat.dochenhlechX;
                                }
                            }
                            else
                            {
                                if (vitrihinh == soluonghinhtuyetchieu - 1)
                                {
                                    nhanvatdoithu.Dangbinga = true;
                                    nhanvatdoithu.biquynh1 = false;        
                                }
                            }
                        
                        }
                        fight_type = -1;
                    }
                    break;
                case 2:
                    {
                        nhanvatdoithu.Dangdo = true;
                        fight_type = -1;
                    }
                    break;
            }
        }
    }



}

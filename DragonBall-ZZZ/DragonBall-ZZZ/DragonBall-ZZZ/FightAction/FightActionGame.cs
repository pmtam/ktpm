﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace DragonBall_ZZZ.FightAction
{
    public class FightActionGame
    {
        private char loai;
        public bool dangthuchien = false;
        public int solan_vacham = 0;
        static public SoundEffect tiengvacham;
        public SoundEffect soundaction;
        public SoundEffectInstance instanceaction;
        public int vitrihinh=0;
        public int decreaseblood = 10;
        public int increaseenergy = 5;
        

        public static SoundEffect Tiengvacham
        {
            get { return FightActionGame.tiengvacham; }
            set { FightActionGame.tiengvacham = value; }
        }
       
        protected char Loai
        {
            get { return loai; }
            set { loai = value; }
        }

        virtual public void capNhat_X_Tick(object sender, EventArgs e)
        {
            ;
        }

        virtual public void DiChuyen()
        {
            ;
        }

       

        virtual public void Load(ContentManager contend, int chisonhanvat)
        {
            ;
        }
        

        virtual public void Draw(SpriteBatch spriteBatch, ref Vector2 viTri, Nhanvat nhanvat)
        {
            ;
        }

        

        virtual public bool Test(Nhanvat nhanvat)
        {
            return true;
        }

        virtual public bool IsFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu, Vector2 vitri)
        {
            return false;
        }

        virtual public void SolveFight(Nhanvat nhanvat, Nhanvat nhanvatdoithu)
        {
            ;
        }


    }
}

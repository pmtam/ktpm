﻿

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ScreenManage;
using DragonBall_ZZZ;
#endregion

namespace DragonBall_ZZZ
{
  
 abstract class MenuScreen : GameScreen
    {

        

        List<MenuEntry> menuEntries = new List<MenuEntry>();
        int selectedEntry = 0;
        string menuTitle;

        InputAction menuUp;
        InputAction menuDown;
        InputAction menuSelect;
        InputAction menuCancel;
        protected IList<MenuEntry> MenuEntries
        {
            get { return menuEntries; }
        }
        public MenuScreen(string menuTitle)
        {
            this.menuTitle = menuTitle;

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            menuUp = new InputAction(
               
                new Keys[] { Keys.Up },
                true);
            menuDown = new InputAction(
               
                new Keys[] { Keys.Down },
                true);
            menuSelect = new InputAction(
             
                new Keys[] { Keys.Enter },
                true);
            menuCancel = new InputAction(
          
                new Keys[] { Keys.Escape },
                true);
        }


        public override void HandleInput(GameTime gameTime, InputState input)
        {
           
            PlayerIndex playerIndex;
            if (menuUp.Evaluate(input, ControllingPlayer, out playerIndex))
            {
                selectedEntry--;

                if (selectedEntry < 0)
                    selectedEntry = menuEntries.Count - 1;
            }

        
            if (menuDown.Evaluate(input, ControllingPlayer, out playerIndex))
            {
                selectedEntry++;

                if (selectedEntry >= menuEntries.Count)
                    selectedEntry = 0;
            }

            if (menuSelect.Evaluate(input, ControllingPlayer, out playerIndex))
            {
                OnSelectEntry(selectedEntry, playerIndex);
            }
            else if (menuCancel.Evaluate(input, ControllingPlayer, out playerIndex))
            {
                OnCancel(playerIndex);
               

            }
        }
        protected void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[entryIndex].OnSelectEntry(playerIndex);
        }
        protected virtual void OnCancel(PlayerIndex playerIndex)
        {
            ExitScreen();
        }
        protected void OnCancel(object sender, PlayerIndexEventArgs e)
        {
            OnCancel(e.PlayerIndex);
           
        }
        protected void UpdateMenuEntryLocations()
        {
           
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

         
            Vector2 position = new Vector2(0f, 400f);

          
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];


                //position.X = ScreenManager.GraphicsDevice.Viewport.Width / 2 - menuEntry.GetWidth(this) / 2;
                position.X = Global.viTriButton_X;

                if (ScreenState == ScreenState.TransitionOn)
                    position.X -= transitionOffset * 512;
                else
                    position.X += transitionOffset * 512;

             
                menuEntry.Position = position;

              
                position.Y += menuEntry.GetHeight(this);
            }
        }


    
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

          
            for (int i = 0; i < menuEntries.Count; i++)
            {
                bool isSelected = IsActive && (i == selectedEntry);

                menuEntries[i].Update(this, isSelected, gameTime);
            }
        }


      
        public override void Draw(GameTime gameTime)
        {
       
            UpdateMenuEntryLocations();

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

        
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                bool isSelected = IsActive && (i == selectedEntry);

                menuEntry.Draw(this, isSelected, gameTime);
            }

            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

         
            Vector2 titlePosition = new Vector2(graphics.Viewport.Width / 2, 100);
            Vector2 titleOrigin = font.MeasureString(menuTitle) / 2;
            Color titleColor = new Color(192, 192, 192) * TransitionAlpha;
            float titleScale = 1.25f;

            titlePosition.Y -= transitionOffset * 100;

            spriteBatch.DrawString(font, menuTitle, titlePosition, titleColor, 0,
                                   titleOrigin, titleScale, SpriteEffects.None, 0);

            spriteBatch.End();
        }


     
    }
}

﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ScreenManage;
using Microsoft.Xna.Framework.Media;

namespace DragonBall_ZZZ
{
    class ScreenDoiMat : GameScreen
    {
        
        Vatphamhotro ht01, ht11;
        Vatphamhotro ht02, ht12;
        Texture2D GokuSmall1;
        Texture2D GokuSmall2;
        int time = 0;
        public int nv1 = -1, nv2 = -1;
        public int[] lvp1 = { -1, -1 }; // 
        public int[] lvp2 = { -1, -1 }; //
        Texture2D GokuBig1;
        Texture2D GokuBig2;
        ContentManager content;
        Texture2D backgroundTexture;
        Texture2D VS;
        int idb = -1;
        #region Initialization
        public ScreenDoiMat()
        {
            TransitionOnTime = TimeSpan.FromSeconds(2);
            TransitionOffTime = TimeSpan.FromSeconds(1.5);
            Name = "Doimat";
            
           
        }
        public override void Activate()
        {

            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            Random r = new Random();
            if (r.Next(10) > 5)
            {
                backgroundTexture = content.Load<Texture2D>(@"Image/unnamed");
            }
            else
            {
                backgroundTexture = content.Load<Texture2D>(@"Image/backgrounddm");
            }

            GokuBig1 = content.Load<Texture2D>(@"Avatar/Goku/GokuBig1");
       
            GokuBig2 = content.Load<Texture2D>(@"Avatar/Goku/GokuBig2");

            VS = content.Load<Texture2D>(@"Image/VS");

            GokuSmall1 = content.Load<Texture2D>(@"Avatar/Goku1");


            GokuSmall2 = content.Load<Texture2D>(@"Avatar/Goku2");

        }
        public override void Unload()
        {
           
        }


        #endregion

        #region Update and Draw


        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);
            time++;
            if (time == 80)
            {
                ScreenFight sf = new ScreenFight();
                sf.set_indexitems(lvp1[0], lvp2[0]);
             

                sf.set_indextypecharacter(nv1 + 1, nv2 + 1);
                ScreenManager.AddScreen(sf, ControllingPlayer);
                
               // MediaPlayer.Volume = 0;
            }
        }



        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);
            Rectangle recMainTitle = new Rectangle(250, -110, 500, 300);
            Rectangle recI = new Rectangle(5, 160, 300, 280);
            Rectangle recI2 = new Rectangle(695, 160, 300, 280);
            Rectangle name = new Rectangle(100, 450, 100, 50);
            Rectangle name2 = new Rectangle(800, 450, 100, 50);
            Vector2 pos = new Vector2 (viewport.Width/2-70, viewport.Height/2);
            spriteBatch.Begin();


            spriteBatch.Draw(backgroundTexture, fullscreen,
                             new Color(TransitionAlpha, TransitionAlpha, TransitionAlpha));



            spriteBatch .Draw(VS,  new Rectangle (viewport.Width/2-100, viewport.Height/2-100, 200, 150), Color.White);
            
            if (nv1 == 0)
            {
                spriteBatch.Draw(GokuBig1, recI, Color.White);
                spriteBatch.Draw(GokuSmall1, name, Color.White);
            }
            else
            {
                
            }
            if (nv2 == 0)
            {
                spriteBatch.Draw(GokuBig2, recI2, Color.White);
                spriteBatch.Draw(GokuSmall2, name2, Color.White);
            }
            else
            {
            }
            Vector2 poss = new Vector2(20, 470);
            Vector2 poss2 = new Vector2(730, 470);
         
          
            Rectangle rec1 = new Rectangle(30, 490, 40, 40);
         

            Rectangle rec2 = new Rectangle(690, 490, 40, 40);
       
             
            Vector2 tpos1 = new Vector2(80, 490);
          
         
            Vector2 tpos2 = new Vector2(740, 490);
          

            switch (lvp1[0])
            {

                case 0:
                    ht01 = new Vatphamtangmau();
                    ht01.ScreenManager = ScreenManager;
                    ht01.Activate();
                    spriteBatch.Draw(((Vatphamtangmau)(ht01)).TangmauTextture1, rec1, Color.White);
                    spriteBatch.DrawString(ScreenManager.Font, "Tang 20% mau", tpos1, Color.Yellow);
                    break;
                case 1:
                    ht01 = new Vatphamtangnangluong();
                    ht01.ScreenManager = ScreenManager;
                    ht01.Activate();
                    spriteBatch.Draw(((Vatphamtangnangluong)(ht01)).TangnangluongTextture1, rec1, Color.White);
                    spriteBatch.DrawString(ScreenManager.Font, "Tang 20% nang luong", tpos1, Color.Yellow);

                    break;

              
            }
            
            switch (lvp2[0])
            {

                case 0:
                    ht02 = new Vatphamtangmau();
                    ht02.ScreenManager = ScreenManager;
                    ht02.Activate();
                    spriteBatch.Draw(((Vatphamtangmau)(ht02)).TangmauTextture1, rec2, Color.White);
                    spriteBatch.DrawString(ScreenManager.Font, "Tang 20% mau", tpos2, Color.White);
                    break;
                case 1:
                    ht02 = new Vatphamtangnangluong();
                    ht02.ScreenManager = ScreenManager;
                    ht02.Activate();
                    spriteBatch.Draw(((Vatphamtangnangluong)(ht02)).TangnangluongTextture1, rec2, Color.White);
                    spriteBatch.DrawString(ScreenManager.Font, "Tang 20% nang luong", tpos2, Color.White);
                    break;

               
            }
            
           

            




            spriteBatch.End();  
        }


        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ScreenManage;
using Microsoft.Xna.Framework.Media;
namespace DragonBall_ZZZ
{
 
    public class Map : GameScreen
    {
        public Song amMap;
        ContentManager content;
        InputAction pauseAction;
        public List<My2DSprite> _Sprites;

        protected List<My2DSprite> Sprites
        {
            get { return _Sprites; }
            set { _Sprites = value; }
        }

        Map2 map2;
        private int _Width;

        public int Width
        {
            get { return _Width; }
            set { _Width = value; }
        }
        private int _Height;

        public int Height
        {
            get { return _Height; }
            set { _Height = value; }
        }
        Vector2 _PO;

        public Vector2 PO
        {
            get { return _PO; }
            set { 
                _PO = value;
                if (_PO.X < 0)
                    _PO.X = 0.0f;
                if (_PO.Y < 0)
                    _PO.Y = 0.0f;
                if (_PO.X >= _Width - Global.GameWidth)
                    _PO.X = _Width - Global.GameWidth;
                if (_PO.Y >= _Height - Global.GameHeight)
                    _PO.Y = _Height - Global.GameHeight;
            
            }
        }

        private int _w;

        public int w
        {
            get { return _w; }
            set { _w = value; }
        }
        private int _h;

        public int h
        {
            get { return _h; }
            set { _h = value; }
        }
        public Map()
        {           

            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(1);
            this.Name = "";
        }

        public override void Activate()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            amMap = content.Load<Song>(@"AMTHANH/AmMap/AmMap");

            _Sprites = new List<My2DSprite>();
            My2DSprite tmp;
            Texture2D[] texture;

            string[] strMap1 = new string[4];
            strMap1[0] = @"Map\111_1_1";
            strMap1[1] = @"Map\111_2_1";
            strMap1[2] = @"Map\111_1_2";
            strMap1[3] = @"Map\111_2_2";

            int i, n = strMap1.Length;
            _Width = 0;
            _Height = 0;
            for (i = 0; i < n; i++)
            {
                texture = new Texture2D[1];
                texture[0] = content.Load<Texture2D>(strMap1[i]);
                tmp = new My2DSprite(texture, 0, 0);

                _Sprites.Add(tmp);
            }

            _w = (int)_Sprites[0].Width;
            _h = (int)_Sprites[0].Height;
            _Width = _w * 2;
            _Height = _h * 2;

            map2 = new Map2();
            map2.Activate(content);

            pauseAction = new InputAction(

                  new Keys[] { Keys.Escape },
                  true);

            if (OptionsMenuScreen.currentSound == 0)
            {
                MediaPlayer.Play(amMap);
            }
        }


        public override void Unload()
        {
            content.Unload();
            amMap.Dispose();
        }


        public override void HandleInput(GameTime gameTime, InputState input)
        {

            if (input == null)
                throw new ArgumentNullException("input");


            int _playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[_playerIndex];
            PlayerIndex player;
            if (pauseAction.Evaluate(input, ControllingPlayer, out player))
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
        }
        List<Vector2> ListOrigin = new List<Vector2>();
        private bool bPressed = false;

        private Vector2 pStart;
        private Vector2 pOrigin;
        private Vector2 pCur;
        private Vector2 pEnd;

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                           bool coveredByOtherScreen)
        {
            
            if (coveredByOtherScreen == false || coveredByOtherScreen == false )
            {
                MouseState ms = Mouse.GetState();
                if (ms.RightButton == ButtonState.Pressed)
                {
                    if (!bPressed)
                        ms = BeginDrapMap(ms);

                    else
                        ms = DrapMap(ms);
                }
                else
                {
                    if (bPressed)
                        ms = EndDrapMap(ms);
                }


                int idx = (int)(PO.X / w);
                int idy = (int)(PO.Y / h);
                if (idx >= 0 && idx < _Sprites.Count - 2)
                {

                    _Sprites[idx].Left = idx * _w - PO.X;
                    _Sprites[idx].Top = -PO.Y;
                }
                if (idx + 1 >= 0 && idx + 1 < _Sprites.Count - 2)
                {
                    _Sprites[idx + 1].Left = (idx + 1) * _w - PO.X;
                    _Sprites[idx + 1].Top = -PO.Y;
                }
                if (idy >= 0 && idy < _Sprites.Count - 2)
                {
                    _Sprites[idy + 2].Left = idy * _w - PO.X;
                    _Sprites[idy + 2].Top = 1 * _h - PO.Y;
                }
                if (idy + 1 >= 0 && idy + 1 < _Sprites.Count - 2)
                {
                    _Sprites[idy + 2 + 1].Left = (idy + 1) * _w - PO.X;
                    _Sprites[idy + 2 + 1].Top = 1 * _h - PO.Y;
                }
                if (idy == 1 && idx == 0)
                {
                    _Sprites[idy + 1].Left = idx * _w - PO.X;
                    _Sprites[idy + 1].Top = 1 * _h - PO.Y;
                }

                map2.Update(PO);

                if(map2.viTriClick !=0)
            {
                ScreenDoiMat sf = new ScreenDoiMat();
                        sf.nv1 = 0;
                        sf.nv2 = 0;

                        sf.lvp1[0] = 0;
                        sf.lvp2[0] = 0;

                        switch (map2.viTriClick)
                        {
                            case 1:
                                ScreenManager.AddScreen(sf, ControllingPlayer);
                                break;
                            case 2:
                                ScreenManager.AddScreen(sf, ControllingPlayer);
                                break;
                            case 3:
                                ScreenManager.AddScreen(sf, ControllingPlayer);
                                break;
                        }
            }
            }
            
            base.Update(gameTime, otherScreenHasFocus, false);
            
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);
            spriteBatch.Begin();
            if (map2.viTriClick == 0)
            {
                int idx = (int)(PO.X / w);
                int idy = (int)(PO.Y / h);
                if (idx >= 0 && idx < _Sprites.Count - 2)
                    _Sprites[idx].Draw(gameTime, spriteBatch, Color.White);
                if (idx + 1 >= 0 && idx + 1 < _Sprites.Count - 2)
                    _Sprites[idx + 1].Draw(gameTime, spriteBatch, Color.White);
                if (idy >= 0 && idy < _Sprites.Count - 2)
                    _Sprites[idy + 2].Draw(gameTime, spriteBatch, Color.White);
                if (idy + 1 >= 0 && idy + 1 < _Sprites.Count - 2)
                    _Sprites[idy + 2 + 1].Draw(gameTime, spriteBatch, Color.White);
                if (idy == 1 && idx == 0)
                    _Sprites[idy + 1].Draw(gameTime, spriteBatch, Color.White);

                map2.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();
            
        }

        Vector2 MapStart;
        private MouseState BeginDrapMap(MouseState ms)
        {
            ListOrigin.Clear();
            bPressed = true;
            pStart = new Vector2(ms.X, ms.Y);
            pOrigin = this.PO;
            MapStart = pOrigin;
            return ms;
        }
        private MouseState EndDrapMap(MouseState ms)
        {
            bPressed = false;
            pEnd = new Vector2(ms.X, ms.Y);

            return ms;
        }

        private MouseState DrapMap(MouseState ms)
        {
            pCur = new Vector2(ms.X, ms.Y);
            this.PO = pOrigin - (pCur - pStart);
            return ms;
        }
    }
}

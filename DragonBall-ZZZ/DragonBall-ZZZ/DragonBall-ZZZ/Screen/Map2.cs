﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ScreenManage;

namespace DragonBall_ZZZ
{
    public class Map2
    {
        Texture2D node1, node2;
        ContentManager content;
        public List<My2DSprite> _Sprites;
        public int viTriClick;
        int bMap = 0;
        bool bClick;
        int _n;
        private int _Width;
        List<int[,]> colorMap;
        
        public int Width
        {
            get { return _Width; }
            set { _Width = value; }
        }
        private int _Height;

        public int Height
        {
            get { return _Height; }
            set { _Height = value; }
        }
        Vector2 _PO;

        public Vector2 PO
        {
            get { return _PO; }
            set
            {
                _PO = value;
            }
        }

        public Map2()
        {
            
        }


        public void Activate(ContentManager content)
        {
            colorMap = new List<int[,]>();
            _Sprites = new List<My2DSprite>();

            string[] strMap = new string[3];
            strMap[0] = @"Map\m1";
            strMap[1] = @"Map\m2";
            strMap[2] = @"Map\m3";
            _n = strMap.Length;
            _Width = 0;
            _Height = 0;

            for (int i = 0; i < _n; i++)
            {
                Texture2D[] texture = new Texture2D[1];
                texture[0] = content.Load<Texture2D>(strMap[i]);
                My2DSprite tmp = new My2DSprite(texture, 0, 0);

                _Height += (int)tmp.Height;
                _Sprites.Add(tmp);

                Color[] c1 = new Color[texture[0].Width * texture[0].Height];
                texture[0].GetData(c1);

                int[,] arr = new int[texture[0].Width, texture[0].Height];

                for (int x = 0; x < texture[0].Width; x++)
                    for (int y = 0; y < texture[0].Height; y++)
                    {
                        if (c1[x + y * texture[0].Width] == c1[0])
                            arr[x, y] = 0;
                        else
                            arr[x, y] = 1;
                    }
                colorMap.Add(arr);
            }
            _Width += (int)_Sprites[0].Width;

            node1 = content.Load<Texture2D>(@"Map/node1");
            node2 = content.Load<Texture2D>(@"Map/node2");
        }

        public void Update(Vector2 po)
        {
            MouseState ms = Mouse.GetState();

            PO = po;
            if (ms.X >= 0 && ms.X < Global.GameWidth && ms.Y >= 0 && ms.Y < Global.GameHeight)
            {
                bMap = KiemTraThuocMap(ms.X, ms.Y);
                if (bMap != 0 && ms.LeftButton == ButtonState.Pressed)
                {
                    bClick = true;
                }

            }
            else
            {
                bClick = false;
                bMap = 0;
                viTriClick = 0;
            }
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (bClick)
            {
                viTriClick = bMap;
                switch (bMap)
                {
                    case 1:
                        _Sprites[0].Left = -PO.X;
                        _Sprites[0].Top = 15 - PO.Y;
                        _Sprites[0].Draw(gameTime, spriteBatch, Color.Tomato);
                        break;
                    case 2:
                        _Sprites[1].Left = -PO.X;
                        _Sprites[1].Top = 15 + _Sprites[0].Height - PO.Y;
                        _Sprites[1].Draw(gameTime, spriteBatch, Color.Tomato);
                        break;
                    case 3:
                        _Sprites[2].Left = -PO.X;
                        _Sprites[2].Top = 15 + _Sprites[0].Height + _Sprites[1].Height - PO.Y;
                        _Sprites[2].Draw(gameTime, spriteBatch, Color.Tomato);
                        break;
                }
            }
            else
            {
                switch (bMap)
                {
                    case 1:
                        _Sprites[0].Left = -PO.X;
                        _Sprites[0].Top = 15 - PO.Y;
                        _Sprites[0].Draw(gameTime, spriteBatch, Color.Yellow);
                        spriteBatch.Draw(node1, new Rectangle(50, 20, node1.Width, node1.Height), Color.White);
                        break;
                    case 2:
                        _Sprites[1].Left = -PO.X;
                        _Sprites[1].Top = 15 + _Sprites[0].Height - PO.Y;
                        _Sprites[1].Draw(gameTime, spriteBatch, Color.Yellow);
                        spriteBatch.Draw(node2, new Rectangle(50, 20, node1.Width, node2.Height), Color.White);
                        break;
                    case 3:
                        _Sprites[2].Left = -PO.X;
                        _Sprites[2].Top = 15 + _Sprites[0].Height + _Sprites[1].Height - PO.Y;
                        _Sprites[2].Draw(gameTime, spriteBatch, Color.Yellow);
                        spriteBatch.Draw(node1, new Rectangle(50, 20, node1.Width, node1.Height), Color.White);
                        break;
                }
            }
        }

        private int KiemTraThuocMap(int X, int Y)
        {
            int h1 = (int)_Sprites[0].Height;
            int h2 = (int)_Sprites[1].Height;
            int h3 = (int)_Sprites[2].Height;

            if (X + PO.X >= 0 && X + PO.X < _Sprites[0].Width && Y + PO.Y >= 15 && Y + PO.Y < 15 + h1)
            {
                if ((colorMap[0])[X+ (int)PO.X, Y + (int)PO.Y -15] != 0)
                    return 1;
            }

            if (X + PO.X >= 0 && X + PO.X < _Sprites[1].Width && Y + PO.Y >= 15 + h1 && Y + PO.Y < 15 + h1 + h2)
            {
                if ((colorMap[1])[X + (int)PO.X, Y + (int)PO.Y - h1 - 15] != 0)
                    return 2;
            }

            if (X + PO.X >= 0 && X + PO.X < _Sprites[2].Width && Y + PO.Y >= 15 + h1 + h2 && Y + PO.Y < 15 + h1 + h2 + h3)
            {
                if ((colorMap[2])[X + (int)PO.X, Y + (int)PO.Y - h1 - h2 - 15] != 0)
                    return 3;
            }

            return 0;
        }
    }
}

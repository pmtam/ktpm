﻿using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ScreenManage;
using System.Collections.Generic;
namespace DragonBall_ZZZ
{
    class AvatarEntry : GameScreen
    {
        ContentManager content;
        Texture2D avatarTextture;
        Rectangle rectangle;
        Texture2D Arrow;
        Texture2D iGoku1, iYumi1;
        Texture2D iGoku2, iYumi2;
        Texture2D aP1, aP2, aP11, aP22;
        Texture2D nGoku, nYumi;
        Texture2D nGoku2, nYumi2;
        int delay = 0;
        public Texture2D AvatarTextture
        {
            get { return avatarTextture; }
            set { avatarTextture = value; }
        }
        Vector2 position;


        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }
        public event EventHandler<PlayerIndexEventArgs> SelectedAvatar;
        protected internal virtual void OnSelectAvatar(PlayerIndex playerIndex)
        {
            if (SelectedAvatar != null)
                SelectedAvatar(this, new PlayerIndexEventArgs(playerIndex));
        }
        public AvatarEntry(Texture2D textture)
        {
            this.avatarTextture = textture;
        }


        public override void  Activate()
        {
            if (content == null) 
            content = new ContentManager(ScreenManager.Game.Services, "Content");
            Arrow = content.Load<Texture2D>(@"Image/Arrow");
            iGoku1 = content.Load<Texture2D>(@"Avatar/Goku/iGoku");
            iYumi1 = content.Load<Texture2D>(@"Avatar/Yumi/iYumi");
          
            iGoku2 = content.Load<Texture2D>(@"Avatar/Goku/iGoku2");
            iYumi2 = content.Load<Texture2D>(@"Avatar/Yumi/iYumi2");
          

            aP1 = content.Load<Texture2D>(@"Arrow/arrowP1");
            aP2 = content.Load<Texture2D>(@"Arrow/arrowP2");
            aP11 = content.Load<Texture2D>(@"Arrow/arrowP11");
            aP22 = content.Load<Texture2D>(@"Arrow/arrowP22");

            nGoku = content.Load<Texture2D>(@"Avatar/nGoku");
            nYumi = content.Load<Texture2D>(@"Avatar/nYumi");
           

            nGoku2 = content.Load<Texture2D>(@"Avatar/nGoku2");
            nYumi2 = content.Load<Texture2D>(@"Avatar/nYumi2");
           





        }
        int j = 0;
        string[] ImageIntroCharacterGoku = { "hinh-" };
        Texture2D GokuTexttureIntro;
        

        public virtual void Update(GameTime gameTime)
        {
        }
        public virtual void Draw(Map aScreen, bool isSelect, bool isMove, GameTime gameTime, int index, int seq)
        {

            Activate();
            Rectangle rectanglearrow = new Rectangle((int)position.X, (int)position.Y, 110, 60);

            rectangle = new Rectangle((int)position.X, (int)position.Y, 110, 60);

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;         
            Rectangle rectangleArrow = new Rectangle((int)position.X + 25, (int)position.Y - 40, 30, 30);
            Rectangle recI = new Rectangle(5, 160, 300, 280);
            Rectangle recI2 = new Rectangle(695, 160, 300, 280);
            Rectangle name = new Rectangle(100, 450, 100, 50);
            Rectangle name2 = new Rectangle(800, 450, 100, 50);
            Vector2 posGoku = new Vector2(100, 450);
            Vector2 origin = new Vector2(0, 0);
            if (isMove)
            {
                if (index == 0)
                {
                    seq = 0;
                    spriteBatch.Draw(iGoku1, recI, Color.White);
                    spriteBatch.Draw(nGoku, name, Color.White);
                    
                }
                else
                {
                    if (index == 1)
                    {
                        seq = 0;
                        spriteBatch.Draw(iYumi1, recI, Color.White);
                        spriteBatch.Draw(nYumi, name, Color.White);

                    }
                    else
                    {
                            if (index == 2)
                            {
                                if (seq == 0)
                                {
                                    spriteBatch.Draw(iGoku1, recI, Color.White);
                                    spriteBatch.Draw(nGoku, name, Color.White);
                                }
                                else
                                {
                                    if (seq == 1)
                                    {
                                        spriteBatch.Draw(iYumi1, recI, Color.White);
                                        spriteBatch.Draw(nYumi, name, Color.White);
                                    }
                                    
                                }
                            }
                            else
                            {
                                if (index == 4)
                                {
                                    spriteBatch.Draw(iGoku2, recI2, Color.White);
                                    spriteBatch.Draw(nGoku2, name2, Color.White);
                                }
                                else
                                {
                                    if (index == 5)
                                    {
                                        spriteBatch.Draw(iYumi2, recI2, Color.White);
                                        spriteBatch.Draw(nYumi2, name2, Color.White);
                                    }
                                    else
                                    {
                                       
                                        if (index == 6)
                                        {
                                            if (seq == 0)
                                            {
                                                spriteBatch.Draw(iGoku2, recI2, Color.White);
                                                spriteBatch.Draw(nGoku2, name2, Color.White);
                                            }
                                            else
                                            {
                                                if (seq == 1)
                                                {
                                                    spriteBatch.Draw(iYumi2, recI2, Color.White);
                                                    spriteBatch.Draw(nYumi2, name2, Color.White);
                                                }
                                                

                                            }

                                        }

                                    }
                                }
                            }
                        
                    }
                }
            }
            

            if (!isMove)
            {
                spriteBatch.Draw(avatarTextture, rectangle, Color.Gray);
            }
            else
            {
                if (isMove && !isSelect)
                { 
                    spriteBatch.Draw(avatarTextture, rectangle, Color.White);
                    if (index >= 0 && index <= 2)
                    {
                        // long am thanh nv1
                     
                        spriteBatch.Draw(aP11, rectangle, Color.White);
                    }
                    else
                    {
                        // long am thanh nv2
                        
                        spriteBatch.Draw(aP22, rectangle, Color.White);
                    }
                }
                if (isMove && isSelect)
                {
                    spriteBatch.Draw(avatarTextture, rectangle, Color.White);
                    if (index >= 0 && index <= 2)
                    {
                        spriteBatch.Draw(aP1, rectangle, Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(aP2, rectangle, Color.White);
                    }
                }


            }
        }
    }
}
        



     

    

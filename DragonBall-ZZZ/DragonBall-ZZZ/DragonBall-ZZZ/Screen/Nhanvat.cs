﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.Threading;
using System.Windows.Forms;
using DragonBall_ZZZ.Screen;
using DragonBall_ZZZ.FightAction;
using ScreenManage;
namespace DragonBall_ZZZ
{

    public class Nhanvat : GameScreen
    {
       
        public int chisoloainv;
        public Item.Item loaivp;
        public SoundEffect sounddie;
        public SoundEffectInstance instancedie;
        public System.Windows.Forms.Timer capNhat_X;
        public bool biquynh1 = false;
        public bool biquynh2 = false;
        public int blood;
        public int energy;
        static public int max_energy = 100;
        static public int max_blood = 100;
        public int chisonhanvat = -1;
        public int buocChay;//do dai moi buoc nhan vat
        public int buocnhay;
        public HanhDong huongChuyenDong;
        public Vector2 viTri;//vi tri nhan vat tren man hinh
        public Vector2 ViTri
        {
            get { return viTri; }
            set { viTri = value; }
        }




        public int vitrihinhdungyen;
        public int vitrihinhdichuyen ;
        public int vitrihinhnhaytaicho ;
        public int vitridangnhaytoi ;
        public int vitrihinhbiquynh1 ;
        public int vitrihinhbiquynh2 ;
        public int vitrihinhbinga ;
        public int vitrihinhvucday;
        public int vitrihinhdungdo;
        public int vitrihinhngoido;
        
        

        
        public int soluonghinhbiquynh2;
        public int soluonghinhbiquynh1;
        public int soluonghinhdichuyen ;
        public int soluonghinhdungyen ;
        public int soluonghinhnhaytaicho ;
        public int soluonghinhnhaytoi ;
        public int soluonghinhbinga;
        public int soluonghinhvucday;
        public int soluonghinhdungdo;
        public int soluonghinhngoido;

        
        public int solan_vacham = 0;
        public float dolui ;
        public int solankiemtradungdokhinhaytoi;
        static public int dochenhlechY;
        static public int dochenhlechX;
        static public int toadoXbandau = 70;
        static public int toadoYbandau = 350;
        public bool dangthuan = true;
        public bool die = false;
        public bool die2 = false;


        public Nhanvat nhanvatdoithu = null;
        
        public KeyboardState keys;
        public KeyboardState previouskeyboardstate;
        public bool kiemtranhaytoi = false;
        public bool kiemtranhaylui = false;
        public bool kiemtracodangnhaytoi = false;


        public bool flag = false;

        // những thuộc tính có thể thay đổi
        public int collision = -1;
        public int fight_type = -1;
        public char loai;

        //
        
        public List<Texture2D> list_hinhdichuyen = new List<Texture2D>();
        public List<Texture2D> list_hinhnhaytaicho = new List<Texture2D>();
        public List<Texture2D> list_hinhnhaytoi = new List<Texture2D>();
        public List<Texture2D> list_hinhbiquynh1 = new List<Texture2D>();
        public List<Texture2D> list_hinhbiquynh2 = new List<Texture2D>();
        public List<Texture2D> list_hinhbinga = new List<Texture2D>(); 
        public List<Texture2D> list_hinhdungyen = new List<Texture2D>();
        public List<Texture2D> list_hinhvucday = new List<Texture2D>();
        public List<Texture2D> list_hinhdungdo = new List<Texture2D>();
        public List<Texture2D> list_hinhngoido = new List<Texture2D>();
        public List<FightActionGame> list_danhsachhanhdong = new List<FightActionGame>();


        public bool dangdo = false;

        public virtual void capNhat_X_Tick(object sender, EventArgs e)
        {
            ;
        }

        public void set_loaivp(Item.Item newloaivp)
        {
            loaivp = newloaivp;
        }

        public Item.Item get_loaivp()
        {
            return loaivp;
        }

        public void setindexitem(int newchisovatphamnv, ContentManager content)
        {
            if (newchisovatphamnv == 0)
            {
                loaivp = new Item.Itemincreasedblood();
                loaivp.Load(content);
            }
            else
            {
                loaivp = new Item.Itemincreasedpower();
                loaivp.Load(content);
            }
        }

        public bool Dangdo
        {
            get { return dangdo; }
            set { dangdo = value; }
        }

        public bool dangvucday = false;

        public bool Dangvucday
        {
            get { return dangvucday; }
            set { dangvucday = value; }
        }

        public bool dangbinga = false;

        public bool Dangbinga
        {
            get { return dangbinga; }
            set { dangbinga = value; }
        }

        public bool dangchuyendong = false;
        public bool Dangchuyendong
        {
            get { return dangchuyendong; }
            set { dangchuyendong = value; }
        }

        public bool dangnhay = false;

        public bool Dangnhay
        {
            get { return dangnhay; }
            set { dangnhay = value; }
        }
        
        public bool dangnhaytoi = false;
        public bool dangnhaylui = false;

        public bool Dangnhaytoi
        {
            get { return dangnhaytoi; }
            set { dangnhaytoi = value; }
        }
        
        public bool dangngoi = false;
        public bool Dangngoi
        {
            get { return dangngoi; }
            set { dangngoi = value; }
        }
        
        static public Game1 Game;
        public SpriteBatch spritebatch;

        public SpriteBatch Spritebatch
        {
            get { return spritebatch; }
            set { spritebatch = value; }
        }


        public virtual void DiChuyen()
        {
            ;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            ;
        }
     

        public virtual bool Iscollision(Nhanvat nhanvatdoithu)
        {
            return false;
        }

      

        public bool IsAction(List<FightActionGame> list_danhsachchuyendong)
        {
            for (int i = 0; i < list_danhsachchuyendong.Count; i++)
            {
                if (list_danhsachchuyendong[i].dangthuchien)
                    return true;
            }
            return false;
        }


        public bool IntersectPixels(System.Drawing.Rectangle rectangleA, Microsoft.Xna.Framework.Color[] dataA,
                                   System.Drawing.Rectangle rectangleB, Microsoft.Xna.Framework.Color[] dataB)
        {
            try
            {
                // Find the bounds of the rectangle intersection
                int top = Math.Max(rectangleA.Top, rectangleB.Top);
                int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
                int left = Math.Max(rectangleA.Left, rectangleB.Left);
                int right = Math.Min(rectangleA.Right, rectangleB.Right);

                // Check every point within the intersection bounds
                for (int y = top; y < bottom; y++)
                {
                    for (int x = left; x < right; x++)
                    {
                        // Get the color of both pixels at this point
                        Microsoft.Xna.Framework.Color colorA = dataA[(x - rectangleA.Left) +
                                             (y - rectangleA.Top) * rectangleA.Width];
                        Microsoft.Xna.Framework.Color colorB = dataB[(x - rectangleB.Left) +
                                             (y - rectangleB.Top) * rectangleB.Width];

                        // If both pixels are not completely transparent,
                        if (colorA.A != 0 && colorB.A != 0)
                        {
                            // then an intersection has been found
                            return true;
                        }
                    }
                }
            }
            catch
            {

            }
            // No intersection found
            return false;
        }

        public void raNgoaiBanDo()
        {
            int xMax = Game.Window.ClientBounds.Width;
            int yMax = Game.Window.ClientBounds.Height;
            //if (viTri.X < 0) this.viTri.X += buocChay;
            if (viTri.X < 0) this.viTri.X = 0;
            if (viTri.Y < 0) this.viTri.Y = 0;
            if (viTri.X > Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width) this.viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width;
            if (viTri.Y > yMax - 202) this.viTri.Y = yMax - 202;
        }


        public bool SolveCollisionItem(Item.Item item)
        {
            Microsoft.Xna.Framework.Color[] personTexture1Data;
            Microsoft.Xna.Framework.Color[] personTexture2Data;


            personTexture1Data =
                new Microsoft.Xna.Framework.Color[list_hinhdungyen[0].Width * list_hinhdungyen[0].Height];
            list_hinhdungyen[0].GetData(personTexture1Data);

            personTexture2Data = new Microsoft.Xna.Framework.Color[item.sprite_item.Width * item.sprite_item.Height];
            item.sprite_item.GetData(personTexture2Data);


            System.Drawing.Rectangle personRectangle =
new System.Drawing.Rectangle((int)viTri.X, (int)viTri.Y,
list_hinhdungyen[0].Width, list_hinhdungyen[0].Height);

            System.Drawing.Rectangle blockRectangle =
new System.Drawing.Rectangle((int)item.vitri.X, (int)item.vitri.Y,
item.sprite_item.Width, item.sprite_item.Height);

            if (IntersectPixels(personRectangle, personTexture1Data,
        blockRectangle, personTexture2Data))
            {
                item.Affect(this);
                return true;
            }
            return false;
        }

        public virtual void Update1(KeyboardState currentkey)
        {
            ;
        }

        public virtual void Update2()
        {
            ;
        }

        public virtual void SolveCollision(Nhanvat nhanvat)
        {
            ;
        }

        public virtual void FightAction(Nhanvat nhanVat2)
        {
            ;
        }
    }
}

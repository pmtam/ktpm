﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace DragonBall_ZZZ
{
    class PlayerIndexEvent :EventArgs
    {
        PlayerIndex playerindex;
        public PlayerIndexEvent(PlayerIndex playerindex)
        {
            this.playerindex = playerindex;
        }
        public PlayerIndex Playerindex
        {
            get { return playerindex; }
            set { playerindex = value; }
        }

    }
}

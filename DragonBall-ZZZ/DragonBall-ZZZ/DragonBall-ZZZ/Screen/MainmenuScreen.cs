﻿
using Microsoft.Xna.Framework;
using ScreenManage;
using Microsoft.Xna.Framework.Storage;
using System;
using System.IO;
using System.Xml.Serialization;
namespace DragonBall_ZZZ
{

    class MainMenuScreen : MenuScreen
    {

        public bool stop = true;
        
        public static bool flag_save = false;
        string filename = "mysave.xml";

        public MainMenuScreen()
            : base("")
        {

            MenuEntry playGameMenuEntry = new MenuEntry("Play Game");
            MenuEntry optionsMenuEntry = new MenuEntry("Options");
            MenuEntry exitMenuEntry = new MenuEntry("Exit");
            MenuEntry LoadMenuEntry = new MenuEntry("Load");
            MenuEntry Introduce = new MenuEntry("Introduces");
           


            playGameMenuEntry.Selected += PlayGameMenuEntrySelected;
            optionsMenuEntry.Selected += OptionsMenuEntrySelected;
            Introduce.Selected += IntroducesEntrySelected;
            exitMenuEntry.Selected += OnCancel;
            LoadMenuEntry.Selected += LoadSelected;
            


            MenuEntries.Add(playGameMenuEntry);
            MenuEntries.Add(optionsMenuEntry);       
            MenuEntries.Add(Introduce);
            MenuEntries.Add(LoadMenuEntry);
            MenuEntries.Add(exitMenuEntry);

        }



        void PlayGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                               new  Map());
        }
        void OptionsMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new OptionsMenuScreen(), e.PlayerIndex);
        }
        void IntroducesEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new IntroduceSreen(), e.PlayerIndex);
        }

        void LoadSelected(object sender, PlayerIndexEventArgs e)
        {
            // gan ham load vo
            if (File.Exists(filename))
            {
                flag_save = true;
                ScreenFight sf = new ScreenFight();
                ScreenManager.AddScreen(sf, e.PlayerIndex);
            }
            else
            {
                flag_save = false;
            }

            
            
                
           
        }

        protected override void OnCancel(PlayerIndex playerIndex)
        {
            const string message = "Are you sure you want to exit this game?";

            MessageBoxScreen confirmExitMessageBox = new MessageBoxScreen(message);

            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmExitMessageBox, playerIndex);
           

        }



        void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Game.Exit();
        }
    }

      
}

﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework.Input;
using System.Xml.Linq;
using ScreenManage;
using Microsoft.Xna.Framework.Media;
using DragonBall_ZZZ.Screen;
using Microsoft.Xna.Framework.Audio;
using System.Timers;
using System.Xml.Serialization;

namespace DragonBall_ZZZ
{
    [Serializable]
    public struct SaveGame
    {
        // tham khao http://www.gavindraper.co.uk/2010/11/25/how-to-loadsave-game-state-in-xna/
       public int time;
       public int blood_nv1;
       public int blood_nv2;
       public int energy_nv1;
       public int energy_nv2;
       public Vector2 position_nv1;
       public Vector2 position_nv2;
       public int chisonv1;
       public int chisonv2;
       public int chisovpnv1;
       public int chisovpnv2;
       public int solansudungvpnv1;
       public int solansudungvpnv2;
       public bool flag_item;
    }
    class ScreenFight : GameScreen
    {
        StorageDevice device;
        string filename = "mysave.xml";
        IAsyncResult asyncResult;
        int TWin = -2, miniTime1 = 0;
        InputAction Load;
        InputAction Save;

       public void SaveToDevice(Nhanvat newnhanvat1, Nhanvat newnhanvat2, int newtime, bool newflag)
        {
            device = null;
            SaveGame SaveData;
            asyncResult = StorageDevice.BeginShowSelector(PlayerIndex.One, null, null);   
            device = StorageDevice.EndShowSelector(asyncResult);
            if (device != null && device.IsConnected)
            {
                SaveData = new SaveGame()
                {
                   time=newtime, 
                   blood_nv1=newnhanvat1.blood,
                   blood_nv2=newnhanvat2.blood,
                   energy_nv1=newnhanvat1.energy,
                   energy_nv2=newnhanvat2.energy,
                   position_nv1=newnhanvat1.viTri,
                   position_nv2=newnhanvat2.viTri,
                   flag_item=newflag,
                   chisonv1=chisoloainv1,
                chisonv2=chisoloainv2,
                chisovpnv1=chisovatphamnv1,
                chisovpnv2=chisovatphamnv2,
                solansudungvpnv1=nhanVat1.loaivp.solandangsudung,
                solansudungvpnv2=nhanVat2.loaivp.solandangsudung
                };
               
                
              

                Stream stream = File.Create(filename);
                XmlSerializer serializer = new XmlSerializer(typeof(SaveGame));
                serializer.Serialize(stream, SaveData);
                stream.Close();
            }
               
       }

       public void LoadGame()
       {
           device = null;
           asyncResult = StorageDevice.BeginShowSelector(PlayerIndex.One, null, null);
           device = StorageDevice.EndShowSelector(asyncResult);
           if (File.Exists(filename))
           {
               Stream stream = File.Open(filename, FileMode.Open);
               XmlSerializer serializer = new XmlSerializer(typeof(SaveGame));
               SaveGame SaveData = (SaveGame)serializer.Deserialize(stream);
               stream.Close();
           }
       }

        Item.Item item = new Item.Itemincreasedblood();
        Vector2 Item_Position = new Vector2();
        bool Flag_item = false;
        Texture2D Fight;
        Texture2D Saving;
        Texture2D Goku1, Goku2;
        System.Windows.Forms.Timer timer_randomitem = new System.Windows.Forms.Timer();
        static public Microsoft.Xna.Framework.Input.Keys keyupnguoichoi1 = Keys.W;
        static public Microsoft.Xna.Framework.Input.Keys keydownnguoichoi1 = Keys.S;
        static public Microsoft.Xna.Framework.Input.Keys keyrightnguoichoi1 = Keys.D;
        static public Microsoft.Xna.Framework.Input.Keys keyleftnguoichoi1 = Keys.A;
        static public Microsoft.Xna.Framework.Input.Keys keyattackbyhandnguoichoi1 = Keys.G;
        static public Microsoft.Xna.Framework.Input.Keys keyattackbyfootnguoichoi1 = Keys.H;
        static public Microsoft.Xna.Framework.Input.Keys keyspecialactionnguoichoi1 = Keys.Space;

        static public Microsoft.Xna.Framework.Input.Keys keyupnguoichoi2 = Keys.Up;
        static public Microsoft.Xna.Framework.Input.Keys keydownnguoichoi2 = Keys.Down;
        static public Microsoft.Xna.Framework.Input.Keys keyrightnguoichoi2 = Keys.Right;
        static public Microsoft.Xna.Framework.Input.Keys keyleftnguoichoi2 = Keys.Left;
        static public Microsoft.Xna.Framework.Input.Keys keyattackbyhandnguoichoi2 = Keys.K;
        static public Microsoft.Xna.Framework.Input.Keys keyattackbyfootnguoichoi2 = Keys.L;
        static public Microsoft.Xna.Framework.Input.Keys keyspecialactionnguoichoi2 = Keys.O;

        static public Microsoft.Xna.Framework.Input.Keys keys_save = Keys.F5;

        bool Is_Pause = false;

        SaveGame SaveData;
        bool flag_pause = true;
        Texture2D mHealthBar, DongHo;
        Texture2D FrameP1;
        Texture2D FrameP2;
        private Nhanvat nhanVat1;
        private Nhanvat nhanVat2;
        private char trangthainhanvat1 = ' ';
        private Texture2D nen;

        Texture2D backgroundTexture;
        ContentManager content;
        int flag = 0;
        public int chisonv1 = -1;
        int chisovatphamnv1 = -1;
        public int chisoloainv1;
        public int chisonv2 = -1;
        int chisovatphamnv2 = -1;
        public int chisoloainv2;
        int timeLoad = 0;
        public Song soundeffect;
        InputAction pauseAction;
        private bool flag_saving = false;
        int timestartsaving = -1;

        #region Initialization

        public ScreenFight()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(1);

            pauseAction = new InputAction(

                 new Keys[] { Keys.Escape },
                 true);
             this.Name = "ScreenFight";
             Save = new InputAction(new Keys[] { Keys.F5 }, true);
             Load = new InputAction(new Keys[] { Keys.F7 }, true);


        }

        public void set_indextypecharacter(int newchisoloainv1, int newchisoloainv2)
        {
            chisoloainv1 = newchisoloainv1;
            chisoloainv2 = newchisoloainv2;
        }
        public override void Activate()
        {
            foreach (GameScreen s in ScreenManager.GetScreens())
            {
                if (s.Name == "Doimat")
                {
                    ScreenManager.RScreen(s);
                }
            }



            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            
            timer_randomitem.Interval = 10000;
            timer_randomitem.Enabled = true;
            timer_randomitem.Tick += new EventHandler(timer_randomitem_Tick);
            if (MainMenuScreen.flag_save)
            {
                device = null;
                asyncResult = StorageDevice.BeginShowSelector(PlayerIndex.One, null, null);
                device = StorageDevice.EndShowSelector(asyncResult);

                Stream stream = File.Open(filename, FileMode.Open);
                XmlSerializer serializer = new XmlSerializer(typeof(SaveGame));
                SaveData = (SaveGame)serializer.Deserialize(stream);
                stream.Close();
                chisoloainv1 = SaveData.chisonv1;
                chisoloainv2 = SaveData.chisonv2;
                chisovatphamnv1 = SaveData.chisovpnv1;
                chisovatphamnv2 = SaveData.chisovpnv2;
            }

            if (flag == 0)
            {
                if (chisoloainv1 == 1)
                {
                    nhanVat1 = new NhanVatGoku(ScreenManager.game11, nhanVat2, content);
                    ((NhanVatGoku)(nhanVat1)).loainhanvat = new NhanVatGoku1BenTrai();
                    ((NhanVatGoku)(nhanVat1)).loainhanvat.LoadContent(content, (NhanVatGoku)nhanVat1);
                    ((NhanVatGoku)(nhanVat1)).capNhat_X.Start();
                    nhanVat1.setindexitem(chisovatphamnv1, content);
                }
                else
                {
                    
                }
                if (chisoloainv2 == 1)
                {
                    nhanVat2 = new NhanVatGoku(ScreenManager.game11, nhanVat1, content);
                    ((NhanVatGoku)(nhanVat2)).loainhanvat = new NhanVatGoku2BenPhai();
                    ((NhanVatGoku)(nhanVat2)).loainhanvat.LoadContent(content, (NhanVatGoku)nhanVat2);
                    ((NhanVatGoku)(nhanVat2)).capNhat_X.Start();
                    nhanVat2.setindexitem(chisovatphamnv2, content);
                }
                else
                {
                    
                }
             
                nhanVat1.nhanvatdoithu = nhanVat2;
                nhanVat2.nhanvatdoithu = nhanVat1;
                flag = 1;
                trangthainhanvat1 = 't';
            }

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
           
           
            backgroundTexture = content.Load<Texture2D>(@"Image/background/IMG00000");
            soundeffect = content.Load<Song>("soundoffightinggame");
            mHealthBar = content.Load<Texture2D>(@"Image/health") as Texture2D;
            DongHo = content.Load<Texture2D>(@"Image/DongHo") as Texture2D;
            FrameP1 = content.Load<Texture2D>(@"Image/khung1") as Texture2D;
            FrameP2 = content.Load<Texture2D>(@"Image/khung2") as Texture2D;

            Goku1 = content.Load<Texture2D>(@"Avatar/Goku/GokuSmall1");

            Goku2 = content.Load<Texture2D>(@"Avatar/Goku/GokuSmall2");


            Fight = content.Load<Texture2D>(@"Image/fight") as Texture2D;
            Saving = content.Load<Texture2D>("Text/saving");
            item.Load(content);

            nhanVat1.chisonhanvat = 1;
            nhanVat2.chisonhanvat = 2;
            
            nhanVat1.Spritebatch = spriteBatch;
            
            nhanVat2.Spritebatch = spriteBatch;
            if (MainMenuScreen.flag_save)
            {
                nhanVat1.blood = SaveData.blood_nv1;
                nhanVat2.blood = SaveData.blood_nv2;
                if (nhanVat1.blood == 0)
                {
                    nhanVat1.die2 = true;
                }
                if (nhanVat2.blood == 0)
                {
                    nhanVat2.die2 = true;
                }
                nhanVat1.energy = SaveData.energy_nv1;
                nhanVat2.energy = SaveData.energy_nv2;
                Time = SaveData.time;
                nhanVat1.viTri = SaveData.position_nv1;
                nhanVat2.viTri = SaveData.position_nv2;
                nhanVat1.loaivp.solandangsudung = SaveData.solansudungvpnv1;
                nhanVat2.loaivp.solandangsudung = SaveData.solansudungvpnv2;
               
                if (SaveData.flag_item)
                {
                    Flag_item = true;
                    timer_randomitem.Start();
                    Random rnd = new Random();
                    int min;
                    int max;
                    if (nhanVat1.viTri.X < nhanVat2.viTri.X)
                    {
                        min = (int)nhanVat1.viTri.X + nhanVat1.list_hinhdungyen[0].Width;
                        max = (int)nhanVat2.viTri.X - item.sprite_item.Width;
                    }
                    else
                    {
                        min = (int)nhanVat2.viTri.X + nhanVat2.list_hinhdungyen[0].Width;
                        max = (int)nhanVat1.viTri.X - item.sprite_item.Width;
                    }
                    if (max <= min)
                    {
                        max = min;
                    }
                    item.vitri.X = rnd.Next(min, max);
                    item.vitri.Y = Nhanvat.toadoYbandau + nhanVat1.list_hinhdungyen[0].Height - item.sprite_item.Height;
                }

                MainMenuScreen.flag_save = false;
            }
            if (OptionsMenuScreen.currentSound == 0)
            {
                MediaPlayer.Play(soundeffect);
            }
        }

        void timer_randomitem_Tick(object sender, EventArgs e)
        {
            Flag_item = false;
            timer_randomitem.Stop();
        }
        public override void HandleInput(GameTime gameTime, InputState input)
        {

            if (input == null)
                throw new ArgumentNullException("input");


            int _playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[_playerIndex];
            PlayerIndex player;
            if (pauseAction.Evaluate(input, ControllingPlayer, out player))
            {
                dabaophuroi = true;
                nhanVat1.dabaophuroi = true;
                nhanVat2.dabaophuroi = true;
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
            else
            {
                if (Save.Evaluate(input, ControllingPlayer, out player))
                {
                    // de ham save
                    SaveToDevice(nhanVat1, nhanVat2, time1, Flag_item);
                    timestartsaving = Time;
                    flag_saving = true;
                }
            }
        }
       
        public override void Unload()
        {
            content.Unload();
            soundeffect.Dispose();
            MediaPlayer.Stop();
        }


        #endregion

       public int Time = 60;
        int miniTime = 0;
        int miniTimeLoad = 0;
        int time1 = 0;
        int time2 = 0;
        int miniTime2 = 0;

        public void set_indexitems(int chiso1, int chiso2)
        {
            chisovatphamnv1 = chiso1;
            chisovatphamnv2 = chiso2;
        }
        public int TestWin(int mnv1, int mnv2)
        {
            if (mnv1 <= 0)
            {
                return 1;
            }
            if (mnv2 <= 0)
            {
                return 2;
            }
            if ((mnv1 >= mnv2) && (Time <= 0))
            {
                return 2;
            }
                if((mnv1 < mnv2) && (Time <=0))
            {
                return 1;
            }
            return -1;
        }
        int time3 = 5;
        int ok = 0;
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {

            if (coveredByOtherScreen == false || coveredByOtherScreen == false )
            {
                nhanVat1.dabaophuroi = false;
                nhanVat2.dabaophuroi = false;
                dabaophuroi = false;
            }

            KeyboardState currentkey = Keyboard.GetState();
            if (flag_saving)
            {
                if (Time < timestartsaving - 1)
                {
                    flag_saving = false;
                }
            }
            TWin = TestWin(nhanVat1.blood, nhanVat2.blood);
            if (TWin == 1 || TWin == 2)
            {
                nhanVat1.dabaophuroi = true;
                nhanVat2.dabaophuroi = true;
                miniTime1 += 1;
                if (miniTime1 == 80)
                {
                    miniTime1 = 0;
                    time3 -= 1;
                    if (time3 == 0)
                    {

                        LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new Map());

                    }
                }

            }


            if (dabaophuroi == false && ok == 1 )
            {
                miniTime += 1;
            }
                if (miniTime == 80)
                {
                    
                    if (Time > 0 )
                    {
                        Time--;
                        time1 = Time;
                        miniTime = 0;
                    }

                    else
                    {
                        
                        miniTime = 0;
                        time1 -= 1;
                        if (time1 == -3)
                        {
                            LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new Map());
                        }
                    }
                }
                if (time2 == 1)
                {
                    ok = 1;
                }
                miniTime2 += 1;
                if (miniTime2 == 80)
                {
                    time2 += 1;
                    miniTime2 = 0;
                }

                miniTimeLoad += 1;
                if (miniTimeLoad == 80)
                {
                    timeLoad += 1;
                    miniTimeLoad = 0;
                }
                if (trangthainhanvat1 == 't')
                {
                    if (this.nhanVat1.viTri.X >= this.nhanVat2.viTri.X + nhanVat2.list_hinhdungyen[0].Width - Nhanvat.dochenhlechX && this.nhanVat1.viTri.Y == Nhanvat.toadoYbandau && this.nhanVat2.viTri.Y == Nhanvat.toadoYbandau)
                    {
                        SpriteBatch spriteBatch1 = nhanVat1.spritebatch;
                        SpriteBatch spriteBatch2 = nhanVat2.spritebatch;
                        float vitrinhanvat1 = -1;
                        float vitrinhanvat2 = -1;

                        int blood1 = nhanVat1.blood;
                        int blood2 = nhanVat2.blood;

                        int energy1 = nhanVat1.energy;
                        int energy2 = nhanVat2.energy;
                        vitrinhanvat1 = nhanVat1.viTri.X;
                        vitrinhanvat2 = nhanVat2.viTri.X;
                        if (chisoloainv1 == 1)
                        {
                            //Item.Item item_tam = nhanVat1.get_loaivp();
                            ((NhanVatGoku)(nhanVat1)).loainhanvat = new NhanVatGoku1BenPhai();
                            ((NhanVatGoku)(nhanVat1)).loainhanvat.LoadContent(content, (NhanVatGoku)nhanVat1);
                            ((NhanVatGoku)(nhanVat1)).capNhat_X.Start();
                            //nhanVat1.set_loaivp(item_tam);
                            //nhanVat1.setindexitem(chisovatphamnv1);
                        }
                        else
                        {
                            
                        }
                        nhanVat1.spritebatch = spriteBatch1;

                        if (chisoloainv2 == 1)
                        {
                            //Item.Item item_tam = nhanVat2.get_loaivp();
                           
                            ((NhanVatGoku)(nhanVat2)).loainhanvat = new NhanVatGoku2BenTrai();
                            ((NhanVatGoku)(nhanVat2)).loainhanvat.LoadContent(content, (NhanVatGoku)nhanVat2);
                            ((NhanVatGoku)(nhanVat2)).capNhat_X.Start();
                           // nhanVat2.set_loaivp(item_tam);
                           // nhanVat2.setindexitem(chisovatphamnv2);
                        }
                        else
                        {
                           
                            
                        }
                        nhanVat1.nhanvatdoithu = nhanVat2;
                        nhanVat2.nhanvatdoithu = nhanVat1;
                        nhanVat2.spritebatch = spriteBatch2;
                        nhanVat1.chisonhanvat = 1;
                        nhanVat2.chisonhanvat = 2;
                        trangthainhanvat1 = 'p';
                       
                        nhanVat1.viTri.X = vitrinhanvat1;
                        nhanVat2.viTri.X = vitrinhanvat2;
                        nhanVat1.energy = energy1;
                        nhanVat1.blood = blood1;
                        nhanVat2.energy = energy2;
                        nhanVat2.blood = blood2;

                    }
                }
                else
                {
                    if (trangthainhanvat1 == 'p')
                    {
                        if (nhanVat1.viTri.X + nhanVat1.list_hinhdungyen[0].Width <= nhanVat2.viTri.X + Nhanvat.dochenhlechX && this.nhanVat1.viTri.Y == Nhanvat.toadoYbandau && this.nhanVat2.viTri.Y == Nhanvat.toadoYbandau)
                        {
                            SpriteBatch spriteBatch1 = nhanVat1.spritebatch;
                            SpriteBatch spriteBatch2 = nhanVat2.spritebatch;
                            nhanVat1.chisonhanvat = 1;
                            nhanVat2.chisonhanvat = 2;
                            float vitrinhanvat1 = -1;
                            float vitrinhanvat2 = -1;
                            int blood1 = nhanVat1.blood;
                            int blood2 = nhanVat2.blood;
                            int energy1 = nhanVat1.energy;
                            int energy2 = nhanVat2.energy;
                            vitrinhanvat1 = nhanVat1.viTri.X;
                            vitrinhanvat2 = nhanVat2.viTri.X;

                            if (chisoloainv1 == 1)
                            {
                               
                                ((NhanVatGoku)(nhanVat1)).loainhanvat = new NhanVatGoku1BenTrai();
                                ((NhanVatGoku)(nhanVat1)).loainhanvat.LoadContent(content, (NhanVatGoku)nhanVat1);
                                ((NhanVatGoku)(nhanVat1)).capNhat_X.Start();
                               // nhanVat1.setindexitem(chisovatphamnv1);
                            }
                            else
                            {
                               
                                
                            }
                            if (chisoloainv2 == 1)
                            {
                              
                                ((NhanVatGoku)(nhanVat2)).loainhanvat = new NhanVatGoku2BenPhai();
                                ((NhanVatGoku)(nhanVat2)).loainhanvat.LoadContent(content, (NhanVatGoku)nhanVat2);
                                ((NhanVatGoku)(nhanVat2)).capNhat_X.Start();
                              //  nhanVat2.setindexitem(chisovatphamnv2);
                            }
                            else
                            {
                                
                            }
                            nhanVat1.nhanvatdoithu = nhanVat2;
                            nhanVat2.nhanvatdoithu = nhanVat1;
                            nhanVat2.spritebatch = spriteBatch2;
                            nhanVat1.chisonhanvat = 1;
                            nhanVat2.chisonhanvat = 2;
                            trangthainhanvat1 = 't';
                            
                            nhanVat1.viTri.X = vitrinhanvat1;
                            nhanVat2.viTri.X = vitrinhanvat2;
                            nhanVat1.energy = energy1;
                            nhanVat1.blood = blood1;
                            nhanVat2.energy = energy2;
                            nhanVat2.blood = blood2;
                        }
                    }
                }

                // TODO: Add your update logic here
                nhanVat1.Update1(currentkey);//update chuyen dong nhan vat
                 this.nhanVat1.Update2();

                this.nhanVat2.Update1(currentkey);
                this.nhanVat2.Update2();

                nhanVat1.FightAction(nhanVat2);
                nhanVat2.FightAction(nhanVat1);
                if (Flag_item == false && Time == 30)
                {
                    Flag_item = true;
                    timer_randomitem.Start();
                    Random rnd = new Random();
                    int min;
                    int max;
                    if (nhanVat1.viTri.X < nhanVat2.viTri.X)
                    {
                        min = (int)nhanVat1.viTri.X + nhanVat1.list_hinhdungyen[0].Width;
                        max = (int)nhanVat2.viTri.X - item.sprite_item.Width;
                    }
                    else
                    {
                        min = (int)nhanVat2.viTri.X + nhanVat2.list_hinhdungyen[0].Width;
                        max = (int)nhanVat1.viTri.X - item.sprite_item.Width;
                    }
                    if (max <= min)
                    {
                        max = min;
                    }
                    item.vitri.X = rnd.Next(min, max);
                    item.vitri.Y = Nhanvat.toadoYbandau + nhanVat1.list_hinhdungyen[0].Height - item.sprite_item.Height;
                }
                if (Flag_item)
                {
                    if (nhanVat1.SolveCollisionItem(item))
                    {
                        Flag_item = false;
                        timer_randomitem.Stop();
                    }
                    else
                    {
                        if (nhanVat2.SolveCollisionItem(item))
                        {
                            Flag_item = false;
                            timer_randomitem.Stop();
                        }
                    }
                }
            

            base.Update(gameTime, otherScreenHasFocus, false);
        }

        int flag1 = 0;
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(Color.CornflowerBlue);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);
            Vector2 positionTime = new Vector2(viewport.Width / 2 - 25, 55);
            Vector2 origin = new Vector2(0, ScreenManager.Font.LineSpacing / 2);
            spriteBatch.Begin();
            spriteBatch.Draw(backgroundTexture, fullscreen,
                           Color.White);
            Vector2 posWin = new Vector2(viewport.Width / 2 - 135, viewport.Height / 2);
            if (flag_saving)
            {
                spriteBatch.Draw(Saving, new Rectangle(viewport.Width / 2 - 100, viewport.Height / 2, 300, 100), Color.White);
            }
            if (time2 == 1)
            {
                spriteBatch.Draw(Fight, new Rectangle(viewport.Width / 2 - 100, viewport.Height / 2, 200, 150), Color.White);
            }

            if (chisoloainv1 == 1)
            {
                spriteBatch.Draw(Goku1, new Rectangle(5, 32, 70, 40), Color.White);
            }
            else
            {
               
            }

            if (chisoloainv2 == 1)
            {
                spriteBatch.Draw(Goku2, new Rectangle(925, 32, 70, 40), Color.White);
            }
            else
            {
                
            }



            string strTime = Time.ToString();

            if (Time < 10)
            {
                strTime = "0" + Time.ToString();
            }
            spriteBatch.Draw(DongHo, new Vector2(450, 10), Color.White);
            spriteBatch.DrawString(ScreenManager.Font, strTime.ToString(), positionTime, Color.Yellow, 0, origin, 2, SpriteEffects.None, 0);



            spriteBatch.Draw(mHealthBar, new Rectangle(80, 30, mHealthBar.Width, 20), new Rectangle(0, 45, mHealthBar.Width, 20), Color.Red);
            //Draw the current health level based on the current Health            
            spriteBatch.Draw(mHealthBar, new Rectangle(80, 30, (int)(mHealthBar.Width * (1 - ((double)nhanVat1.blood / 100))), 20), new Rectangle(0, 45, mHealthBar.Width, 20), Color.Gray);
            //Draw the box around the health bar
            spriteBatch.Draw(mHealthBar, new Rectangle(80, 30, mHealthBar.Width, 21), new Rectangle(0, 0, mHealthBar.Width, 21), Color.White);




            spriteBatch.Draw(mHealthBar, new Rectangle(80, 53, mHealthBar.Width, 20), new Rectangle(0, 45, mHealthBar.Width, 20), Color.Blue);
            //Draw the current health level based on the current Health            
            spriteBatch.Draw(mHealthBar, new Rectangle(80, 53, (int)(mHealthBar.Width * (1 - ((double)nhanVat1.energy / 100))), 20), new Rectangle(0, 45, mHealthBar.Width, 20), Color.Gray);
            //Draw the box around the health bar
            spriteBatch.Draw(mHealthBar, new Rectangle(80, 53, mHealthBar.Width, 21), new Rectangle(0, 0, mHealthBar.Width, 21), Color.White);




            spriteBatch.Draw(mHealthBar, new Rectangle(540, 30, mHealthBar.Width, 20), new Rectangle(0, 45, mHealthBar.Width, 20), Color.Gray);

            //Draw the current health level based on the current Health
            spriteBatch.Draw(mHealthBar, new Rectangle(540, 30, (int)(mHealthBar.Width * ((double)nhanVat2.blood / 100)), 20), new Rectangle(0, 45, mHealthBar.Width, 20), Color.Red);

            //Draw the box around the health bar
            spriteBatch.Draw(mHealthBar, new Rectangle(540, 30, mHealthBar.Width, 21), new Rectangle(0, 0, mHealthBar.Width, 21), Color.White);





            spriteBatch.Draw(mHealthBar, new Rectangle(540, 53, mHealthBar.Width, 20), new Rectangle(0, 45, mHealthBar.Width, 20), Color.Gray);

            //Draw the current health level based on the current Health
            if (nhanVat2.energy > 100)
            {
                nhanVat2.energy = 100;
            }
            spriteBatch.Draw(mHealthBar, new Rectangle(540, 53, (int)(mHealthBar.Width * ((double)nhanVat2.energy / 100)), 20), new Rectangle(0, 45, mHealthBar.Width, 20), Color.Blue);

            //Draw the box around the health bar
            spriteBatch.Draw(mHealthBar, new Rectangle(540, 53, mHealthBar.Width, 21), new Rectangle(0, 0, mHealthBar.Width, 21), Color.White);


            if (TWin == 1)
            {
                spriteBatch.DrawString(ScreenManager.Font, "Play 2 Win", posWin, Color.White, 0, origin, 2, SpriteEffects.None, 0);
            }
            else
            {
                if (TWin == 2)
                {
                    spriteBatch.DrawString(ScreenManager.Font, "Play 1 Win", posWin, Color.White, 0, origin, 2, SpriteEffects.None, 0);


                }
            }
          


            spriteBatch.End();

            if (Flag_item)
            {
                item.Draw(null, spriteBatch, 1);
            }
            this.nhanVat1.Draw(spriteBatch);
            this.nhanVat2.Draw(spriteBatch);
            base.Draw(gameTime);
        }

    }
}
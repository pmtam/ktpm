﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScreenManage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace DragonBall_ZZZ
{
    class Vatphamtangmau :Vatphamhotro
    {

        Texture2D TangmauTextture;

        public Texture2D TangmauTextture1
        {
            get { return TangmauTextture; }
            set { TangmauTextture = value; }
        }
        ContentManager content;
        public Vatphamtangmau()
        {
            _muctangmau = 5;
        }
        int _muctangmau;

        public int Muctangmau
        {
            get { return _muctangmau; }
            set { _muctangmau = value; }
        }
        public override void Activate()
        {
            content = new ContentManager(ScreenManager.Game.Services, "Content");
            TangmauTextture = content.Load<Texture2D>(@"item/Tangmau");

            
        }
        
        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime, bool ismove, bool isselect, int style)
        {
            Vector2 pos = new Vector2(50, 550);
            Vector2 pos2 = new Vector2(750, 550);
            Rectangle rec = new Rectangle( (int)(position.X), (int)(position.Y) , 40, 40);
           
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            if (!ismove)
            {
               
                    spriteBatch.Draw(TangmauTextture, rec, Color.Gray);
                
            }
            else
            {
                if (ismove && !isselect)
                {
                    spriteBatch.Draw(TangmauTextture, rec, Color.White);
                    if (style == 1)
                    {
                        spriteBatch.DrawString(ScreenManager.Font, "Tang 20% mau", pos, Color.Yellow);
                    }
                    else
                    {
                        spriteBatch.DrawString(ScreenManager.Font, "Tang 20% mau", pos2, Color.White);
                    }
                }
                else
                {
                    if (ismove && isselect)
                    {
                        spriteBatch.Draw(TangmauTextture, rec, Color.Yellow);

                        
                    }
                }
            }
        }
        public override void Update(Microsoft.Xna.Framework.GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            
        }

    }
}

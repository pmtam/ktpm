﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using DragonBall_ZZZ.FightAction.FightActionGoku;

namespace DragonBall_ZZZ.Screen
{
    public class NhanVatGoku2BenTrai : LoainhanvatGoku
    {
        public override void capNhat_X_Tick(object sender, EventArgs e, NhanVatGoku nhanvat)
        {
            if (nhanvat.die2)
            {
                nhanvat.vitrihinhbinga = 6;
            }
            else
            {
                if (nhanvat.die || nhanvat.dangbinga)
                {
                    nhanvat.dangdo = false;
                    nhanvat.vitrihinhdungdo = 0;
                    if (nhanvat.vitrihinhbinga < nhanvat.soluonghinhbinga - 1)
                    {
                        if (nhanvat.die && OptionsMenuScreen.currentSound == 0)
                        {
                            NhanVatGoku.instancedie.Play();
                        }
                        nhanvat.vitrihinhbinga++;
                        nhanvat.viTri.X -= (float)(Nhanvat.dochenhlechX * 0.5);
                        if (nhanvat.viTri.X < 0)
                        {
                            nhanvat.viTri.X = 0;
                            nhanvat.dangvucday = true;
                        }
                    }
                    else
                    {
                        nhanvat.vitrihinhbinga = 0;
                        if (nhanvat.die)
                        {
                            nhanvat.die2 = true;
                            if (OptionsMenuScreen.currentSound == 0)
                            {
                                NhanVatGoku.instancedie.Stop();
                            }
                        }
                        else
                        {
                            nhanvat.dangbinga = false;
                            nhanvat.dangvucday = true;
                        }
                    }
                }
                else
                {
                    if (nhanvat.dangvucday)
                    {
                        if (nhanvat.vitrihinhvucday < nhanvat.soluonghinhvucday - 1)
                        {
                            nhanvat.vitrihinhvucday++;
                            if (nhanvat.vitrihinhvucday == 3)
                            {
                                nhanvat.viTri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - Nhanvat.dochenhlechX - nhanvat.list_hinhvucday[nhanvat.vitrihinhvucday].Height;
                            }
                            else
                            {
                                nhanvat.viTri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[0].Height - nhanvat.list_hinhvucday[nhanvat.vitrihinhvucday].Height;
                            }
                            if (nhanvat.vitrihinhvucday >= 2 && nhanvat.vitrihinhvucday <= 5)
                            {
                                nhanvat.viTri.X += (float)((nhanvat.soluonghinhbinga - 1) * (float)(Nhanvat.dochenhlechX * 0.2) / 4.0);
                            }
                        }
                        else
                        {
                            nhanvat.vitrihinhvucday = 0;
                            nhanvat.viTri.Y = Nhanvat.toadoYbandau;
                            nhanvat.dangvucday = false;
                        }
                    }
                    else
                    {
                        if (nhanvat.dangdo)
                        {
                            //instancedungdo.Play();
                            if (nhanvat.vitrihinhdungdo < nhanvat.soluonghinhdungdo - 1)
                            {
                                nhanvat.viTri.Y = Nhanvat.toadoYbandau;
                                for (int i = 0; i < nhanvat.list_danhsachhanhdong.Count; i++)
                                {
                                    nhanvat.list_danhsachhanhdong[i].dangthuchien = false;
                                    nhanvat.list_danhsachhanhdong[i].vitrihinh = 0;
                                }
                                nhanvat.dangbinga = false;
                                nhanvat.vitrihinhbinga = 0;
                                nhanvat.dangchuyendong = false;
                                nhanvat.vitrihinhdungyen = 0;
                                nhanvat.dangngoi = false;
                                nhanvat.dangnhay = false;
                                nhanvat.vitrihinhnhaytaicho = 0;
                                nhanvat.dangnhaylui = false;
                                nhanvat.dangnhaytoi = false;
                                nhanvat.vitridangnhaytoi = 0;
                                nhanvat.dangvucday = false;
                                nhanvat.vitrihinhvucday = 0;
                                nhanvat.vitrihinhdungdo++;

                                nhanvat.viTri.Y = Nhanvat.toadoYbandau + nhanvat.list_hinhdungyen[1].Height - nhanvat.list_hinhdungdo[nhanvat.vitrihinhdungdo].Height;
                                nhanvat.viTri.X -= (nhanvat.dolui) / nhanvat.soluonghinhdungdo;
                                if (nhanvat.viTri.X < 0)
                                {
                                    nhanvat.viTri.X = 0;
                                }
                            }
                            else
                            {
                                NhanVatGoku.instancedungdo.Stop();
                                nhanvat.viTri.Y = Nhanvat.toadoYbandau;
                                nhanvat.vitrihinhdungdo = 0;
                                nhanvat.dangdo = false;
                            }
                        }
                        else
                        {
                            if (nhanvat.biquynh1)
                            {
                                if (nhanvat.vitrihinhbiquynh1 < nhanvat.soluonghinhbiquynh1 - 1)
                                {
                                    for (int i = 0; i < nhanvat.list_danhsachhanhdong.Count; i++)
                                    {
                                        nhanvat.list_danhsachhanhdong[i].dangthuchien = false;
                                        nhanvat.list_danhsachhanhdong[i].vitrihinh = 0;
                                    }
                                    nhanvat.dangbinga = false;
                                    nhanvat.vitrihinhbinga = 0;
                                    nhanvat.dangchuyendong = false;
                                    nhanvat.vitrihinhdungyen = 0;
                                    nhanvat.dangngoi = false;
                                    nhanvat.dangnhay = false;
                                    nhanvat.vitrihinhnhaytaicho = 0;
                                    nhanvat.dangnhaylui = false;
                                    nhanvat.dangnhaytoi = false;
                                    nhanvat.vitridangnhaytoi = 0;
                                    nhanvat.dangvucday = false;
                                    nhanvat.dangdo = false;
                                    nhanvat.vitrihinhdungdo = 0;
                                    nhanvat.vitrihinhvucday = 0;
                                    nhanvat.vitrihinhbiquynh1++;

                                    nhanvat.viTri.X -= (nhanvat.dolui) / nhanvat.soluonghinhbiquynh1;
                                    if (nhanvat.viTri.X < 0)
                                    {
                                        nhanvat.viTri.X = 0;
                                    }
                                }
                                else
                                {

                                    nhanvat.vitrihinhbiquynh1 = 0;
                                    nhanvat.biquynh1 = false;
                                }
                            }
                            else
                            {
                                if (nhanvat.biquynh2)
                                {
                                    if (nhanvat.vitrihinhbiquynh2 < nhanvat.soluonghinhbiquynh2 - 1)
                                    {

                                        for (int i = 0; i < nhanvat.list_danhsachhanhdong.Count; i++)
                                        {
                                            nhanvat.list_danhsachhanhdong[i].dangthuchien = false;
                                            nhanvat.list_danhsachhanhdong[i].vitrihinh = 0;
                                        }
                                        nhanvat.dangbinga = false;
                                        nhanvat.vitrihinhbinga = 0;
                                        nhanvat.dangchuyendong = false;
                                        nhanvat.vitrihinhdungyen = 0;
                                        nhanvat.dangngoi = false;
                                        nhanvat.dangnhay = false;
                                        nhanvat.vitrihinhnhaytaicho = 0;
                                        nhanvat.dangnhaylui = false;
                                        nhanvat.dangnhaytoi = false;
                                        nhanvat.vitridangnhaytoi = 0;
                                        nhanvat.dangvucday = false;
                                        nhanvat.dangdo = false;
                                        nhanvat.vitrihinhdungdo = 0;
                                        nhanvat.vitrihinhvucday = 0;
                                        nhanvat.vitrihinhbiquynh2++;

                                        nhanvat.viTri.X -= (nhanvat.dolui) / nhanvat.soluonghinhbiquynh2;
                                        if (nhanvat.viTri.X < 0)
                                        {
                                            nhanvat.viTri.X = 0;
                                        }
                                    }
                                    else
                                    {

                                        nhanvat.vitrihinhbiquynh2 = 0;
                                        nhanvat.biquynh2 = false;
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < nhanvat.list_danhsachhanhdong.Count; i++)
                                    {
                                        if (nhanvat.list_danhsachhanhdong[i].dangthuchien)
                                        {
                                            nhanvat.list_danhsachhanhdong[i].capNhat_X_Tick(sender, e);
                                            return;
                                        }
                                    }


                                    if (nhanvat.loaivp.dangsudung)
                                    {
                                        nhanvat.loaivp.capNhat_X_Tick(nhanvat);
                                        return;
                                    }



                                    if (nhanvat.dangnhaylui)
                                    {
                                        if (nhanvat.vitridangnhaytoi > 0)
                                        {
                                            if (nhanvat.collision == 2 || nhanvat.collision == 7)
                                            {
                                                nhanvat.collision = -1;
                                            }
                                            else
                                            {
                                                if (nhanvat.collision != 6)
                                                {
                                                    nhanvat.viTri.X -= nhanvat.buocChay * 8;
                                                    if (nhanvat.viTri.X < 0)
                                                    {
                                                        nhanvat.viTri.X = 0;
                                                    }
                                                }
                                            }
                                            if (nhanvat.vitridangnhaytoi == 0)
                                            {
                                                nhanvat.viTri.Y = Nhanvat.toadoYbandau;
                                            }
                                            else
                                            {
                                                if (nhanvat.vitridangnhaytoi > 0 && nhanvat.vitridangnhaytoi < 5)
                                                    nhanvat.viTri.Y += nhanvat.buocnhay + 10;
                                                else
                                                    nhanvat.viTri.Y -= (float)(nhanvat.buocnhay * 3.0) / 2;
                                            }

                                            nhanvat.vitridangnhaytoi--;
                                        }
                                        else
                                        {
                                            nhanvat.vitridangnhaytoi = 0;
                                            nhanvat.viTri.Y = Nhanvat.toadoYbandau;
                                            nhanvat.solankiemtradungdokhinhaytoi = 0;
                                            nhanvat.kiemtranhaytoi = false;
                                            nhanvat.solan_vacham = 0;
                                            ((KickbyfootwhenjumpGokuleft)(nhanvat.list_danhsachhanhdong[4])).solan_vacham = 0;
                                            ((BeatbyhandwhenjumpGokuleft)(nhanvat.list_danhsachhanhdong[1])).solan_vacham = 0;
                                            nhanvat.dangnhaylui = false;

                                            //collision = -1;
                                        }
                                    }
                                    else
                                    {

                                        if (nhanvat.Dangnhaytoi)
                                        {
                                            nhanvat.kiemtracodangnhaytoi = true;
                                            if (nhanvat.vitridangnhaytoi < nhanvat.soluonghinhnhaytoi - 2)
                                            {
                                                if (nhanvat.collision == 2 || nhanvat.collision == 7)
                                                {
                                                    nhanvat.collision = -1;
                                                }
                                                else
                                                {
                                                    if (nhanvat.collision != 6)
                                                    {
                                                        nhanvat.viTri.X += nhanvat.buocChay * 8;
                                                    }
                                                }
                                                if (nhanvat.vitridangnhaytoi == 0)
                                                {
                                                    nhanvat.viTri.Y = Nhanvat.toadoYbandau;
                                                }
                                                else
                                                {
                                                    if (nhanvat.vitridangnhaytoi > 0 && nhanvat.vitridangnhaytoi < 5)
                                                        nhanvat.viTri.Y -= nhanvat.buocnhay;
                                                    else
                                                        nhanvat.viTri.Y += (float)(nhanvat.buocnhay * 3.0) / 2;
                                                }
                                                nhanvat.raNgoaiBanDo();
                                                nhanvat.vitridangnhaytoi++;
                                            }
                                            else
                                            {
                                                nhanvat.vitridangnhaytoi = 0;
                                                nhanvat.viTri.Y = Nhanvat.toadoYbandau;
                                                nhanvat.solankiemtradungdokhinhaytoi = 0;
                                                nhanvat.kiemtranhaytoi = false;
                                                nhanvat.solan_vacham = 0;
                                                ((KickbyfootwhenjumpGokuleft)(nhanvat.list_danhsachhanhdong[4])).solan_vacham = 0;
                                                ((BeatbyhandwhenjumpGokuleft)(nhanvat.list_danhsachhanhdong[1])).solan_vacham = 0;
                                                nhanvat.Dangnhaytoi = false;

                                                nhanvat.collision = -1;
                                            }
                                        }
                                        else
                                        {
                                            if (nhanvat.Dangnhay)
                                            {
                                                nhanvat.biquynh1 = false;
                                                if (nhanvat.vitrihinhnhaytaicho < nhanvat.soluonghinhnhaytaicho - 1)
                                                {
                                                    if (nhanvat.vitrihinhnhaytaicho == 0 || nhanvat.vitrihinhnhaytaicho == 5)
                                                        nhanvat.viTri.Y = Nhanvat.toadoYbandau;
                                                    else
                                                    {
                                                        if (nhanvat.vitrihinhnhaytaicho > 0 && nhanvat.vitrihinhnhaytaicho < 4)
                                                            nhanvat.viTri.Y -= nhanvat.buocnhay;
                                                        else
                                                            nhanvat.viTri.Y += (float)(nhanvat.buocnhay * 3.0) / 1;
                                                    }
                                                    nhanvat.vitrihinhnhaytaicho++;
                                                }
                                                else
                                                {
                                                    nhanvat.vitrihinhnhaytaicho = 0;
                                                    ((KickbyfootwhenjumpGokuleft)(nhanvat.list_danhsachhanhdong[4])).solan_vacham = 0;
                                                    ((BeatbyhandwhenjumpGokuleft)(nhanvat.list_danhsachhanhdong[1])).solan_vacham = 0;
                                                    nhanvat.Dangnhay = false;
                                                    nhanvat.solan_vacham = 0;
                                                    nhanvat.kiemtranhaytoi = false;
                                                }
                                            }
                                            else
                                            {
                                                if (nhanvat.Dangchuyendong)
                                                {
                                                    if (nhanvat.huongChuyenDong == HanhDong.DiChuyenTraiQuaPhai)
                                                    {
                                                        if (nhanvat.vitrihinhdichuyen < nhanvat.soluonghinhdichuyen - 1)
                                                        {
                                                            nhanvat.vitrihinhdichuyen++;
                                                        }
                                                        else
                                                        {
                                                            nhanvat.vitrihinhdichuyen = 0;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        if (nhanvat.vitrihinhdichuyen > 0)
                                                            nhanvat.vitrihinhdichuyen--;
                                                        else
                                                        {
                                                            nhanvat.vitrihinhdichuyen = nhanvat.soluonghinhdichuyen - 1;

                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (nhanvat.vitrihinhdungyen < nhanvat.soluonghinhdungyen - 1)
                                                    {
                                                        nhanvat.vitrihinhdungyen++;
                                                        nhanvat.kiemtracodangnhaytoi = false;
                                                    }
                                                    else
                                                        nhanvat.vitrihinhdungyen = 0;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public override void LoadContent(ContentManager contend, NhanVatGoku nhanvat)
        {
            nhanvat.chisonhanvat = 2;
            nhanvat.list_hinhbinga.Clear();
            nhanvat.list_hinhbiquynh1.Clear();
            nhanvat.list_hinhbiquynh2.Clear();
            nhanvat.list_hinhdichuyen.Clear();
            nhanvat.list_hinhdungyen.Clear();
            nhanvat.list_hinhnhaytaicho.Clear();
            nhanvat.list_hinhnhaytoi.Clear();
            nhanvat.list_danhsachhanhdong.Clear();
            nhanvat.list_danhsachhanhdong.Clear();
            nhanvat.list_hinhvucday.Clear();
            nhanvat.list_hinhdungdo.Clear();


            nhanvat.list_danhsachhanhdong.Add(new BeatbyhandGokuleft());
            nhanvat.list_danhsachhanhdong.Add(new BeatbyhandwhenjumpGokuleft());
            nhanvat.list_danhsachhanhdong.Add(new BeatbyhandwhensitGokuleft());
            nhanvat.list_danhsachhanhdong.Add(new KickbyfootGokuleft());
            nhanvat.list_danhsachhanhdong.Add(new KickbyfootwhenjumpGokuleft());
            nhanvat.list_danhsachhanhdong.Add(new KickbyfootwhensitGokuleft());
            nhanvat.list_danhsachhanhdong.Add(new SpecialactionGokuleft());



            nhanvat.hinhngoinhanvat = contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhngoi");

            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-1"));
            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-2"));
            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-3"));
            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-4"));
            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-5"));
            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-6"));
            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-7"));
            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-8"));
            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-9"));
            nhanvat.list_hinhdungyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungyen/dungyen-10"));

            nhanvat.list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-1"));
            nhanvat.list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-2"));
            nhanvat.list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-3"));
            nhanvat.list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-4"));
            nhanvat.list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-5"));
            nhanvat.list_hinhdichuyen.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdichuyenqualai/dichuyen-6"));

            nhanvat.list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-1"));
            nhanvat.list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-3"));
            nhanvat.list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-4"));
            nhanvat.list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-5"));
            nhanvat.list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-6"));
            nhanvat.list_hinhnhaytaicho.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytaicho/nhaytaicho-7"));

            nhanvat.list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-1"));
            nhanvat.list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-2"));
            nhanvat.list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-3"));
            nhanvat.list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-4"));
            nhanvat.list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-5"));
            nhanvat.list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-6"));
            nhanvat.list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-7"));
            nhanvat.list_hinhnhaytoi.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhnhaytoi/hinhnhaytoi-8"));

            nhanvat.list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh1"));
            nhanvat.list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh2"));
            nhanvat.list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh3"));
            nhanvat.list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh4"));
            nhanvat.list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh5"));
            nhanvat.list_hinhbiquynh1.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh1/biquynh6"));

            nhanvat.list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh2/hinhbiquynh2-1"));
            nhanvat.list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh2/hinhbiquynh2-2"));
            nhanvat.list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh2/hinhbiquynh2-3"));
            nhanvat.list_hinhbiquynh2.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbiquynh2/hinhbiquynh2-4"));

            nhanvat.list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-1"));
            nhanvat.list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-2"));
            nhanvat.list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-3"));
            nhanvat.list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-4"));
            nhanvat.list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-5"));
            nhanvat.list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-6"));
            nhanvat.list_hinhbinga.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhbinga/hinhbinga-7"));

            nhanvat.list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-1"));
            nhanvat.list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-2"));
            nhanvat.list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-3"));
            nhanvat.list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-4"));
            nhanvat.list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-5"));
            nhanvat.list_hinhvucday.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhvucday/hinhvucday-6"));

            nhanvat.list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-1"));
            nhanvat.list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-2"));
            nhanvat.list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-3"));
            nhanvat.list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-4"));
            nhanvat.list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-5"));
            nhanvat.list_hinhdungdo.Add(contend.Load<Texture2D>("HINHNHANVAT2BENTRAI/hinhdungdo/hinhdungdo-6"));

            for (int i = 0; i < nhanvat.list_danhsachhanhdong.Count; i++)
            {
                nhanvat.list_danhsachhanhdong[i].Load(contend, nhanvat.chisonhanvat);
            }

           // nhanvat.loaivp.Load(contend);
        }

        public override void Update1(KeyboardState currentkey, NhanVatGoku nhanvat)
        {
            nhanvat.keys = currentkey;
            Keys[] currentPressedKeys = nhanvat.keys.GetPressedKeys();
            nhanvat.kiemtranhaytoi = false;
            nhanvat.kiemtranhaylui = false;

                if (nhanvat.keys.IsKeyDown(ScreenFight.keyrightnguoichoi2))
                {
                    if (currentPressedKeys.Contains(ScreenFight.keyattackbyhandnguoichoi2) && nhanvat.dabaophuroi == false)
                    {
                        if (!nhanvat.Dangnhay && !nhanvat.dangnhaytoi && !nhanvat.dangnhaylui)
                        {
                            if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                            {
                                nhanvat.list_danhsachhanhdong[0].dangthuchien = true;
                                nhanvat.DiChuyen();
                                return;
                            }
                            return;
                        }
                    }
                    else
                    {
                        if (currentPressedKeys.Contains(ScreenFight.keyattackbyfootnguoichoi2) && nhanvat.dabaophuroi == false)
                        {
                            if (!nhanvat.Dangnhay && !nhanvat.dangnhaytoi && !nhanvat.dangnhaylui)
                            {
                                if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                                {
                                    nhanvat.list_danhsachhanhdong[3].dangthuchien = true;
                                    nhanvat.DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                        else
                        {
                            //di chuyen tu phai qua trai
                            nhanvat.kiemtranhaytoi = true;
                            nhanvat.huongChuyenDong = HanhDong.DiChuyenTraiQuaPhai;
                            nhanvat.DiChuyen();
                            return;
                        }
                    }
                }
                if (nhanvat.keys.IsKeyDown(ScreenFight.keyleftnguoichoi2))
                {
                    if (currentPressedKeys.Contains(ScreenFight.keyattackbyhandnguoichoi2) && nhanvat.dabaophuroi == false)
                    {
                        if (!nhanvat.Dangnhay && !nhanvat.Dangnhaytoi && !nhanvat.dangnhaylui)
                        {
                            if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                            {
                                nhanvat.list_danhsachhanhdong[0].dangthuchien = true;
                                nhanvat.DiChuyen();
                                return;
                            }
                            return;
                        }
                    }
                    else
                    {
                        if (currentPressedKeys.Contains(ScreenFight.keyattackbyfootnguoichoi2) && nhanvat.dabaophuroi == false)
                        {
                            if (!nhanvat.Dangnhay && !nhanvat.Dangnhaytoi && !nhanvat.dangnhaylui)
                            {
                                if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                                {
                                    nhanvat.list_danhsachhanhdong[3].dangthuchien = true;
                                    nhanvat.DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                        else
                        {
                            //di chuyen tu phai qua trái
                            nhanvat.kiemtranhaylui = true;
                            nhanvat.huongChuyenDong = HanhDong.DiChuyenPhaiQuaTrai;
                            nhanvat.flag = true;
                            nhanvat.DiChuyen();
                            return;
                        }
                    }
                }

                if (nhanvat.keys.IsKeyDown(ScreenFight.keydownnguoichoi2))
                {
                    if (currentPressedKeys.Contains(ScreenFight.keyattackbyhandnguoichoi2) && !nhanvat.previouskeyboardstate.IsKeyDown(ScreenFight.keyattackbyhandnguoichoi2) && nhanvat.dabaophuroi == false)
                    {
                        if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                        {
                            nhanvat.list_danhsachhanhdong[2].dangthuchien = true;
                            nhanvat.DiChuyen();
                            return;
                        }
                        return;
                    }
                    else
                    {
                        if (currentPressedKeys.Contains(ScreenFight.keyattackbyfootnguoichoi2) && !nhanvat.previouskeyboardstate.IsKeyDown(ScreenFight.keyattackbyfootnguoichoi2) && nhanvat.dabaophuroi == false)
                        {
                            if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                            {
                                nhanvat.list_danhsachhanhdong[5].dangthuchien = true;
                                nhanvat.DiChuyen();
                                return;
                            }
                            return;
                        }
                        else
                        {
                            nhanvat.huongChuyenDong = HanhDong.DangNgoi;
                            nhanvat.DiChuyen();
                            return;
                        }
                    }
                }
                // chưa xử lý nút nhấn nhảy quýnh tay liên tục
                if (nhanvat.keys.IsKeyDown(ScreenFight.keyattackbyhandnguoichoi2) && nhanvat.dabaophuroi == false)
                {
                    if (nhanvat.Dangnhay)
                    {
                        if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                        {
                            nhanvat.list_danhsachhanhdong[1].dangthuchien = true;
                            nhanvat.DiChuyen();
                            return;
                        }
                        return;
                    }
                    else
                    {
                        if (nhanvat.Dangnhaytoi)
                        {
                            if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                            {
                                nhanvat.list_danhsachhanhdong[1].dangthuchien = true;
                                nhanvat.DiChuyen();
                                return;
                            }
                            return;
                        }
                        else
                        {
                            if (nhanvat.dangnhaylui)
                            {
                                if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                                {
                                    nhanvat.list_danhsachhanhdong[1].dangthuchien = true;
                                    nhanvat.DiChuyen();
                                    return;
                                }
                                return;
                            }
                            else
                            {
                                if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                                {
                                    nhanvat.list_danhsachhanhdong[0].dangthuchien = true;
                                    nhanvat.DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                    }
                }

                // chưa xử lý nút nhấn nhảy đá chân liên tục
                if (nhanvat.keys.IsKeyDown(ScreenFight.keyattackbyfootnguoichoi2) && nhanvat.dabaophuroi == false)
                {
                    if (nhanvat.Dangnhay)
                    {
                        if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                        {
                            nhanvat.list_danhsachhanhdong[4].dangthuchien = true;
                            nhanvat.DiChuyen();
                            return;
                        }
                        return;
                    }
                    else
                    {
                        if (nhanvat.Dangnhaytoi)
                        {
                            if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                            {
                                nhanvat.list_danhsachhanhdong[4].dangthuchien = true;
                                nhanvat.DiChuyen();
                                return;
                            }
                            return;
                        }
                        else
                        {
                            if (nhanvat.dangnhaylui)
                            {
                                if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                                {
                                    nhanvat.list_danhsachhanhdong[4].dangthuchien = true;
                                    nhanvat.DiChuyen();
                                    return;
                                }
                                return;
                            }
                            else
                            {
                                if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong))
                                {
                                    nhanvat.list_danhsachhanhdong[3].dangthuchien = true;
                                    nhanvat.DiChuyen();
                                    return;
                                }
                                return;
                            }
                        }
                    }
                }
                if (nhanvat.keys.IsKeyDown(ScreenFight.keyspecialactionnguoichoi2) && nhanvat.dabaophuroi == false)
                {
                    if (!nhanvat.dangchuyendong || nhanvat.dangchuyendong || nhanvat.dangngoi)
                    {
                        if (!nhanvat.IsAction(nhanvat.list_danhsachhanhdong) && (((SpecialactionGokuleft)(nhanvat.list_danhsachhanhdong[6])).IsSatisfied(nhanvat)))
                        {
                            nhanvat.list_danhsachhanhdong[6].dangthuchien = true;
                            nhanvat.DiChuyen();
                            return;
                        }
                        return;
                    }
                }


                if (nhanvat.loaivp.IsUse(nhanvat.keys, nhanvat.chisonhanvat) && !nhanvat.loaivp.dangsudung && nhanvat.dabaophuroi == false)
                {
                    nhanvat.loaivp.dangsudung = true;
                    nhanvat.loaivp.Affect(nhanvat);
                }


                nhanvat.Dangchuyendong = false;
                nhanvat.kiemtranhaytoi = false;
                nhanvat.kiemtranhaylui = false;
            
        }



        public override void Update2(NhanVatGoku nhanvat)
        {
            if (!nhanvat.dangnhaytoi && !nhanvat.dangnhaylui && !nhanvat.Dangnhay)
            {
                if (nhanvat.kiemtranhaytoi)
                {
                    if (nhanvat.keys.IsKeyDown(ScreenFight.keyupnguoichoi2) == true)
                    {
                        nhanvat.huongChuyenDong = HanhDong.NhayToi;
                        nhanvat.DiChuyen();
                        nhanvat.kiemtranhaytoi = false;
                    }
                }
                else
                {
                    if (nhanvat.kiemtranhaylui)
                    {
                        if (nhanvat.keys.IsKeyDown(ScreenFight.keyupnguoichoi2) == true)
                        {
                            nhanvat.huongChuyenDong = HanhDong.NhayLui;
                            nhanvat.DiChuyen();
                            nhanvat.kiemtranhaylui = false;
                        }
                    }
                    else
                    {
                        if (nhanvat.keys.IsKeyDown(ScreenFight.keyupnguoichoi2) == true)
                        {
                            nhanvat.huongChuyenDong = HanhDong.NhayLenTaiCho;
                            nhanvat.DiChuyen();
                            nhanvat.kiemtranhaytoi = false;
                            nhanvat.kiemtranhaylui = false;
                        }
                    }
                }
                nhanvat.previouskeyboardstate = nhanvat.keys;
                if (nhanvat.Iscollision(nhanvat.nhanvatdoithu))
                {
                    nhanvat.SolveCollision(nhanvat.nhanvatdoithu);
                }
                return;
            }
            if (nhanvat.Iscollision(nhanvat.nhanvatdoithu))
            {
                nhanvat.SolveCollision(nhanvat.nhanvatdoithu);
            }
            return;
        }

      

        public override void FightAction(Nhanvat nhanvatdoithu, Nhanvat nhanvat)
        {
            for (int i = 0; i < nhanvat.list_danhsachhanhdong.Count; i++)
            {
                if (nhanvat.list_danhsachhanhdong[i].dangthuchien)
                {
                    if (nhanvat.list_danhsachhanhdong[i].IsFight(nhanvat, nhanvatdoithu, nhanvat.viTri))
                    {
                        nhanvat.list_danhsachhanhdong[i].SolveFight(nhanvat, nhanvatdoithu);
                    }
                }
            }
        }

      




       
      
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScreenManage;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using DragonBall_ZZZ.ChuyenDong;
using Microsoft.Xna.Framework.Media;

namespace DragonBall_ZZZ
{
        class BackgroundScreen : GameScreen
        {
            ContentManager content;
            Texture2D backgroundTexture;
            public static Song NhacNen;

            My2DSprite AnhChinh;
            List<My2DSprite> HinhDongMenu;
            int nHinhDongMenu = 2;


            #region Initialization
            public BackgroundScreen()
            {
                TransitionOnTime = TimeSpan.FromSeconds(1.5);
                TransitionOffTime = TimeSpan.FromSeconds(1);
                this.Name = "";

            }
            public override void Activate()
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");
                NhacNen = content.Load<Song>(@"AmThanh\NhacNenMenu\NhacNen");
                MediaPlayer.Play(NhacNen);

                backgroundTexture = content.Load<Texture2D>(@"HinhAnh\Menu\Main");


                Texture2D[] menuTemp = new Texture2D[1];
                menuTemp[0] = content.Load<Texture2D>(@"HinhAnh\Menu\Main0");

                HinhDongMenu = new List<My2DSprite>();
                for (int i = 0; i < nHinhDongMenu; i++)
                {
                    HinhDongMenu.Add(new My2DSprite(menuTemp, 0 + i * 1503, 0));
                    AbstractPathUpdater pathUpdate = new ChayToiChayLui(new Vector2((float)0 + i * Global.kichThuocAnhMenuDong, (float)0),
                        new Vector2((float)0.5, (float)0));
                    HinhDongMenu[i].PathUpdate = pathUpdate;
                }


                Texture2D[] temp = new Texture2D[3];

                temp[0] = content.Load<Texture2D>(@"HinhAnh\Menu\Main1");
                temp[1] = content.Load<Texture2D>(@"HinhAnh\Menu\Main2");
                temp[2] = content.Load<Texture2D>(@"HinhAnh\Menu\Main3");

                AnhChinh = new My2DSprite(temp, Global.viTriAnhDongMenu_X, Global.viTriAnhDongMenu_Y);
                AnhChinh.Delay = 80 * 16;
            }
            public override void Unload()
            {
                content.Unload();
                NhacNen.Dispose();
                MediaPlayer.Stop();
            }


            #endregion

            #region Update and Draw


            public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                           bool coveredByOtherScreen)
            {
                for (int i = 0; i < nHinhDongMenu; i++)
                {
                    HinhDongMenu[i].Update(gameTime);
                }

                AnhChinh.Update(gameTime);

                base.Update(gameTime, otherScreenHasFocus, false);
            }
            public override void Draw(GameTime gameTime)
            {
                SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
                Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
                Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);

                //Rectangle recMainTitle = new Rectangle(250, -110, 500, 300);
                spriteBatch.Begin();

                for (int i = 0; i < nHinhDongMenu; i++)
                {
                    HinhDongMenu[i].Draw(gameTime, spriteBatch, Color.White);
                }
                spriteBatch.Draw(backgroundTexture, fullscreen, Color.White);
                AnhChinh.Draw(gameTime, spriteBatch, Color.White);
                spriteBatch.End();
            }


            #endregion
        }
   
}

﻿

using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ScreenManage;
using System.Collections.Generic;

namespace DragonBall_ZZZ
{
   
    class MenuEntry :GameScreen
    {
        ContentManager content;
        string text;  
        float selectionFade;
        Vector2 position;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }
        public event EventHandler<PlayerIndexEventArgs> Selected;
        protected internal virtual void OnSelectEntry(PlayerIndex playerIndex)
        {
            if (Selected != null)
                Selected(this, new PlayerIndexEventArgs(playerIndex));
            
            
        }


        public MenuEntry(string text)
        {
            this.text = text;
            
        } 
        public  void Update(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            if (isSelected)
                selectionFade = 2;
            else
                selectionFade = 0;
        }
        public  void Draw(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            Color color = isSelected ? Color.Red : Color.Yellow;
            float scale = 1 + 0.05f * selectionFade;
            ScreenManager screenManager = screen.ScreenManager;
            SpriteBatch spriteBatch = screenManager.SpriteBatch;
            SpriteFont font = screenManager.Font;
             
            Vector2 origin = new Vector2(0, font.LineSpacing / 2);

            spriteBatch.DrawString(font, text, position, color, 0,
                                   origin, scale, SpriteEffects.None, 0);
        }
        public int GetHeight(MenuScreen screen)
        {
            return screen.ScreenManager.Font.LineSpacing;
        }
        public  int GetWidth(MenuScreen screen)
        {
            return (int)screen.ScreenManager.Font.MeasureString(Text).X;
        }
    }
}

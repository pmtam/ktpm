﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DragonBall_ZZZ
{
    class Vatphamtangnangluong :Vatphamhotro
    {

        Texture2D TangnangluongTextture;
        public string name;

        public Texture2D TangnangluongTextture1
        {
            get { return TangnangluongTextture; }
            set { TangnangluongTextture = value; }
        }
        ContentManager content;

        int _muctangnangluong;

        public int Muctangnangluong
        {
            get { return _muctangnangluong; }
            set { _muctangnangluong = value; }
        }
        public Vatphamtangnangluong()
        {
            _muctangnangluong = 5;
            name = "Tang 50% nang luong";
        }
        public override void Activate()
        {
            content = new ContentManager(ScreenManager.Game.Services, "Content");
           TangnangluongTextture = content.Load<Texture2D>(@"item/Tangnangluong");
        }
        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime, bool ismove, bool isselect, int style)
        {
            Vector2 pos = new Vector2(20, 550);
            Vector2 pos2 = new Vector2(675, 550);
            Rectangle rec = new Rectangle((int)(position.X), (int)(position.Y), 40, 40);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            if (!ismove)
            {

                spriteBatch.Draw(TangnangluongTextture, rec, Color.Gray);
            }
            else
            {
                if (ismove && !isselect)
                {
                    spriteBatch.Draw(TangnangluongTextture, rec, Color.White);
                    if (style == 1)
                    {
                        spriteBatch.DrawString(ScreenManager.Font, name, pos, Color.Yellow);
                    }
                    else
                    {
                        spriteBatch.DrawString(ScreenManager.Font, name, pos2, Color.White);
                    }
                }
                else
                {
                    if (ismove && isselect)
                    {
                        spriteBatch.Draw(TangnangluongTextture, rec, Color.Yellow);
                      
                    }
                }
            }
        }
        public override void Update(Microsoft.Xna.Framework.GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {

        }
    }
}

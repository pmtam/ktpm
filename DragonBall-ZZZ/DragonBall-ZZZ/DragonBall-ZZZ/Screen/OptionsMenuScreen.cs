﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using ScreenManage;

namespace DragonBall_ZZZ
{
  
    class OptionsMenuScreen : MenuScreen
    {

        MenuEntry SoundMenuEntry;
     
       public enum Sound
        {
            On,
            Off,
        }

        public static Sound  currentSound = Sound.On;

        public static int _currentSound = 0;

        public static bool frobnicate = true;


        public OptionsMenuScreen()
            : base("Options")
        {
           
            SoundMenuEntry = new MenuEntry(string.Empty);
            MenuEntry back = new MenuEntry("Back");
            SetMenuEntryText();        
            SoundMenuEntry.Selected += SoundMenuEntrySelected;
            back.Selected += OnCancel;

          
            MenuEntries.Add(SoundMenuEntry);

            MenuEntries.Add(back);
        }
        void SetMenuEntryText()
        {
           
            SoundMenuEntry.Text = "Sound:  " + currentSound;

            if (currentSound == 0)
            {
                MediaPlayer.Play(BackgroundScreen.NhacNen);
            }
            else
            {
                MediaPlayer.Stop();
            }

           
        }
        void SoundMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            
            currentSound++;

            if (currentSound > Sound.Off)
                currentSound = 0;
            SetMenuEntryText();
        }

        void FrobnicateMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            frobnicate = !frobnicate;

            SetMenuEntryText();
        }  
       

     
    }
}

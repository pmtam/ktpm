﻿


using System;
using Microsoft.Xna.Framework;
using ScreenManage;

namespace DragonBall_ZZZ
{
  
    class PlayerIndexEventArgs : EventArgs
    {
       
        public PlayerIndexEventArgs(PlayerIndex playerIndex)
        {
            this.playerIndex = playerIndex;
        }   
        public PlayerIndex PlayerIndex
        {
            get { return playerIndex; }
        }
        PlayerIndex playerIndex;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScreenManage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DragonBall_ZZZ
{
    class Trailer:GameScreen
    {
        ContentManager content;
        InputAction keyStop;
        Video vi;
        VideoPlayer pl = new VideoPlayer();

        public Trailer()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(1);
            this.Name = "";

            keyStop = new InputAction(new Keys[] { Keys.Escape }, true);
        }

        public override void HandleInput(GameTime gameTime, InputState input)
        {
            PlayerIndex playerIndex;

            if (keyStop.Evaluate(input, ControllingPlayer, out playerIndex))
            {
                IsExiting = true;
                pl.Stop();
                ScreenManager.RemoveScreen(this);
                ScreenManager.AddScreen(new BackgroundScreen(), ControllingPlayer);
                ScreenManager.AddScreen(new MainMenuScreen(), ControllingPlayer);
                
            }
        }

        public override void Activate()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            vi = content.Load<Video>(@"Video\Trailer");
            pl.Play(vi);
        }

        public override void Unload()
        {
            content.Unload();
            
            MediaPlayer.Stop();
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                           bool coveredByOtherScreen)
        {
            if (pl.State == MediaState.Stopped)
            {
                IsExiting = true;
                pl.Stop();
                ScreenManager.RemoveScreen(this);
                ScreenManager.AddScreen(new BackgroundScreen(), ControllingPlayer);
                ScreenManager.AddScreen(new MainMenuScreen(), ControllingPlayer);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);
            spriteBatch.Begin();
            spriteBatch.Draw(pl.GetTexture(), fullscreen, Color.White);
            spriteBatch.End();
        }


    }
}

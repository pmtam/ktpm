﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.Threading;
using System.Windows.Forms;
using DragonBall_ZZZ.FightAction;

namespace DragonBall_ZZZ.Screen
{
    public enum HanhDong
    {
        DiChuyenTraiQuaPhai,
        DiChuyenPhaiQuaTrai,
        NhayLenTaiCho,
        NhayToi,
        NhayLui,
        DangNgoi,
        QuynhTay,
        DaChan,
        NgoiQuynhTay,
        NgoiQuynhChan,
        NhayQuynhTay,
        NhayDaChan
    }

   public class NhanVatGoku: Nhanvat
    {    
        public HanhDong huongChuyenDong;
        public Texture2D hinhngoinhanvat;
        public LoainhanvatGoku loainhanvat;
        public static SoundEffect sounddie;
        public static SoundEffect sounddungdo;
        public static SoundEffectInstance instancedungdo;
        public static SoundEffectInstance instancedie;
        ContentManager contentGoku;
     
       
        public NhanVatGoku(Game1 game, Nhanvat newnhanvatdoithu, ContentManager newcontent)
        {
            Game = game;
            this.capNhat_X = new System.Windows.Forms.Timer();
            this.capNhat_X.Interval = 80;
            capNhat_X.Tick += new EventHandler(capNhat_X_Tick);
            capNhat_X.Enabled = true;

            contentGoku = newcontent;
            viTri.Y = toadoYbandau;
            nhanvatdoithu = newnhanvatdoithu;
            blood = max_blood;
            energy = max_energy;
            vitrihinhdichuyen = 0;
            vitrihinhnhaytaicho = 0;
            vitridangnhaytoi = 0;
            vitrihinhbiquynh1 = 0;
            vitrihinhbiquynh2 = 0;
            vitrihinhbinga = 0;
            vitrihinhdungyen = 0;
            vitrihinhvucday = 0;
            vitrihinhdungdo = 0;
            
            soluonghinhbiquynh2 = 3;
            soluonghinhbiquynh1 = 6;
            soluonghinhdichuyen = 6;
            soluonghinhdungyen = 10;
            soluonghinhnhaytaicho = 6;
            soluonghinhbinga = 7;
            soluonghinhnhaytoi = 8;
            soluonghinhvucday = 6;
            soluonghinhdungdo = 6;
            
            dolui = 75;
            solankiemtradungdokhinhaytoi = 0;
            dochenhlechY = 30;
            dochenhlechX = 60;

            buocChay = 3;
            buocnhay = 70;

            if (OptionsMenuScreen.currentSound == 0)
            {
                FightActionGame.Tiengvacham = contentGoku.Load<SoundEffect>("Sound10");
                NhanVatGoku.sounddie = contentGoku.Load<SoundEffect>("AMTHANH/AMTHANHGOKU/bichetcuaGoku");
                NhanVatGoku.sounddungdo = contentGoku.Load<SoundEffect>("amthanhdo");
                NhanVatGoku.instancedungdo = NhanVatGoku.sounddungdo.CreateInstance();
                NhanVatGoku.instancedie = NhanVatGoku.sounddie.CreateInstance();
            }
            
        }


        public void capNhat_X_Tick(object sender, EventArgs e)
        {
            loainhanvat.capNhat_X_Tick(sender, e, this);
        }

        public override void Update1(KeyboardState currentkey)
        {
            loainhanvat.Update1(currentkey, this);
        }

        public override void Update2()
        {
            loainhanvat.Update2(this);
        }

        public bool Iscollision(Nhanvat nhanvatdoithu)
        {
            if (die2)
            {
                if (nhanvatdoithu.viTri.X <= viTri.X + list_hinhbinga[vitrihinhbinga].Width - dochenhlechX)
                {
                    collision = 9;
                    return true;
                }
            }
            else
            {
                if (nhanvatdoithu.die2)
                {
                    if (viTri.X + list_hinhdungyen[0].Width + dochenhlechX >= nhanvatdoithu.viTri.X)
                    {
                        collision = 11;
                        return true;
                    }
                }
                else
                {
                    if (Dangnhaytoi)
                    {
                        if (nhanvatdoithu.Dangnhay)
                        {
                            Microsoft.Xna.Framework.Color[] personTexture1Data;
                            Microsoft.Xna.Framework.Color[] personTexture2Data;


                            personTexture1Data =
                                new Microsoft.Xna.Framework.Color[list_hinhnhaytoi[vitridangnhaytoi].Width * list_hinhnhaytoi[vitridangnhaytoi].Height];
                            list_hinhnhaytoi[vitridangnhaytoi].GetData(personTexture1Data);

                            personTexture2Data = new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width * nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height];
                            nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].GetData(personTexture2Data);


                            System.Drawing.Rectangle personRectangle =
    new System.Drawing.Rectangle((int)viTri.X, (int)viTri.Y,
    list_hinhnhaytoi[vitridangnhaytoi].Width, list_hinhnhaytoi[vitridangnhaytoi].Height);

                            System.Drawing.Rectangle blockRectangle =
        new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
        nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width, nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height);

                            if (IntersectPixels(personRectangle, personTexture1Data,
                        blockRectangle, personTexture2Data))
                            {
                                collision = 12;
                                return true;
                            }
                        }
                        else
                        {
                            if (nhanvatdoithu.Dangnhaytoi)
                            {

                                Microsoft.Xna.Framework.Color[] personTexture1Data;
                                Microsoft.Xna.Framework.Color[] personTexture2Data;



                                personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhnhaytoi[vitridangnhaytoi].Width * list_hinhnhaytoi[vitridangnhaytoi].Height];
                                list_hinhnhaytoi[vitridangnhaytoi].GetData(personTexture1Data);

                                personTexture2Data =
                                    new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width * nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height];
                                nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].GetData(personTexture2Data);


                                System.Drawing.Rectangle personRectangle =
        new System.Drawing.Rectangle((int)viTri.X, (int)viTri.Y,
        list_hinhnhaytoi[vitridangnhaytoi].Width, list_hinhnhaytoi[vitridangnhaytoi].Height);

                                System.Drawing.Rectangle blockRectangle =
            new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
            nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width, nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height);

                                if (IntersectPixels(personRectangle, personTexture1Data,
                            blockRectangle, personTexture2Data))
                                {
                                    collision = 6;
                                    return true;
                                }
                            }
                            else
                            {
                                if (nhanvatdoithu.Dangchuyendong)
                                {
                                    Microsoft.Xna.Framework.Color[] personTexture1Data;
                                    Microsoft.Xna.Framework.Color[] personTexture2Data;

                                    personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhnhaytoi[vitridangnhaytoi].Width * list_hinhnhaytoi[vitridangnhaytoi].Height];
                                    list_hinhnhaytoi[vitridangnhaytoi].GetData(personTexture1Data);

                                    personTexture2Data =
                                        new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width * nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height];
                                    nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].GetData(personTexture2Data);

                                    System.Drawing.Rectangle personRectangle =
            new System.Drawing.Rectangle((int)viTri.X, (int)viTri.Y,
            list_hinhnhaytoi[vitridangnhaytoi].Width, list_hinhnhaytoi[vitridangnhaytoi].Height);

                                    System.Drawing.Rectangle blockRectangle =
                new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width, nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height);

                                    if (IntersectPixels(personRectangle, personTexture1Data,
                                blockRectangle, personTexture2Data))
                                    {
                                        collision = 7;
                                        return true;
                                    }
                                }
                                else
                                {
                                    if (!nhanvatdoithu.Dangchuyendong)
                                    {
                                        Microsoft.Xna.Framework.Color[] personTexture1Data;
                                        Microsoft.Xna.Framework.Color[] personTexture2Data;

                                        personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhnhaytoi[vitridangnhaytoi].Width * list_hinhnhaytoi[vitridangnhaytoi].Height];
                                        list_hinhnhaytoi[vitridangnhaytoi].GetData(personTexture1Data);

                                        personTexture2Data =
                                            new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width * nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height];
                                        nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].GetData(personTexture2Data);

                                        System.Drawing.Rectangle personRectangle =
                new System.Drawing.Rectangle((int)viTri.X, (int)viTri.Y,
                nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width - dochenhlechX, nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height);

                                        System.Drawing.Rectangle blockRectangle =
                    new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                    nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width, nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height);

                                        if (IntersectPixels(personRectangle, personTexture1Data,
                                    blockRectangle, personTexture2Data))
                                        {
                                            collision = 8;
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (this.dangchuyendong)
                        {
                            if (nhanvatdoithu.Dangnhay)
                            {
                                Microsoft.Xna.Framework.Color[] personTexture1Data;
                                Microsoft.Xna.Framework.Color[] personTexture2Data;


                                personTexture1Data =
                                    new Microsoft.Xna.Framework.Color[list_hinhdichuyen[vitrihinhdichuyen].Width * list_hinhdichuyen[vitrihinhdichuyen].Height];
                                list_hinhdichuyen[vitrihinhdichuyen].GetData(personTexture1Data);

                                personTexture2Data = new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width * nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height];
                                nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].GetData(personTexture2Data);


                                System.Drawing.Rectangle personRectangle =
        new System.Drawing.Rectangle((int)viTri.X - dochenhlechX, (int)viTri.Y,
        list_hinhdichuyen[vitrihinhdichuyen].Width, list_hinhdichuyen[vitrihinhdichuyen].Height);

                                System.Drawing.Rectangle blockRectangle =
            new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
            nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Width, nhanvatdoithu.list_hinhnhaytaicho[nhanvatdoithu.vitrihinhnhaytaicho].Height);

                                if (IntersectPixels(personRectangle, personTexture1Data,
                            blockRectangle, personTexture2Data))
                                {
                                    collision = 0;
                                    return true;
                                }
                            }
                            else
                            {
                                if (nhanvatdoithu.Dangnhaytoi)
                                {
                                    Microsoft.Xna.Framework.Color[] personTexture1Data;
                                    Microsoft.Xna.Framework.Color[] personTexture2Data;

                                    personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhdichuyen[vitrihinhdichuyen].Width * list_hinhdichuyen[vitrihinhdichuyen].Height];
                                    list_hinhdichuyen[vitrihinhdichuyen].GetData(personTexture1Data);

                                    personTexture2Data =
                                        new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width * nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height];
                                    nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].GetData(personTexture2Data);


                                    System.Drawing.Rectangle personRectangle =
            new System.Drawing.Rectangle((int)viTri.X, (int)viTri.Y,
            list_hinhdichuyen[vitrihinhdichuyen].Width, list_hinhdichuyen[vitrihinhdichuyen].Height);

                                    System.Drawing.Rectangle blockRectangle =
                new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width, nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height);

                                    if (IntersectPixels(personRectangle, personTexture1Data,
                                blockRectangle, personTexture2Data))
                                    {
                                        collision = 0;
                                        return true;
                                    }
                                }
                                else
                                {
                                    if (nhanvatdoithu.Dangchuyendong)
                                    {
                                        Microsoft.Xna.Framework.Color[] personTexture1Data;
                                        Microsoft.Xna.Framework.Color[] personTexture2Data;

                                        personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhdichuyen[vitrihinhdichuyen].Width * list_hinhdichuyen[vitrihinhdichuyen].Height];
                                        list_hinhdichuyen[vitrihinhdichuyen].GetData(personTexture1Data);

                                        personTexture2Data =
                                            new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width * nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height];
                                        nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].GetData(personTexture2Data);

                                        System.Drawing.Rectangle personRectangle =
                new System.Drawing.Rectangle((int)viTri.X - dochenhlechX, (int)viTri.Y,
                list_hinhdichuyen[vitrihinhdichuyen].Width, list_hinhdichuyen[vitrihinhdichuyen].Height);

                                        System.Drawing.Rectangle blockRectangle =
                    new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                    nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width, nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height);

                                        if (IntersectPixels(personRectangle, personTexture1Data,
                                    blockRectangle, personTexture2Data))
                                        {
                                            collision = 2;
                                            nhanvatdoithu.collision = 2;
                                            return true;
                                        }
                                    }
                                    else
                                    {
                                        if (!nhanvatdoithu.Dangchuyendong)
                                        {
                                            if (nhanvatdoithu.die == true)
                                            {
                                                if (nhanvatdoithu.viTri.X + dochenhlechX <= viTri.X + list_hinhdichuyen[vitrihinhdichuyen].Width)
                                                {
                                                    collision = 10;
                                                    return true;
                                                }
                                            }
                                            else
                                            {
                                                Microsoft.Xna.Framework.Color[] personTexture1Data;
                                                Microsoft.Xna.Framework.Color[] personTextture2Data;

                                                personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhdichuyen[vitrihinhdichuyen].Width * list_hinhdichuyen[vitrihinhdichuyen].Height];
                                                list_hinhdichuyen[vitrihinhdichuyen].GetData(personTexture1Data);

                                                personTextture2Data =
                                                    new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width * nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height];
                                                nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].GetData(personTextture2Data);

                                                System.Drawing.Rectangle personRectangle =
                        new System.Drawing.Rectangle((int)viTri.X, (int)viTri.Y,
                        list_hinhdichuyen[vitrihinhdichuyen].Width - dochenhlechX, list_hinhdichuyen[vitrihinhdichuyen].Height);

                                                System.Drawing.Rectangle blockRectangle =
                            new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                            nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width, nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height);

                                                if (IntersectPixels(personRectangle, personTexture1Data,
                                            blockRectangle, personTextture2Data))
                                                {
                                                    collision = 0;
                                                    return true;
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!dangchuyendong)
                            {
                                if (nhanvatdoithu.Dangnhay)
                                {
                                    ;
                                }
                                else
                                {
                                    if (nhanvatdoithu.Dangnhaytoi)
                                    {
                                        Microsoft.Xna.Framework.Color[] personTexture1Data;
                                        Microsoft.Xna.Framework.Color[] personTexture2Data;



                                        personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhdungyen[vitrihinhdungyen].Width * list_hinhdungyen[vitrihinhdungyen].Height];
                                        list_hinhdungyen[vitrihinhdungyen].GetData(personTexture1Data);

                                        personTexture2Data =
                                            new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width * nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height];
                                        nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].GetData(personTexture2Data);


                                        System.Drawing.Rectangle personRectangle =
                new System.Drawing.Rectangle((int)viTri.X + dochenhlechX, (int)viTri.Y,
                list_hinhdungyen[vitrihinhdungyen].Width, list_hinhdungyen[vitrihinhdungyen].Height);

                                        System.Drawing.Rectangle blockRectangle =
                    new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                    nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Width, nhanvatdoithu.list_hinhnhaytoi[nhanvatdoithu.vitridangnhaytoi].Height);

                                        if (IntersectPixels(personRectangle, personTexture1Data,
                                    blockRectangle, personTexture2Data))
                                        {
                                            collision = 10;
                                            return true;
                                        }
                                    }
                                    else
                                    {
                                        if (nhanvatdoithu.Dangchuyendong)
                                        {
                                            Microsoft.Xna.Framework.Color[] personTexture1Data;
                                            Microsoft.Xna.Framework.Color[] personTexture2Data;

                                            personTexture1Data = new Microsoft.Xna.Framework.Color[list_hinhdichuyen[vitrihinhdichuyen].Width * list_hinhdichuyen[vitrihinhdichuyen].Height];
                                            list_hinhdichuyen[vitrihinhdichuyen].GetData(personTexture1Data);

                                            personTexture2Data =
                                                new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width * nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height];
                                            nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].GetData(personTexture2Data);

                                            System.Drawing.Rectangle personRectangle =
                    new System.Drawing.Rectangle((int)viTri.X - dochenhlechX, (int)viTri.Y,
                    list_hinhdichuyen[vitrihinhdichuyen].Width, list_hinhdichuyen[vitrihinhdichuyen].Height);

                                            System.Drawing.Rectangle blockRectangle =
                        new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                        nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Width, nhanvatdoithu.list_hinhdichuyen[nhanvatdoithu.vitrihinhdichuyen].Height);

                                            if (IntersectPixels(personRectangle, personTexture1Data,
                                        blockRectangle, personTexture2Data))
                                            {
                                                collision = 3;
                                                return true;
                                            }
                                        }
                                        else
                                        {
                                            if (!nhanvatdoithu.Dangchuyendong)
                                            {

                                                if (nhanvatdoithu.die == true)
                                                {
                                                    if (nhanvatdoithu.viTri.X + dochenhlechX <= viTri.X + list_hinhdichuyen[vitrihinhdichuyen].Width)
                                                    {
                                                        collision = 10;
                                                        return true;
                                                    }
                                                }
                                                else
                                                {
                                                    Microsoft.Xna.Framework.Color[] personTexture1Data;
                                                    Microsoft.Xna.Framework.Color[] personTextture2Data;

                                                    personTextture2Data = new Microsoft.Xna.Framework.Color[list_hinhdungyen[vitrihinhdungyen].Width * list_hinhdungyen[vitrihinhdungyen].Height];
                                                    list_hinhdungyen[vitrihinhdungyen].GetData(personTextture2Data);

                                                    personTexture1Data =
                                                        new Microsoft.Xna.Framework.Color[nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width * nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height];
                                                    nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].GetData(personTexture1Data);

                                                    System.Drawing.Rectangle personRectangle =
                            new System.Drawing.Rectangle((int)viTri.X, (int)viTri.Y,
                            list_hinhdungyen[vitrihinhdungyen].Width - dochenhlechX, list_hinhdungyen[vitrihinhdungyen].Height);

                                                    System.Drawing.Rectangle blockRectangle =
                                new System.Drawing.Rectangle((int)nhanvatdoithu.viTri.X, (int)nhanvatdoithu.viTri.Y,
                                nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Width, nhanvatdoithu.list_hinhdungyen[nhanvatdoithu.vitrihinhdungyen].Height);

                                                    if (IntersectPixels(personRectangle, personTexture1Data,
                                                blockRectangle, personTextture2Data))
                                                    {
                                                        collision = 3;
                                                        return true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }


        public override void SolveCollision(Nhanvat nhanvatdoithu)
        {
            switch (collision)
            {
                case 0:
                    {
                        nhanvatdoithu.viTri.X = this.viTri.X + list_hinhdichuyen[vitrihinhdichuyen].Width - dochenhlechX;
                        if (nhanvatdoithu.viTri.X > Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width)
                        {
                            nhanvatdoithu.viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width;
                            viTri.X = nhanvatdoithu.viTri.X - list_hinhdichuyen[vitrihinhdichuyen].Width + dochenhlechX;
                        }
                        collision = -1;
                    }
                    break;
                case 2:
                    {
                        ;
                    }
                    break;
                case 3:
                    {
                        this.viTri.X = nhanvatdoithu.viTri.X - list_hinhdungyen[vitrihinhdungyen].Width + dochenhlechX;
                        if (this.viTri.X < 0)
                        {
                            this.viTri.X = 0;
                            nhanvatdoithu.viTri.X = viTri.X + list_hinhdungyen[vitrihinhdungyen].Width - dochenhlechX;
                        }
                        collision = -1;
                    }
                    break;
                case 6:
                    {
                        if (collision == -1)
                        {
                            viTri.X = nhanvatdoithu.viTri.X - list_hinhdungyen[0].Width + dochenhlechX;
                            nhanvatdoithu.collision = 6;
                        }
                    }
                    break;
                case 7:
                    {
                        viTri.X = nhanvatdoithu.viTri.X - list_hinhnhaytoi[vitridangnhaytoi].Width + dochenhlechX;
                        collision = -1;
                    }
                    break;
                case 8:
                    {
                        nhanvatdoithu.viTri.X += buocChay;
                        if (nhanvatdoithu.viTri.X > Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width)
                        {
                            nhanvatdoithu.viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width;
                            viTri.X = nhanvatdoithu.viTri.X - list_hinhnhaytoi[vitridangnhaytoi].Width + dochenhlechX;
                        }
                        kiemtracodangnhaytoi = false;
                        collision = -1;
                    }
                    break;
                case 9:
                    {
                        nhanvatdoithu.viTri.X = viTri.X + list_hinhbinga[list_hinhbinga.Count - 1].Width - dochenhlechX;
                        collision = -1;
                    }
                    break;
                case 10:
                    {
                        nhanvatdoithu.viTri.X = viTri.X + list_hinhdungyen[vitrihinhdungyen].Width - dochenhlechX;
                        if (nhanvatdoithu.viTri.X > Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width)
                        {
                            nhanvatdoithu.viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width;
                            viTri.X = nhanvatdoithu.viTri.X - list_hinhdungyen[vitrihinhdungyen].Width + dochenhlechX;
                        }
                        collision = -1;
                    }
                    break;
                case 11:
                    {
                        viTri.X = nhanvatdoithu.viTri.X - list_hinhdungyen[0].Width + dochenhlechX;
                        collision = -1;
                    }
                    break;
                case 12:
                    {
                        nhanvatdoithu.viTri.X = viTri.X + list_hinhnhaytoi[vitridangnhaytoi].Width - dochenhlechX;
                        if (nhanvatdoithu.viTri.X > Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width)
                        {
                            nhanvatdoithu.viTri.X = Game.Window.ClientBounds.Width - list_hinhdungyen[0].Width;
                            viTri.X = nhanvatdoithu.viTri.X - list_hinhnhaytoi[vitridangnhaytoi].Width + dochenhlechX;
                        }
                        kiemtracodangnhaytoi = false;
                        collision = -1;
                    }
                    break;

            }
        }

        public override void FightAction(Nhanvat nhanVat2)
        {
            loainhanvat.FightAction(nhanVat2, this);
        }


        public override  void DiChuyen()
        {
            for(int i=0;i<list_danhsachhanhdong.Count;i++)
            {
                if (list_danhsachhanhdong[i].dangthuchien)
                {
                    list_danhsachhanhdong[i].DiChuyen();
                    return;
                }
            }

            if (huongChuyenDong == HanhDong.DiChuyenTraiQuaPhai && dabaophuroi == false)
            {
                if (collision == 1 || collision == 2)
                {
                    this.dangchuyendong = true;
                    collision = -1;
                }
                else
                {
                    if (!die)
                    {
                        this.dangchuyendong = true;
                        this.viTri.X += this.buocChay;
                        raNgoaiBanDo();
                    }
                    else
                    {
                        ;
                    }
                }
            }
            else
            {
                if (huongChuyenDong == HanhDong.DiChuyenPhaiQuaTrai && dabaophuroi == false)
                {
                    if (collision == 1 || collision == 7 || collision == 2)
                    {
                        this.dangchuyendong = true;
                        flag = true;
                        collision = -1;
                    }
                    else
                    {
                        if (!die)
                        {
                            this.dangchuyendong = true;
                            this.viTri.X -= this.buocChay;
                            raNgoaiBanDo();
                            flag = true;
                        }
                        
                    }
                }
                else
                {
                    if (huongChuyenDong == HanhDong.DangNgoi && dabaophuroi == false)
                    {
                        dangngoi = true;
                    }
                    
                    else
                    {
                        if (huongChuyenDong == HanhDong.NhayLenTaiCho && dabaophuroi == false)
                        {
                            dangnhay = true;
                        }
                        else
                        {
                            if (huongChuyenDong == HanhDong.NhayToi && dabaophuroi == false)
                            {
                                dangnhaytoi = true;
                            }
                            else
                            {
                                if (huongChuyenDong == HanhDong.NhayLui && dabaophuroi == false)
                                {
                                    dangnhaylui = true;
                                    dangnhaytoi = false;
                                    vitridangnhaytoi = soluonghinhnhaytoi - 1;
                                }
                            }
                        }
                    }
                }
            }
        }


         public override void Draw(SpriteBatch spriteBatch)
        {
            if (die2)
            {
                spriteBatch.Begin();
                spriteBatch.Draw(list_hinhbinga[vitrihinhbinga], viTri, Microsoft.Xna.Framework.Color.White);
                spriteBatch.End();
            }
            else
            {
                if (die || dangbinga)
                {
                    viTri.Y = toadoYbandau;
                    spriteBatch.Begin();
                    spriteBatch.Draw(list_hinhbinga[vitrihinhbinga], viTri, Microsoft.Xna.Framework.Color.White);
                    spriteBatch.End();
                }
                else
                {
                    if (dangvucday)
                    {
                        spriteBatch.Begin();
                        spriteBatch.Draw(list_hinhvucday[vitrihinhvucday], viTri, Microsoft.Xna.Framework.Color.White);
                        spriteBatch.End();
                    }
                    else
                    {
                        if (dangdo)
                        {
                            spriteBatch.Begin();
                            spriteBatch.Draw(list_hinhdungdo[vitrihinhdungdo], viTri, Microsoft.Xna.Framework.Color.White);
                            spriteBatch.End();
                        }
                        else
                        {
                            for (int i = 0; i < list_danhsachhanhdong.Count; i++)
                            {
                                if (list_danhsachhanhdong[i].dangthuchien)
                                {
                                    if (list_danhsachhanhdong[i].Test(this))
                                    {
                                        list_danhsachhanhdong[i].Draw(spritebatch, ref viTri, this);
                                        return;
                                    }
                                }
                            }

                                if (loaivp.dangsudung)
                                {
                                    loaivp.Draw(this, spritebatch, 0);
                                }
                            
                            if (biquynh1)
                            {
                                spriteBatch.Begin();
                                spriteBatch.Draw(list_hinhbiquynh1[vitrihinhbiquynh1], viTri, Microsoft.Xna.Framework.Color.White);
                                spriteBatch.End();
                            }
                            else
                            {
                                if (biquynh2)
                                {
                                    spriteBatch.Begin();
                                    spriteBatch.Draw(list_hinhbiquynh2[vitrihinhbiquynh2], viTri, Microsoft.Xna.Framework.Color.White);
                                    spriteBatch.End();
                                }
                                else
                                {
                                    if (dangnhaylui)
                                    {
                                        spriteBatch.Begin();
                                        spriteBatch.Draw(list_hinhnhaytoi[vitridangnhaytoi], viTri, Microsoft.Xna.Framework.Color.White);
                                        spriteBatch.End();
                                    }

                                    else
                                    {
                                        if (dangnhaytoi)
                                        {
                                            spriteBatch.Begin();
                                            spriteBatch.Draw(list_hinhnhaytoi[vitridangnhaytoi], viTri, Microsoft.Xna.Framework.Color.White);
                                            spriteBatch.End();
                                        }


                                        else
                                        {
                                            if (dangchuyendong)
                                            {
                                                if (huongChuyenDong == HanhDong.DiChuyenPhaiQuaTrai || huongChuyenDong == HanhDong.DiChuyenTraiQuaPhai)
                                                {
                                                    spriteBatch.Begin();
                                                    spriteBatch.Draw(list_hinhdichuyen[vitrihinhdichuyen], viTri, Microsoft.Xna.Framework.Color.White);
                                                    spriteBatch.End();
                                                }
                                            }

                                            else
                                            {
                                                if (dangnhay)
                                                {
                                                    spriteBatch.Begin();
                                                    spriteBatch.Draw(list_hinhnhaytaicho[vitrihinhnhaytaicho], viTri, Microsoft.Xna.Framework.Color.White);
                                                    spriteBatch.End();
                                                }
                                                else
                                                {
                                                    if (dangngoi)
                                                    {
                                                        spriteBatch.Begin();
                                                        spriteBatch.Draw(hinhngoinhanvat, viTri, Microsoft.Xna.Framework.Color.White);
                                                        spriteBatch.End();
                                                        dangngoi = false;
                                                    }
                                                    else
                                                    {
                                                        if (!dangchuyendong)
                                                        {
                                                            spriteBatch.Begin();
                                                            spriteBatch.Draw(list_hinhdungyen[vitrihinhdungyen], viTri, Microsoft.Xna.Framework.Color.White);
                                                            spriteBatch.End();
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }





      
    }
}

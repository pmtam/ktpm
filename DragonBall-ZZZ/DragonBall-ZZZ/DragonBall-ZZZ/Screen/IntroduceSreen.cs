﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using ScreenManage;
using Microsoft.Xna.Framework;
namespace DragonBall_ZZZ
{
    class IntroduceSreen: MenuScreen
    {
        Vector2 position;
       
        MenuEntry txtName;
        MenuEntry back = new MenuEntry("");
        public IntroduceSreen()
            : base("Introduces")
        {
            MenuEntries.Add(back);
            back.Selected += OnCancel;
            position.X = 100;
            position.Y = 400;

            this.Name = "";


        }
       
        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {

            Viewport ktscreen = ScreenManager.GraphicsDevice.Viewport;
          

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();

            spriteBatch.DrawString(ScreenManager.Font, "Dai hoc Khoa hoc Tu nhien\n   Khoa Cong nghe thong tin \n      Lap trinh Game\n\n\n        Product by: \n                   Phan Minh Tam \n                   Hoang Phuc Nguyen", position, Color.White);
            
            spriteBatch.End();
            base.Draw(gameTime);

        }
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            if(position.Y > 140)
            {
                position.Y--;
            }
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

      

    }
}

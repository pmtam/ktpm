﻿

#region Using Statements
using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Xml.Linq;
using DragonBall_ZZZ;
using System.Windows.Forms;
#endregion

namespace ScreenManage
{
    public class ScreenManager : DrawableGameComponent
    {

        List<GameScreen> screens = new List<GameScreen>();
        List<GameScreen> tempScreensList = new List<GameScreen>();
        InputState input = new InputState();
        SpriteBatch spriteBatch;
        SpriteFont font;
        bool isInitialized;
        bool traceEnabled;

        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        public SpriteFont Font
        {
            get { return font; }
        }

        public bool TraceEnabled
        {
            get { return traceEnabled; }
            set { traceEnabled = value; }
        }
        public Game1 game11;
        public ScreenManager(Game1 game)
            : base(game)
        {
            game11 = game;
        }
        public override void Initialize()
        {
            base.Initialize();

            Cursor myCursor = NativeMethods.LoadCustomCursor(@"../../../cursor.ani");
            Form winForm = (Form)Form.FromHandle(game11.Window.Handle);
            winForm.Cursor = myCursor;

            isInitialized = true;
        }
        protected override void LoadContent()
        {

            ContentManager content = Game.Content;

            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = content.Load<SpriteFont>(@"Menufont/menufont");



            foreach (GameScreen screen in screens)
            {
                screen.Activate();
            }
        }



        protected override void UnloadContent()
        {

            foreach (GameScreen screen in screens)
            {
                screen.Unload();
            }
        }

        public void RScreen(GameScreen g)
        {
            screens.Remove(g);
        }



        public override void Update(GameTime gameTime)
        {
            input.Update();
            tempScreensList.Clear();

            foreach (GameScreen screen in screens)
                tempScreensList.Add(screen);

            bool otherScreenHasFocus = !Game.IsActive;
            bool coveredByOtherScreen = false;

            while (tempScreensList.Count > 0)
            {

                GameScreen screen = tempScreensList[tempScreensList.Count - 1];

                if (tempScreensList.Count == 3 && tempScreensList[1].Name == "Doimat")
                {
                    tempScreensList.RemoveAt(tempScreensList.Count - 2);
                }

                tempScreensList.RemoveAt(tempScreensList.Count - 1);
                screen.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

                if (screen.ScreenState == ScreenState.TransitionOn ||
                    screen.ScreenState == ScreenState.Active)
                {

                    if (!otherScreenHasFocus)
                    {
                        screen.HandleInput(gameTime, input);

                        otherScreenHasFocus = true;
                    }


                    if (!screen.IsPopup)
                        coveredByOtherScreen = true;
                }
            }


            if (traceEnabled)
                TraceScreens();
        }


        void TraceScreens()
        {
            List<string> screenNames = new List<string>();

            foreach (GameScreen screen in screens)
                screenNames.Add(screen.GetType().Name);

            Debug.WriteLine(string.Join(", ", screenNames.ToArray()));
        }
        public override void Draw(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
            {
                if (screen.ScreenState == ScreenState.Hidden)
                    continue;

                screen.Draw(gameTime);
            }
        }
        public void AddScreen(GameScreen screen, PlayerIndex? controllingPlayer)
        {
            screen.ControllingPlayer = controllingPlayer;
            screen.ScreenManager = this;
            screen.IsExiting = false;
            if (isInitialized)
            {
                screen.Activate();
            }

            screens.Add(screen);
        }
        public void RemoveScreen(GameScreen screen)
        {
            if (isInitialized)
            {
                screen.Unload();
            }
            screens.Remove(screen);
            tempScreensList.Remove(screen);
        }
        public GameScreen[] GetScreens()
        {
            return screens.ToArray();
        }

        public void Deactivate()
        {
            return;
        }
        public bool Activate()
        {
            return false;
        }

    }
}
